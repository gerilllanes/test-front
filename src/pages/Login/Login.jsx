import { useState, useContext, useEffect } from "react";
import UserContext from "../../context/UserContext/UserContext";
import Form from "./components/Form/FormLogin";

const Login = () => {

  useEffect(() => {
    document.body.classList.add("background-login");
    return () => {
      document.body.classList.remove("background-login");
    };
  }, []);

  const userContext = useContext(UserContext);

  const [form, setForm] = useState({
    email: "",
    password: "",
  });

  const handleOnChange = (e) => {
    setForm({ ...form, [e.name]: e.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    userContext.userLogin(form);
  };

  return (
    <>
      <Form
        onChange={handleOnChange}
        handleSubmit={handleSubmit}
        buttonText={"Log in"}
        values={form}
      />
    </>
  );
};

export default Login;
