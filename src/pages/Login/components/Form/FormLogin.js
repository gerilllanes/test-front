import React, { useState } from "react";
import { Link as Redirecting } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import { IconButton, InputAdornment } from "@material-ui/core";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import translate from './../../../../lang/lang';

const useStyles = makeStyles((theme) => ({
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
    backgroundColor: "#121212",
    opacity: 0.9,
    padding: "3rem 5rem",
    fontFamily: "Bebas-Bold",
    letterSpacing: 1,
  },
  formControl: {
    marginBottom: "1rem"
  },
  label: {
    color: "#ffffff",
    margin: "0.5rem 0",
    letterSpacing: 1,
  },
  input: {
    color: "rgba(255, 255, 255, 0.8)",
    backgroundColor: "rgba(255, 255, 255, 0.1)",
    borderRadius: 0,
    fontFamily: "Bebas-Bold",
    letterSpacing: 1,
    margin: "0.3rem 0"
  },
  span: {
    color: "#ffffff",
    textDecoration: "none",
    display: "flex",
    float: "right",
    marginBottom: "1rem",
    "&:hover": {
      color: "#5EE8FF"
    }
  },
  link: {
    color: "#ffffff",
    textDecoration: "none",
    display: "flex",
    justifyContent: "center",
    "&:hover": {
      color: "#5EE8FF"
    }
  },
  submit: {
    margin: theme.spacing(0, 0, 2),
    backgroundColor: "#5EE8FF",
    borderRadius: 3,
    fontFamily: "Bebas-Bold",
    fontSize: 20,
    '&:hover': {
      backgroundColor: '#30CBE5',
    },
  },
}));

const FormLogin = ({
  handleSubmit,
  onChange,
  values,
  buttonText,
}) => {
  const classes = useStyles();
  const [hidden, setHidden] = useState(false);

  const handleHidden = () => {
    setHidden(!hidden);
  };

  return (
    <>
      <form className={classes.form} validate="true" onSubmit={handleSubmit}>
        <div className={classes.formControl}>
          <InputLabel className={classes.label}>{translate("Email")}</InputLabel>
          <TextField
            variant="outlined"
            required
            fullWidth
            border={0}
            name="email"
            value={values.email}
            onChange={(e) => onChange(e.target)}
            InputProps={{
              classes: { root: classes.input },
            }}
          />
        </div>
        <div className={classes.formControl}>
          <InputLabel className={classes.label}>{translate("Password")}</InputLabel>
          <TextField
            variant="outlined"
            required
            fullWidth
            name="password"
            value={values.password}
            onChange={(e) => onChange(e.target)}
            type={hidden ? "text" : "password"}
            InputLabelProps={{
              classes: {
                root: classes.input,
              },
            }}
            InputProps={{
              classes: { root: classes.input },
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton className={classes.btn} onClick={handleHidden}>
                    {hidden ? (
                      <VisibilityIcon style={{ color: "#7F7D80" }} />
                    ) : (
                      <VisibilityOffIcon style={{ color: "#7F7D80" }} />
                    )}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
          <Redirecting
            to={"/recover"}
            className={classes.span}
          >
            {translate("I forgot my password")}
          </Redirecting>
        </div>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          className={classes.submit}
        >
          {buttonText}
        </Button>
        <Redirecting
            to={"/register"}
            className={classes.link}
          >
            {translate("I don't have an account")}
          </Redirecting>
      </form>
    </>
  );
};

export default FormLogin;
