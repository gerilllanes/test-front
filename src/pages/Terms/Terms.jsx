import React from "react";
import TermsData from "./components/TermsData";
import { makeStyles } from "@material-ui/core/styles";
import glooudsBanner from "../../assets/img/glooudsBanner.png";

const useStyles = makeStyles((theme) => ({
  banner: {
    height: "100%",
    width: "100%",
  },
  wrap: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    boxSizing: "border-box",
    height: "100vh",
    padding: "2rem",
  },
  header: {
    padding: "1rem 0",
    borderBottom: "1px solid #ccc",
    textAlign: "center",
    "& > h2": {
      fontSize: "1.75rem",
      lineHeight: "1.75rem",
      textAlign: "center",
    },
  },
  container: {
    display: "flex",
    flexDirection: "column",
    boxSizing: "border-box",
    padding: "1rem",
    width: "768px",
    height: "100%",
    borderRadius: "0.25rem",
  },
  content: {
    flexGrow: 1,
    overflowY: "scroll",
    color: "black",
    border: "1px solid rgba(0, 0, 0, 0.25)",
    backgroundColor: "#fff",
    padding: "10px",
  },
}));

const Terms = () => {
  const classes = useStyles();
  return (
    <main className={classes.wrap}>
      <section className={classes.container}>
        <div className={classes.header}>
          <img src={glooudsBanner} alt="" className={classes.banner} />
        </div>
        <div className={classes.content}>
          <h2>TÉRMINOS Y CONDICIONES DE USO</h2>
          <div style={{ fontFamily: "Bebas-Light" }}>
            <TermsData />
          </div>
        </div>
      </section>
    </main>
  );
};

export default Terms;
