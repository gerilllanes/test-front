import React from "react";

const TermsConditionsText = () => {
  return (
    <>
      <p>Aceptación.</p>
      <p>
        1.1. Los presentes términos y condiciones (en adelante los "Términos y
        Condiciones”) regulan el uso de la Herramienta Digital denominada
        "‘GLOOUDS’ - 360 Sports Management" (en adelante, la "Herramienta
        Digital”) que el titular y propietario de la Plataforma, el Sr. DIEGO
        OSCAR MÜLLER, (en adelante “el Sr. MÜLLER”) pone a disposición de sus
        Usuarios en las tiendas de aplicaciones móviles y en el portal web. Para
        la utilización de la Herramienta Digital, es condición necesaria y
        excluyente, la lectura y aceptación previa y sin reservas de los
        presentes Términos y Condiciones.{" "}
      </p>
      <p>
        1.2. La utilización de la Herramienta Digital le otorgará la condición
        de "Usuario/s" a toda persona física o jurídica que solicite la
        suscripción a la misma, abone una Contraprestación por ella, y realice
        la descarga en su dispositivo móvil o inicie sesión en la misma en su
        versión de portal.
      </p>
      <p>
        1.3. El Usuario manifiesta, en tal sentido, haber leído, entendido y
        aceptado los presentes Términos y Condiciones, puestos a su disposición,
        en todo momento, con carácter previo a la utilización de la Herramienta
        Digital.{" "}
      </p>
      <p>
        1.4. La utilización de la Herramienta Digital conlleva, asimismo, la
        aceptación por parte del Usuario de todos aquellos avisos, reglamentos
        de uso e instrucciones actualmente en vigencia y los que fueren puestos
        en su conocimiento efectivo por parte del Sr. MÜLLER en el futuro.
      </p>
      <p>
        1.5. Las infracciones por parte de los Usuarios a estos Términos y
        Condiciones darán derecho a el Sr. MÜLLER a suspender o cancelar en
        forma temporal o definitiva de la Herramienta Digital.
      </p>
      <p>Objeto.</p>
      <p>
        2.1. Los presentes Términos y Condiciones regulan la prestación de la
        Herramienta Digital por parte del Sr. MÜLLER y su utilización por parte
        de los Usuarios. El Sr. MÜLLER se reserva el derecho a modificar en
        forma unilateral, en cualquier momento y sin previo aviso, la
        presentación, configuración y Términos y Condiciones de la Herramienta
        Digital.
      </p>
      <p>La Herramienta Digital.</p>
      <p>
        3.1. La Herramienta Digital es una Plataforma de gestión deportiva, con
        un fuerte foco en la actividad y evolución de deportistas en todos sus
        estadios (inicial, escuelas de deportes, competitivo, profesionales,
        etc.).
      </p>
      <p>
        3.2. La Herramienta Digital, permite recolectar y procesar datos de los
        diferentes perfiles integrándose con diversas fuentes de información
        complementarias (por ejemplo, GPS, Plataformas de Video, Climatología
        entre otras) e incorporar la información generada en Gloouds- 360 Sports
        Management (Socialmetrix, video análisis, etc.).
      </p>
      <p>
        3.3. La Herramienta Digital es un completo sistema integrador y
        agrupador metódico de información que ofrece la posibilidad de
        utilizarla como medio de gestión para analizar, comparar y cruzar datos,
        permitiendo así la optimización de la toma de decisiones inteligentes
        futuras.
      </p>
      <p>
        3.4. La Herramienta Digital está compuesto por cuatro módulos, cada uno
        de los cuales ofrece una funcionalidad diferente, a saber: a) Módulo
        jugadores y equipos; b) Análisis GPS; c) Análisis Video; y d) Módulo
        torneos.
      </p>
      <p>Condiciones de Acceso y Utilización de la Herramienta Digital.</p>
      <p>
        4.1. La Herramienta Digital estará disponible para todos los
        dispositivos móviles y para todas las versiones que cuenten con los
        sistemas operativos de Android y IOS. Asimismo, posibilita el acceso a
        la misma ingresando a su cuenta a través de su portal web. El servicio
        se proporciona de forma continua todos los días de la semana, las 24
        horas del día, excepto cuando el servicio se suspenda debido a problemas
        técnicos y / o mantenimiento, en cuyo caso el Sr. MÜLLER proporcionará
        al Usuario un aviso por escrito por correo electrónico con antelación, a
        menos que se trate de casos de fuerza mayor o imprevistos.
      </p>
      <p>
        4.2. Para acceder a la Herramienta Digital, el Usuario deberá solicitar
        la suscripción a la Plataforma. Inicialmente puede pedir que se le
        otorgue una versión DEMO a fines de probarla. El Contrato se considerará
        celebrado y los Términos y Condiciones aceptados cuando el Usuario
        utilice cualquiera de las versiones de la Herramienta Digital, incluida
        la versión DEMO. La falta de pago de la Contraprestación a la fecha del
        vencimiento se considerará como causa de incumplimiento del Contrato.
      </p>
      <p>
        4.3. El Usuario deberá dar de alta su cuenta generando su nombre de
        Usuario, y recibirá una contraseña inicial, la cual luego deberá
        modificar, respetando las medidas de seguridad mínimas establecidas por
        la Herramienta Digital, que le permitirán acceder a la Plataforma. Ambos
        datos conforman las credenciales de Inicio de Sesión. El Usuario conoce
        y acepta que será exclusivamente responsable de adoptar todas las
        medidas de seguridad necesarias para proteger sus datos de login y el
        dispositivo móvil donde opera con la Herramienta Digital. En caso de
        pérdida y / o no recordar las Credenciales de inicio de sesión, el
        Usuario puede solicitar nuevas Credenciales de inicio de sesión a través
        de la Plataforma para reemplazar las anteriores.
      </p>
      <p>
        4.4. El Sr. MÜLLER podrá dar de baja la suscripción sin previo aviso si
        observare que el Usuario no respeta los presentes términos y condiciones
        y/o las que se le informen en el futuro. Esta cancelación del servicio
        no implica ni obliga a reintegro alguno de la Contraprestación abonada
        por el período en curso.
      </p>
      <p>
        4.5. La Herramienta Digital en su formato para dispositivos móviles
        contará con la modalidad de acceso por medio de ejecutar el icono y una
        vez descargada, en la terminal móvil se podrá hacer uso de ella.
      </p>
      <p>
        4.6. El Usuario declara, con el uso de la Herramienta Digital, que es
        mayor de edad o capaz legalmente. En cualquier otro caso, necesitará la
        autorización previa de sus padres o tutores legales. Si se aceptan los
        presentes términos y condiciones, se está manifestando ser mayor de edad
        y que posee la capacidad legal necesaria para contratar los servicios
        ofrecidos.
      </p>
      <p>
        4.7. El Usuario acepta y reconoce que el Sr. MÜLLER no será responsable
        contractual o extracontractualmente, por ningún daño o perjuicio,
        directo o indirecto, ni siquiera en carácter solidario ni en carácter de
        proveedor de servicios, derivado de la utilización de la Herramienta
        Digital. El Usuario entiende, reconoce y acepta tal circunstancia,
        desistiendo en este acto y bajo declaración jurada de iniciar reclamo
        alguno, de índole administrativa y/o judicial, contra el Sr. MÜLLER,
        respecto de las operaciones y funciones mencionadas en los presentes
        Términos y Condiciones, sin importar la teoría legal en las cuales las
        fundamente.{" "}
      </p>
      <p>
        {" "}
        4.8. El Usuario asume total responsabilidad frente al Sr. MÜLLER y a
        terceros por los daños y perjuicios de toda clase que se generen como
        consecuencia de su uso de la Herramienta Digital, debiendo el Usuario
        indemnizar y mantener indemne al Sr. MÜLLER y a terceros ante cualquier
        reclamo, incluyendo honorarios profesionales que pudieran corresponder.
      </p>
      <p>Ingreso por primera vez.</p>
      <p>
        5.1. Una vez confirmada la suscripción, abonada la Contraprestación y
        aceptados los términos y condiciones se deberán generar las credenciales
        de inicio de sesión, conforme fue indicado previamente, para acceder a
        su cuenta de Herramienta Digital.
      </p>
      <p>
        5.2. Una vez que la cuenta de la Plataforma haya sido creada por el
        Usuario, se debe descargar la Herramienta Digital en el dispositivo
        móvil donde se desea utilizar. Efectuada la descarga deberá seleccionar
        el icono de la Herramienta Digital para ingresar a la misma. En caso de
        acceder a través del portal web, se debe ingresar a
        https://www.gloouds.com/
      </p>
      <p>
        5.3. Para acceder a la Herramienta Digital, el sistema solicitará los
        datos de inicio. Con ellos, se reconocerá la identidad y se podrá
        acceder a la cuenta. Una vez iniciada la sesión, se solicitará que se
        modifique la contraseña. En los siguientes ingresos se requerirán los
        mismos datos de acceso.
      </p>
      <p>
        5.4. Al acceder a su cuenta, se le solicitará al Usuario que proporcione
        información específica sobre sí mismo, su empresa, corporación o
        profesión para poder utilizar el servicio. El Usuario se compromete a
        proporcionar información veraz, precisa y completa y a abstenerse de
        representar falsamente la afiliación con cualquier persona o entidad. El
        Usuario siempre debe asegurarse de que la dirección de correo
        electrónico proporcionada permanezca activa o, alternativamente,
        informar de cualquier dirección de correo electrónico nueva y activa a
        la que se puedan enviar avisos.
      </p>
      <p>
        5.5. El Usuario puede actualizar y corregir en cualquier momento la
        información personal ingresada en la Herramienta Digital. El Usuario es
        responsable de la veracidad y exactitud de los datos personales
        ingresados ​​y el Sr. MÜLLER no asumirá ninguna responsabilidad al
        respecto.
      </p>
      <p>
        5.6. El Sr. MÜLLER se reserva el derecho de invitar al Usuario a
        eventos, foros y conferencias organizados por el propio Sr. MÜLLER.
      </p>
      <p>Módulos y funcionalidades disponibles</p>
      <p>
        6.1. Módulo Jugadores y Equipos. Este módulo se encuentra compuesto por:
        a) Historial de Jugadores Adiciona jugadores y clubes para poder
        gestionar y evaluar 360º a cada jugador con trazabilidad cuantitativa y
        cualitativa, proyecciones y Smartdata productiva. Base de datos
        personalizada b) Socialmetrix Asigna puntajes y valoraciones a las
        distintas habilidades de cada jugador y compara los rankings 6.2. Módulo
        Análisis GPS. GLooUDS sincroniza los datos de los dispositivos GPS,
        existentes en el club, con la base de información propia lo cual permite
        realizar análisis integrales y cruzados de las actitudes y aptitudes
        físicas de cada jugador bajo diversas condiciones climáticas y de
        estrés. 6.3. Módulo Análisis Video. A través de esta herramienta se
        podrán identificar aquellos momentos destacados de los entrenamientos y
        competencias, conformando así un nuevo video eficaz y dinámico. 6.4.
        Módulo Torneos GLooUDS posibilita la creación, organización y gestión
        integral de eventos deportivos, definiendo horarios, equipos, árbitros y
        creando fixtures e incluso generando el link de pago para que, tanto los
        equipos como los jugadores, puedan suscribirse
      </p>
      <p>Uso Correcto de la Herramienta Digital.</p>
      <p>
        7.1. El Usuario se compromete a utilizar la Herramienta Digital de forma
        correcta y diligente. Asimismo, a respetar la ley, la moral, las buenas
        costumbres y el orden público mediante el uso de la Herramienta Digital.
        El Usuario se obliga a utilizar la Herramienta Digital con fines lícitos
        y cumpliendo estrictamente con la normativa vigente, quedando prohibido
        el uso en actividades que directa e indirectamente afecten derechos
        individuales y/o colectivos de terceros.{" "}
        <p>
          7.2. El Usuario se compromete a abstenerse de utilizar la Herramienta
          Digital con fines o efectos ilícitos, prohibidos en los presentes
          Términos y Condiciones, lesivos de los derechos e intereses de
          terceros, o que de cualquier forma puedan dañar, inutilizar,
          sobrecargar o deteriorar la Herramienta Digital. El Sr. MÜLLER se
          reserva la facultad de adoptar las medidas que estime oportunas en
          caso de que existan indicios de uso fraudulento de la Herramienta
          Digital, incluyendo la adopción de las acciones legales oportunas.{" "}
        </p>{" "}
        <p>
          7.3. El Usuario se compromete a abstenerse de: i) reproducir, o
          copiar, distribuir, permitir el acceso del público a través de
          cualquier modalidad de comunicación pública, transformar o modificar
          el o los contenidos de la Herramienta Digital y la Herramienta Digital
          en sí misma, a menos que se cuente con la autorización del titular de
          los correspondientes derechos o ello resulte legalmente permitido, ii)
          suprimir, eludir o manipular el “copyright” o “derecho de autor” y
          demás datos identificativos de los derechos morales de los titulares
          incorporados a los contenidos de la Herramienta Digital, así como los
          dispositivos técnicos de protección, las huellas Digitales o
          cualquiera mecanismo de información que pudiera contener los
          contenidos de la Herramienta Digital.{" "}
        </p>{" "}
        <p>
          7.4. El Usuario se compromete a abstenerse de obtener e incluso de
          intentar obtener, los contenidos de la Herramienta Digital empleando
          para ello medios o procedimientos distintos de los que, en cada caso,
          se hayan proporcionado o se hayan indicado a tal efecto.{" "}
        </p>{" "}
        <p>
          7.5. El Sr. MÜLLER se reserva el derecho a denegar o retirar el acceso
          a la Herramienta Digital, en cualquier momento y sin necesidad de
          preaviso a aquellos Usuarios que incumplan estos Términos y
          Condiciones.
        </p>
      </p>
      <p>Conducta del Usuario – Contenidos generados por el Usuario</p>
      <p>
        8.1. El derecho del Usuario a utilizar la Herramienta Digital es
        personal. Si se compran Cuentas adicionales, el Usuario recibirá tantas
        Credenciales de inicio de sesión a la Plataforma como Cuenta adquiera.{" "}
      </p>
      <p>
        8.2. El Sr. MÜLLER puede cancelar y/o suspender inmediatamente las
        Credenciales de inicio de sesión del Usuario y bloquear el acceso a la
        Plataforma, si detecta cualquier uso no autorizado de las Credenciales
        de inicio de sesión y / o cualquier uso de la Plataforma no autorizado o
        realizado por personas no autorizadas.{" "}
      </p>{" "}
      <p>
        8.3. Está estrictamente prohibido que el Usuario asigne, transfiera y /
        o autorice y / o cobre a otros para usar el servicio y / o acceder a la
        Plataforma.{" "}
      </p>{" "}
      <p>
        8.4. Al utilizar la Plataforma, y ​​con respecto específicamente a los
        Contenidos generados por el Usuario, cada Usuario es el único
        responsable de los Contenidos generados por el Usuario cargados por
        éste, y se compromete a no cargar ni publicar a través de la Plataforma
        ninguno de los siguientes: Contenido generado por el Usuario que sea
        ilegal, difamatorio, ofensivo, obsceno, indecente, vulgar, sexualmente
        explícito, acosador, amenazante, invasivo de los derechos de privacidad
        o publicidad, abusivo o de cualquier otro modo objetable; Contenido
        generado por el Usuario que constituiría, alentaría o proporcionaría
        instrucciones para una ofensa criminal, violaría los derechos de
        cualquier parte, o que de otro modo crearía responsabilidad o violaría
        cualquier ley local, estatal, nacional o internacional; Contenido
        generado por el Usuario que describe, hace referencia o comunica actos
        violentos, peligrosos, ilegales y / o delictivos; Contenido generado por
        el Usuario que describe, hace referencia o comunica odio o
        discriminación por motivos de género, orientación sexual, raza, religión
        o nacionalidad; Contenido generado por el Usuario que es perjudicial o
        degrada la buena voluntad asociada con las marcas comerciales o nombres
        del Sr. MÜLLER; Contenido generado por el Usuario que pueda infringir
        cualquier patente, marca comercial, secreto comercial, derecho de autor
        u otro derecho intelectual o de propiedad de un tercero; promociones,
        campañas políticas, publicidad o solicitudes no solicitadas; información
        privada de terceros, incluidas, entre otras, direcciones, números de
        teléfono, direcciones de correo electrónico y números de tarjetas de
        crédito; virus, datos corruptos u otros archivos dañinos, disruptivos o
        destructivos; Contenido generado por el Usuario que, a juicio exclusivo
        del Sr. MÜLLER, es objetable o que restringe o inhibe a cualquier otra
        persona de usar o disfrutar de la Plataforma o que puede exponer al Sr.
        MÜLLER y / o sus Usuarios de la Plataforma a cualquier daño o
        responsabilidad de cualquier tipo.{" "}
      </p>{" "}
      <p>
        8.5. Cualquier uso de la Plataforma en violación de las reglas
        anteriores viola este Contrato y puede resultar en, entre otras cosas,
        la terminación o suspensión de los derechos de cualquier Usuario para
        usar la Plataforma y los servicios.{" "}
      </p>{" "}
      <p>
        8.6. Cada Usuario es el único responsable de todos los Contenidos
        generados por el/ella y cargados en la Plataforma. El Sr. MÜLLER no
        asume ninguna responsabilidad por el Contenido generado y/o cargado por
        el Usuario o cualquier tercero y / o por cualquier pérdida o daño al
        mismo. 8.7. Como proveedor de servicios interactivos, el Sr. MÜLLER no
        se responsabiliza ni asume ninguna responsabilidad por cualquier
        Contenido publicado, almacenado, transmitido o cargado a por los
        Usuarios en la Herramienta Digital; ni se hace responsable por errores,
        difamación, calumnias, exposiciones, omisiones, falsedades,
        obscenidades, pornografía o profanidades que pueda encontrar. En ningún
        caso el contenido mostrado en la Herramienta Digital refleja el
        pensamiento u opinión del Sr. MÜLLER, quien no se hace responsable por
        ninguna valoración, declaración, representación o ningún otro Contenido
        proporcionado por sus Usuarios en su Herramienta Digital, ni responde
        por susceptibilidades, daños morales y perjuicios que otros Usuarios o
        terceros puedan llegar a percibir y/o reclamar por lo publicado en la
        Herramienta Digital. Incluso si se determina que el Usuario retiene los
        derechos morales (incluidos los derechos de atribución o de
        integridad) por la presente, el Usuario declara que,  (a) no requiere
        que se use cualquier información de identificación personal en relación
        con el Contenido, o cualquier trabajo derivado o actualizaciones de
        este; (b) no objeta la publicación, el uso, la modificación, la
        eliminación y la explotación de su Contenido por parte del Sr. MÜLLER o
        por sus licenciatarios, sucesores y designados; (c) exime por siempre y
        acepta no reclamar ni exigir derecho a todos los derechos morales de un
        autor en cualquier parte de su Contenido; y (d) exime por siempre al Sr.
        MÜLLER y a sus licenciatarios, sucesores y designados, de cualquier
        reclamo que podría exigir en virtud de dichos derechos morales. 8.8
        Aunque el Sr. MÜLLER no revisa de manera rutinaria el Contenido generado
        por el Usuario y no tiene la obligación de revisar, filtrar, editar o
        monitorear el Contenido generado por el Usuario cargado en la
        Plataforma, el Sr. MÜLLER se reserva el derecho, y tiene absoluta
        discreción, de revisar, eliminar, rechazar, deshabilitar el acceso,
        filtrar o editar cualquier Contenido generado por el Usuario cargado en
        la Plataforma en cualquier momento y por cualquier motivo sin previo
        aviso, y cada Usuario es el único responsable de crear copias de
        seguridad y reemplazar cualquier Contenido generado por el Usuario
        cargado en la Plataforma en su / sus únicos costos y gastos.{" "}
      </p>{" "}
      <p>
        8.9 El Sr. MÜLLER se reserva el derecho, a su entera discreción, de
        determinar si y qué acción tomar en respuesta a un Contenido Generado
        por el Usuario en particular, y cualquier acción o inacción en una
        instancia particular no dictará o limitará la respuesta del Sr. MÜLLER
        con respecto a otro Contenido Generado por el Usuario.{" "}
      </p>{" "}
      <p>
        8.10. Cada Usuario declara y garantiza que (a) posee y controla todos
        los derechos relacionados con el Contenido generado por el Usuario
        cargado o tiene todos los derechos necesarios para cargar dicho
        Contenido generado por el Usuario en la Plataforma; (b) el Contenido
        generado por el Usuario es preciso y no engañoso; y (c) el Contenido
        generado por el Usuario cargado no viola estos Términos generales ni
        ninguna ley, regla o reglamento aplicable, y no violará ningún derecho
        ni causará daños a ninguna persona o entidad.{" "}
      </p>{" "}
      <p>
        8.11. El Usuario entiende que al ingresar o compartir contenido propio
        de cualquier índole en la Herramienta Digital, y al incluir contenido en
        su cuenta en lo referente a cualquier valoración, opinión, fotografía o
        video, comentario, sugerencia, ideas o materiales similares, otorga al
        Sr. MÜLLER un derecho no exclusivo, exento de regalías, perpetuo,
        transferible, irrevocable y plenamente sublicenciable para (a) albergar,
        utilizar, reproducir, modificar, ejecutar, adaptar, traducir,
        distribuir, publicar, crear trabajos derivados de y exhibir e
        interpretar públicamente dicho Contenido suyo en todo el mundo en
        cualquier medio de comunicación, conocido ahora o que se invente en un
        futuro; (b) hacer que su Contenido esté disponible para el resto del
        mundo y permitir a otros hacer lo mismo; (c) proporcionar, promocionar y
        mejorar su Contenido y hacer que esté disponible para otras empresas,
        organizaciones o individuos para la sindicación, difusión, distribución,
        promoción o publicación de dicho Contenido suyo en otros medios o
        servicios, sujeto a nuestra Política de Privacidad y este Acuerdo; y (d)
        usar el nombre y/o la marca comercial que envíe en conexión con dicho
        Contenido suyo. El Usuario reconoce que el Sr. MÜLLER puede optar por
        proporcionar la atribución de su Contenido a discreción. El Usuario
        reconoce y acepta que su Contenido no es confidencial ni de su propiedad
        y afirma, representa y garantiza que es propietario o tiene las
        licencias, los derechos (incluidos el copyright y otros derechos de
        propiedad), los consentimientos y los permisos necesarios para publicar
        o usar de cualquier modo (y para el Sr. MÜLLER publique y utilice) su
        Contenido, tal como se autoriza en este Acuerdo.{" "}
      </p>{" "}
      <p>
        8.12 Con referencia al artículo 8.11 anterior, siempre que el Usuario
        cargue contenido generado por el Usuario de la Plataforma, está obligado
        a informar al Sr. MÜLLER y a firmar el acuerdo de datos según el
        artículo 28 del Reglamento general de protección de datos (679/2016-GDPR
        ).{" "}
      </p>{" "}
      <p>
        8.13 Sin limitación a lo anterior, cada Usuario otorga al Sr. MÜLLER el
        derecho de realizar revisiones editoriales del Contenido generado por el
        Usuario, de usar y publicar el Contenido generado por el Usuario de
        cualquier manera y forma, incluso electrónica, en la Plataforma o por
        cualquier otro medio, ya sea creado ahora o en el futuro; utilizar el
        Contenido generado por el Usuario para fines comerciales internos; para
        reproducir y distribuir el Contenido generado por el Usuario con fines
        de marketing y publicidad, y para sublicenciar el Contenido generado por
        el Usuario a terceros por cualquier motivo.{" "}
      </p>{" "}
      <p>
        8.14. El Usuario conoce y acepta que sus datos de acceso son privados,
        secretos e intransferibles a terceros. Su resguardo será responsabilidad
        exclusiva del Usuario, quedando el Sr. MÜLLER eximido de cualquier tipo
        de responsabilidad por su divulgación a terceros o uso incorrecto. El
        uso de la Herramienta Digital realizado con el Usuario y Contraseña será
        considerado por el Sr. MÜLLER como válido, legítimo y auténticamente
        realizado por el Usuario, asumiendo este último las consecuencias del
        uso de la Herramienta Digital en su nombre. El Sr. MÜLLER no asume
        responsabilidad alguna en caso de extravío, robo, hurto, pérdida o mal
        uso por parte del Usuario o de terceros de sus datos de login o del
        dispositivo móvil donde haya descargado la Herramienta Digital.
      </p>
      <p>
        Disponibilidad, modificación, actualización y cancelación de la
        Herramienta Digital.
      </p>
      <p>
        9.1 El Sr. MÜLLER se reserva el derecho a modificar, en forma
        unilateral, en cualquier momento y sin previo aviso, la Herramienta
        Digital, sus Términos y Condiciones y las normas que los complementan.
        El Sr. MÜLLER aconseja al Usuario revisar periódicamente las normas que
        reglamentan la Herramienta Digital. El Usuario no podrá hacer
        responsable a el Sr. MÜLLER ni a ningún tercero por la suspensión o
        terminación de la Herramienta Digital.{" "}
        <p>
          9.2. el Sr. MÜLLER no garantiza la disponibilidad, continuidad ni en
          general el normal funcionamiento de la Herramienta Digital en el caso
          de incidencias e interrupciones en el normal funcionamiento de las
          redes involucradas en la Herramienta Digital.{" "}
        </p>{" "}
        <p>
          9.3. el Sr. MÜLLER podrá actualizar la Herramienta Digital, en
          cualquier momento y sin previo aviso a los Usuarios.{" "}
        </p>{" "}
        <p>
          9.4. El Usuario podrá desinstalar la Herramienta Digital en cualquier
          momento que lo desee desde la configuración de su dispositivo móvil,
          teniendo en cuenta las funcionalidades del equipo que posee. La
          desinstalación de la Herramienta Digital no implica el cierre ni la
          cancelación ni la suspensión de su cuenta. La misma quedará activa y
          continuará corriendo con gastos de mantenimiento/suscripción. A fin de
          dar de baja la suscripción, deberá específicamente optar por esta
          opción y recibir la confirmación de la Baja antes de desinstalar la
          aplicación.{" "}
        </p>{" "}
        <p>
          9.5. El Sr. MÜLLER podrá modificar y/o cancelar la Herramienta Digital
          en cualquier momento comunicándolo a los Usuarios con al menos 15 días
          de antelación al día en que deba hacerse efectiva dicha cancelación o
          modificación. Por el uso de esta facultad el Sr. MÜLLER no deberá
          abonar indemnización alguna.{" "}
        </p>{" "}
        <p>
          9.6. EL Sr. MÜLLER proporciona al Usuario soporte postventa por
          teléfono o correo electrónico sin costo adicional. Se proporciona
          asistencia para consultas técnicas relacionadas con el uso de la
          Plataforma y / o el servicio o información administrativa relacionada
          con pagos o facturación
        </p>
      </p>
      <p>Responsabilidad.</p>
      <p>
        10.1. El Usuario acepta que el uso de la Herramienta Digital se realiza
        bajo su propia, exclusiva y única responsabilidad.{" "}
        <p>
          10.2. El Sr. MÜLLER no asume responsabilidad de ningún tipo por la
          información incorrecta o inexacta relacionada a la Herramienta
          Digital, bien sea causada por un Usuario, las entidades publicas o
          privadas que también operen con la herramiente, error humano y/o
          cualquier otro factor que pueda ocurrir.{" "}
        </p>{" "}
        <p>
          10.3. El Sr. MÜLLER no se hace responsable de los contenidos,
          información y detalles publicados en el sitio web y/o en la Plataforma
          de la veracidad, actualización e integridad de los mismos. El Sr.
          MÜLLER, sus empleados y agentes, no serán responsables de ningún daño
          o perjuicio, directo o indirecto, de cualquier tipo, cualquiera que
          sea su causa, origen, naturaleza o consecuencias, que resulten del
          acceso o imposibilidad de acceso a los Sitio web, ni de la confianza
          en la información proporcionada a través del sitio web.{" "}
        </p>{" "}
        <p>
          10.4. El Usuario entiende y acepta expresamente que el Sr. MÜLLER no
          ofrecerá ninguna garantía de que la Plataforma satisfará las
          expectativas y necesidades del Usuario.{" "}
        </p>{" "}
        <p>
          10.5. El Sr. MÜLLER no tiene obligación de controlar y no controla la
          utilización que los Usuarios hacen de la Herramienta Digital. En
          particular, el Sr. MÜLLER no garantiza que los Usuarios utilicen la
          Herramienta Digital de conformidad con estos Términos y Condiciones ni
          que lo hagan de forma diligente y prudente. El Sr. MÜLLER no se
          responsabiliza por toda acción ilícita que pudiera derivar del uso de
          la Herramienta Digital.{" "}
        </p>{" "}
        <p>
          10.6. El Sr. MÜLLER no garantiza la disponibilidad y continuidad del
          funcionamiento de la Herramienta Digital. Cuando ello sea
          razonablemente posible, el Sr. MÜLLER advertirá previamente las
          interrupciones en el funcionamiento de la Herramienta Digital. El Sr.
          MÜLLER tampoco garantiza la utilidad de la Herramienta Digital para la
          realización de ninguna actividad en particular, ni su infalibilidad y,
          en particular, aunque no de modo exclusivo, que los Usuarios puedan
          efectivamente acceder a la Herramienta Digital o ejecutar cualquiera
          de las prestaciones previstas en estos Términos y Condiciones.{" "}
        </p>{" "}
        <p>
          10.7. El Sr. MÜLLER no garantiza la privacidad y la seguridad en la
          utilización de la Herramienta Digital por parte de los Usuarios y, en
          particular, no garantiza que terceros no puedan acceder y en su caso
          interceptar, eliminar alterar y/o modificar o manipular cualquier
          material y/o información publicada. el Sr. MÜLLER excluye toda
          responsabilidad por los daños y perjuicios de toda naturaleza que
          pudieran originarse en el conocimiento que puedan tener los terceros
          de los contenidos y/o de la Herramienta Digital, datos, páginas y/o
          información utilizada y/o publicados a través de la Herramienta
          Digital.{" "}
        </p>{" "}
        <p>
          10.8. El Sr. MÜLLER no será responsable por ningún daño o perjuicio,
          directo o indirecto, y de toda naturaleza, incluyendo sin ningún tipo
          de limitación los daños producidos por las pérdidas o deterioros de la
          información, ni por las demoras, interrupciones y defectos que
          pudieran existir en las transmisiones durante la descarga de la
          Herramienta Digital, mediante la utilización de la Herramienta
          Digital, los virus o fallas del sistema o línea, por la falta de
          disponibilidad y/o continuidad de la Herramienta Digital, la
          utilización que los Usuarios hacen del mismo y en especial pero de
          ningún modo exclusivo a las fallas en el acceso, difusión,
          disposición, eliminación, alteración, modificación o manipulación de
          los datos, páginas, aplicaciones y/o contenidos que los Usuarios
          transmitan, difundan, almacenen, pongan a disposición, reciban,
          obtengan o accedan a través de la Herramienta Digital. Asimismo, el
          Sr. MÜLLER no se responsabiliza por ningún daño o perjuicio directo o
          indirecto, incluyendo sin ningún tipo de limitación daños producidos
          por las perdidas, inexactitudes o deterioros de la información
          publicada ni por los contenidos de la Herramienta Digital.{" "}
        </p>{" "}
        <p>
          10.9. Los Usuarios se obligan a mantener indemne a el Sr. MÜLLER
          frente a cualquier reclamo originado por cualquier violación de lo
          aquí dispuesto, o de cualquier reclamo que inicie o pudiera iniciar
          cualquier tercero por uso indebido o ilegal de la Herramienta Digital,
          sus contenidos. Cada Usuario será exclusivo responsable del uso que le
          diera a la Herramienta Digital y deberá mantener indemne a el Sr.
          MÜLLER y demás empresas del Sr. MÜLLER ante cualquier reclamo que
          pudieran realizar terceros o cualquier organismo administrativo
          judicial u extrajudicial en virtud de su uso.
        </p>
      </p>
      <p>Autorización. Consentimiento.</p>
      <p>
        11.1. Los Usuarios de la Herramienta Digital autorizan al Sr. MÜLLER a
        utilizar sus datos personales mientras dure la relación, con fines
        publicitarios, en los medios y formas que el Sr. MÜLLER considere
        conveniente sin derecho a compensación alguna por difusión pública. Como
        titular de los datos personales, el Usuario podrá, en cualquier momento,
        ejercer su derecho a acceder, rectificar, actualizar, cancelar y/o
        suprimir dichos datos por medio de presentación escrita acreditando
        fehacientemente su identidad cesión.{" "}
        <p>
          11.2. La utilización del Sitio conlleva al conocimiento y expresa
          autorización, y sin reservas, de que aquellos datos que no requieren
          del consentimiento previo del Usuario para la recolección, tratamiento
          y cesion podrán ser utilizados por el Sr. MÜLLER, o por terceras
          personas a las que el Sr. MÜLLER se los hubiere cedido, para la
          realización de acciones promocionales de marketing, sin necesidad de
          notificación y/o autorización previa. Asimismo, el Usuario presta su
          consentimiento previo, expreso e informado a el Sr. MÜLLER para que
          esta última suministre sus datos personales a: (i) cualquier sociedad
          que integre el mismo grupo empresarial a la que el Sr. MÜLLER
          pertenezca, (ii) proveedores de productos y/o servicios con quienes el
          Sr. MÜLLER tenga convenio comercial vigente, con el objeto de
          entregar, ofrecer y/o vender productos, servicios, soluciones y
          cualquier tipo. Asimismo, el Usuario conoce y acepta expresamente que
          sus datos personales serán incluidos en archivos automatizados,
          procesados bajo normas de estricta confidencialidad y protección de
          datos -conforme con lo establecido en los artículos 5º, inciso 2.,
          sub-inciso c), y 11º, de la Ley N° 25.326 de Protección de Datos
          Personales y su Decreto Reglamentario 1558/2001, -{" "}
        </p>{" "}
        <p>
          11.3 El Usuario declara que ha recibido información del Sr. MÜLLER
          sobre el tratamiento de datos personales requerido por el artículo 13
          del Reglamento General de Protección de Datos (679/2016-GDPR)
          proporcionado en el apartado “Políticas de Privacidad” localizado en
          el sitio web de la Herramienta Digital: https://www.gloouds.com/{" "}
        </p>{" "}
        <p>
          11.4. El Usuario entiende que el Sr. MÜLLER utilizará medidas de
          seguridad físicas, electrónicas y de procedimiento razonables para
          proteger contra la pérdida, el uso indebido y la alteración de los
          datos personales que el Sr. MÜLLER reciba del Usuario.
        </p>{" "}
        <p>
          11.5. Cualquier información proporcionada por el Usuario en el momento
          de aceptar el Contrato o cualquier otra información proporcionada por
          el Usuario con respecto a su empresa o profesión (Datos del Cliente)
          está sujeta a la Política de Privacidad del Sr. MÜLLER. El Sr. MÜLLER
          se reserva el derecho de actualizar o modificar la Política de
          privacidad de vez en cuando a su exclusivo criterio.
        </p>
      </p>
      <p>Propiedad Intelectual.</p>
      <p>
        12.1. El Sr. MÜLLER no concede ninguna licencia o autorización de uso de
        ninguna clase sobre sus derechos de propiedad industrial e intelectual o
        sobre cualquier otra propiedad o derechos relacionado con la Herramienta
        Digital o que se encuentren vinculados a él. (Incluye marcas, logotipos,
        diseños, arte, títulos, símbolos, contenidos entre otros). Toda obra
        intelectual, marca, logotipo, diseño, arte, títulos, símbolos, know-how,
        u otras propiedades intelectuales en relación con la imagen, nombre,
        mención, voz, fonogramas u otras son pertenecientes el Sr. MÜLLER y se
        prohíbe su reproducción, distribución, comercialización y cualquier otro
        uso no expresamente autorizado por el Sr. MÜLLER.
      </p>
      <p>Varios.</p>
      <p>
        13.1 Si alguna disposición de este Contrato se considera inválida o
        inaplicable, dicha disposición se considerará nula y sin efecto,
        mientras que las disposiciones restantes permanecerán en pleno vigor y
        efecto.{" "}
        <p>
          13.2 El hecho de que el Sr. MÜLLER no ejerza o haga cumplir cualquier
          derecho o disposición del mismo no constituirá de ninguna manera una
          renuncia a dicho derecho o disposición.{" "}
        </p>{" "}
        <p>
          13.3 Este Contrato expresa el total entendimiento y acuerdo entre el
          Sr. MÜLLER y el Cliente en relación con el tema del mismo.{" "}
        </p>{" "}
        <p>
          13.4 Los títulos de las secciones se proporcionan para facilitar la
          lectura y no tienen ningún efecto legal o contractual.{" "}
        </p>{" "}
        <p>
          13.5 Cada parte actúa de forma independiente y no es un agente o
          representante de ninguna otra parte.
        </p>{" "}
        <p>
          13.6 Ninguna parte tiene el derecho o la autoridad para crear
          obligaciones o dar representaciones o garantías en nombre y en
          representación de otra parte. Este Contrato no se puede interpretar o
          interpretar para crear una asociación, empresa conjunta o sociedad
          entre las partes o para imponer ninguna obligación o responsabilidad
          de una sociedad a una de ellas. Las partes reconocen y acuerdan que
          este Contrato puede rescindirse electrónicamente sin necesidad de
          firmas escritas.{" "}
        </p>{" "}
        <p>
          13.7 El Cliente declara expresamente que ha leído y comprendido en su
          totalidad este Contrato, y que acepta todos sus términos y
          condiciones. El Cliente ha evaluado de forma independiente todos los
          aspectos de este Contrato y la oportunidad de adherirse a ellos. El
          Cliente declara que no se apoya en ninguna representación, garantía o
          declaración del Sr. MÜLLER y / o terceros que no esté expresamente
          contemplada en este Contrato.
        </p>
      </p>
      <p>Ley Aplicable y Jurisdicción.</p>
      <p>
        14.1. La prestación y el uso de la Herramienta Digital regulado mediante
        las presentes Términos y Condiciones se regirán por la ley Española{" "}
        <p>
          14.2. Para el caso de dudas o divergencias para la Herramienta Digital
          de las presentes Términos y Condiciones corresponderá la jurisdicción
          del Tribunal Superior de Justicia de la Comunidad Valenciana, con
          expresa renuncia a cualquier otro fuero o jurisdicción que pudiera
          corresponder.
        </p>
      </p>
      <p>Contacto.</p>
      <p>
        15.1. Se fija domicilio del Sr. MÜLLER en la calle Conde de Salvatierra
        N° 21, Planta 2, Puerta 4, 46004 Valencia, España. Ante cualquier duda
        sobre los términos y condiciones generales o demás políticas y
        principios, se solicita consultar a la casilla de correo
        diego.muller@bery.es.
      </p>
    </>
  );
};

export default TermsConditionsText;
