import React from "react";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import Section from "../../components/Section/Section";
import Edit from "../../components/Section/Edit";
import CategoryTable from "./components/CategoryTable";
import ModalCategory from "./components/ModalCategory";

const Category = () => {
  const headers = ["NOMBRE", "RANGO ETARIO", "DEPORTE"];

  const data = [
    {
      nombre: "german",
      rango: "el capi",
      deporte: "333",
    },
    {
      nombre: "german",
      rango: "el capi",
      deporte: "333",
    },
    {
      nombre: "german",
      rango: "el capi",
      deporte: "333",
    },
    {
      nombre: "german",
      rango: "el capi",
      deporte: "333",
    },
  ];

  return (
    <>
      <div style={{ display: "flex" }}>
        <ArrowBackIosIcon />
        <h1>CATEGORIA</h1>
      </div>
      <Section title="Mis categorias">
        <div style={{ display: "flex" }}>
          <h1>CATEGORÍAS EXISTENTES</h1> <Edit />
          <ModalCategory />
        </div>
        <CategoryTable data={data} headers={headers} />
      </Section>
    </>
  );
};

export default Category;
