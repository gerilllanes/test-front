import React from "react";
import TableDynamic from "../../Admin/AdminClub/components/TableDynamic";

const CategoryTable = ({ headers, data }) => {
  return <TableDynamic headers={headers} data={data} />;
};

export default CategoryTable;
