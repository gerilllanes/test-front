import React from "react";
import ModalTemplate from "../../../components/Section/ModalTemplate";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Checkbox from "@material-ui/core/Checkbox";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";

const useStyles = makeStyles(() => ({
  text: {
    fontFamily: "Bebas-Bold",
    fontSize: 24,
    flexGrow: 1,
    letterSpacing: 1,
    color: "#fff",
  },
  textMid: {
    fontFamily: "Bebas-Bold",
    fontSize: 30,
    flexGrow: 1,
    letterSpacing: 1,
    color: "#fff",
    textAlign: "center",
  },
  select: {
    fontFamily: "Bebas-Bold",
    flexGrow: 1,
    letterSpacing: 1,
    color: "#fff",
    textAlign: "center",
  },
}));

const FormCategory = () => {
  const classes = useStyles();

  return (
    <>
      <ModalTemplate title="Añadir categoria">
        <form noValidate autoComplete="off">
          <Typography className={classes.textMid}>Nombre</Typography>
          <TextField
            variant="filled"
            placeholder="EJ. SUB 16"
            label="NOMBRE"
            inputProps={{
              min: 0,
              style: {
                color: "#FFFF",
                backgroundColor: "#001a1a",
                textAlign: "center",
                fontFamily: "Bebas-Bold",
              },
            }}
          />
          <Typography className={classes.textMid}>RANGO ETARIO</Typography>
          <div style={{ display: "flex" }}>
            <Typography className={classes.text}>DE:</Typography>
            <TextField
              variant="filled"
              placeholder="MIN"
              label="EDAD MINIMA"
              inputProps={{
                min: 0,
                style: {
                  color: "#FFFF",
                  backgroundColor: "#001a1a",
                  textAlign: "center",
                  fontFamily: "Bebas-Bold",
                  width: "50px",
                },
              }}
            />
            <Typography className={classes.text}>HASTA: </Typography>
            <TextField
              variant="filled"
              placeholder="MAX"
              label="EDAD MAXIMA"
              inputProps={{
                min: 0,
                style: {
                  color: "#FFFF",
                  backgroundColor: "#001a1a",
                  textAlign: "center",
                  fontFamily: "Bebas-Bold",
                  width: "50px",
                },
              }}
            />
            <Typography className={classes.text}>ILIMITADO:</Typography>
            <Checkbox />
          </div>
          <div>
            <Typography className={classes.textMid}>DEPORTE:</Typography>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel className={classes.select}>Deporte</InputLabel>
              <Select>
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
            </FormControl>
          </div>
        </form>
      </ModalTemplate>
    </>
  );
};

export default FormCategory;
