import { Container, CssBaseline } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import logo from "../../assets/img/isologo_blanco.png";

const useStyles = makeStyles((theme) => ({
  paper: {
    width: "100%",
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  },
  logo: {
    height: "9rem",
    margin: "1rem"
  },
}));

const AuthLayout = ({ children }) => {
  const classes = useStyles();
  return (
    <>
      <div className="background-login"> 
        <Container  maxWidth="xs" className={classes.root}>
          <Box alignItems="center" className={classes.paper}>
            <img src={logo} alt="logo" className={classes.logo} />
            {children}
          </Box>
        </Container>
      </div> 
    </>
  );
};

export default AuthLayout;
