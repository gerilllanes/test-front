import React, { useEffect, useState, useContext } from "react";

import { Link } from "react-router-dom";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import translate from "../../lang/lang";
import Grid from "@material-ui/core/Grid";
import UserContext from "../../context/UserContext/UserContext";
import Typography from '@material-ui/core/Typography';
import { Route } from "react-router-dom";

const NavBar = (props) => {
  const {
    children,
    breadcrum = {},
    className,
    breadcrumVisible = true,
    breadcrumActive = "valoration",
  } = props;

  const userContext = useContext(UserContext);
  const userLogged = userContext.userLogged;

  const [defaultBreadcrum] = useState({
    ...{ dashboard: "/dashboard" },
    ...breadcrum,
    summary: "/player/summary/1",
    valoration: "/player/valoration/1",
    gps: "/gps",
    video_analysis: "/player/video_analysis/1",
    schedule: "/schedule",
  });

  const buildBreadcrum = () => {
    return Object.keys(defaultBreadcrum).map((key) => {
      return (
        <div
          className={`breadcrum-item${
            breadcrumActive === key ? " active" : ""
          }`}
          key={`breadcrum-item-${key}`}
        >
          <Link
            to={defaultBreadcrum[key]}
            style={{
              textDecoration: "none",
              fontSize: "1rem",
              color: "#ffffff",
            }}
          >
            {`${translate(key)}`}
          </Link>
        </div>
      );
    });
  };

  const breadCrumbNavigation = () => {
    return (
      <Route>
        {({ location }) => {
          const pathnames = location.pathname.split("/").filter((x) => x);
          return (
            <Breadcrumbs aria-label="Breadcrumb" separator='>' style={{color: 'white'}}>
              {pathnames.map((value, index) => {
                const last = index === pathnames.length - 1;
                const to = `/${pathnames.slice(0, index + 1).join("/")}`;

                return last ? (
                  <Typography color="textPrimary" key={to} style={{color: 'white', textDecoration: 'none', fontFamily: 'Bebas-Bold', fontSize: '20px'}}>
                    {value}
                  </Typography>
                ) : (
                  <Link color="inherit" to={to} key={to} style={{color: '#CCC', textDecoration: 'none', fontFamily: 'Bebas-Bold', fontSize: '20px'}}>
                    {value}
                  </Link>
                );
              })}
            </Breadcrumbs>
          );
        }}
      </Route>
    );
  };

  useEffect(() => {
    if (userLogged) {
      console.log("userLogged", userLogged);
    }
  }, [userLogged]);

  return (
    <Grid
      container
      direction="row"
      className={`background-main${className ? " " + className : ""}`}
    >
      <Grid item xs={1} className="side-bar-container">
        <div className="side-bar-container-head">
          <div style={{ minHeight: "2rem", width: "2.5rem" }}>
            <div
              style={{
                top: "0",
                left: "-1rem",
                width: "6rem",
                height: "2rem",
                position: "relative",
                background: "#102E2F",
                transformOrigin: "3rem 0",
                transform: "rotate(-45deg)",
              }}
            ></div>
          </div>
          <div style={{ width: "100%", background: "#102E2F" }}></div>
        </div>
        <div
          className="side-bar-container-body"
          style={{ minHeight: "100px", background: "#102E2F" }}
        >
          <div className="side-bar-logo">
            <img
              alt={userLogged.roleByDefaultClub.name}
              className="img-logo"
              src={userLogged.roleByDefaultClub.img_file}
            ></img>
          </div>
          <div className="side-bar-title">
          <p className="side-title">
              {userLogged.name} {userLogged.lastname}
            </p>
            <p className="side-title">{userLogged.roleByDefaultClub.name}</p>
            <div style={{padding: '1rem'}}>
              {breadCrumbNavigation()}
            </div>
          </div>
          <div style={{ padding: "0.5rem" }}></div>
        </div>
        <div className="side-bar-container-footer">
          <div style={{ minHeight: "2rem", width: "2.5rem" }}>
            <div
              style={{
                top: "0",
                left: "-1rem",
                width: "6rem",
                height: "2rem",
                position: "relative",
                background: "#102E2F",
                transformOrigin: "3rem 0",
                transform: "rotate(-45deg)",
              }}
            ></div>
          </div>
          <div style={{ width: "100%", background: "#102E2F" }}></div>
        </div>
      </Grid>

      <Grid item md={11} className="section-container">
        <div
          className="section-container-head"
          style={{ minHeight: "2rem", display: "flex", overflow: "hidden" }}
        >
          <div
            style={{
              background: "#102E2F",
              maxWidth: "22%",
              padding: "5px 4rem 0",
              display: "flex",
              alignItems: "center",
              fontSize: "1.1rem",
            }}
          >
            {breadcrumActive}
          </div>
          <div
            style={{
              minWidth: "6rem",
              transform: "rotate(45deg)",
              transformOrigin: "0 0",
              background: "#102E2F",
            }}
          ></div>
          <div></div>
        </div>
        <div className="section-container-body">
          {breadcrumVisible && (
            <div className="container-breadcrum">{buildBreadcrum()}</div>
          )}
          <div className="section-body">{children}</div>
        </div>
      </Grid>
    </Grid>
  );
};
export default NavBar;
