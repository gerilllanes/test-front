import React, { useState } from "react";
import Section from "../../../components/Section/Section";
import Tabs from "../components/Tabs";

const TeamsTable = () => {
  const headers = ["APELLIDO", "NOMBRE", "ROL", "TELEFONO", "EMAIL"];
  const data = [
    {
      apellido: "illanes",
      nombre: "german",
      rol: "el capi",
      telefono: "333",
      email: "germi@ger.com",
    },
    {
      apellido: "illanes",
      nombre: "german",
      rol: "el capi",
      telefono: "333",
      email: "germi@ger.com",
    },
    {
      apellido: "illanes",
      nombre: "german",
      rol: "el capi",
      telefono: "333",
      email: "germi@ger.com",
    },
    {
      apellido: "illanes",
      nombre: "german",
      rol: "el capi",
      telefono: "333",
      email: "germi@ger.com",
    },
  ];
  return (
    <Section title="Teams">
      <Tabs data={data} headers={headers}/>
    </Section>
  );
};

export default TeamsTable;
