import React, { useState } from "react";
import { Grid, Tab, Tabs } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Edit from "../../../components/Section/Edit";
import Section from "../../../components/Section/Section";
import TextBox from "../../../components/Section/TextBox";
import AddButton from "../../Admin/AdminClub/components/AddButton";
import TableDynamic from "../../Admin/AdminClub/components/TableDynamic";

const useStyles = makeStyles((theme) => ({
  tabs: { fontFamily: "Bebas-Bold", backgroundColor: "rgba(33, 33, 33, 0.5)" },
  title: { padding: 6 },
}));

const TeamsTable = ({ data, headers }) => {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const handleTabs = (e, val) => {
    setValue(val);
  };

  return (
    <>
      <Tabs value={value} onChange={handleTabs}>
        <Tab className={classes.tabs} label="Equipo 1" />
        <Tab className={classes.tabs} label="Equipo 2" />
        <Tab className={classes.tabs} label="Equipo 3" />
      </Tabs>
      <TabPanel value={value} index={0}>
        <h2 className={classes.title}>
          INFORMACIÓN GENERAL <Edit />
        </h2>
        <Grid container spacing={2}>
          <Grid Grid item xs={12} sm={12} md={4} lg={4}>
            <Section title={`EQUIPO ${value}`}>
              <TextBox text="Nombre:" />
              <TextBox text="Nombre corto:" />
              <TextBox text="Tipo equipo:" />
              <TextBox text="Grupo de edad:" />
              <TextBox text="genero equipo:" />
              <TextBox text="nivel de practica:" />
              <TextBox text="delegado de equipo:" />
              <TextBox text="ciudad:" />
              <TextBox text="direccion:" />
              <TextBox text="creacion:" />
            </Section>
          </Grid>
          <Grid Grid item xs={12} sm={12} md={4} lg={4}>
            <Section title="CLUB">
              <TextBox text="Nombre Club:" />
              <TextBox text="Deporte:" />
              <TextBox text="pais:" />
              <TextBox text="director tecnico:" />
              <TextBox text="preparador fisico:" />
              <TextBox text="medico:" />
              <TextBox text="videoanalista:" />
              <TextBox text="redes sociales:" />
              <TextBox text="sponsor:" />
            </Section>
          </Grid>
          <Grid Grid item xs={12} sm={12} md={4} lg={4}>
            <Section title="COMPETICION">
              <TextBox text="Nombre:" />
              <TextBox text="Confederacion:" />
              <TextBox text="federacion:" />
              <TextBox text="categoria:" />
              <TextBox text="formato:" />
              <TextBox text="temporadas jugadas:" />
            </Section>
          </Grid>
          <Grid Grid item xs={12} sm={12} md={12} lg={12}>
            <AddButton />
          </Grid>
        </Grid>
        PLANTEL <Edit />
        <TableDynamic data={data} headers={headers} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <h2 className={classes.title}>
          INFORMACIÓN GENERAL <Edit />
        </h2>
        <Grid container spacing={2}>
          <Grid Grid item xs={12} sm={12} md={4} lg={4}>
            <Section title={`EQUIPO ${value}`}>
              <TextBox text="Nombre:" />
              <TextBox text="Nombre corto:" />
              <TextBox text="Tipo equipo:" />
              <TextBox text="Grupo de edad:" />
              <TextBox text="genero equipo:" />
              <TextBox text="nivel de practica:" />
              <TextBox text="delegado de equipo:" />
              <TextBox text="ciudad:" />
              <TextBox text="direccion:" />
              <TextBox text="creacion:" />
            </Section>
          </Grid>
          <Grid Grid item xs={12} sm={12} md={4} lg={4}>
            <Section title="CLUB">
              <TextBox text="Nombre Club:" />
              <TextBox text="Deporte:" />
              <TextBox text="pais:" />
              <TextBox text="director tecnico:" />
              <TextBox text="preparador fisico:" />
              <TextBox text="medico:" />
              <TextBox text="videoanalista:" />
              <TextBox text="redes sociales:" />
              <TextBox text="sponsor:" />
            </Section>
          </Grid>
          <Grid Grid item xs={12} sm={12} md={4} lg={4}>
            <Section title="COMPETICION">
              <TextBox text="Nombre:" />
              <TextBox text="Confederacion:" />
              <TextBox text="federacion:" />
              <TextBox text="categoria:" />
              <TextBox text="formato:" />
              <TextBox text="temporadas jugadas:" />
            </Section>
          </Grid>
          <Grid Grid item xs={12} sm={12} md={12} lg={12}>
            <AddButton />
          </Grid>
        </Grid>
        PLANTEL <Edit />
        <TableDynamic data={data} headers={headers} />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <h2 className={classes.title}>
          INFORMACIÓN GENERAL <Edit />
        </h2>
        <Grid container spacing={2}>
          <Grid Grid item xs={12} sm={12} md={4} lg={4}>
            <Section title={`EQUIPO ${value}`}>
              <TextBox text="Nombre:" />
              <TextBox text="Nombre corto:" />
              <TextBox text="Tipo equipo:" />
              <TextBox text="Grupo de edad:" />
              <TextBox text="genero equipo:" />
              <TextBox text="nivel de practica:" />
              <TextBox text="delegado de equipo:" />
              <TextBox text="ciudad:" />
              <TextBox text="direccion:" />
              <TextBox text="creacion:" />
            </Section>
          </Grid>
          <Grid Grid item xs={12} sm={12} md={4} lg={4}>
            <Section title={`CLUB ${value}`}>
              <TextBox text="Nombre Club:" />
              <TextBox text="Deporte:" />
              <TextBox text="pais:" />
              <TextBox text="director tecnico:" />
              <TextBox text="preparador fisico:" />
              <TextBox text="medico:" />
              <TextBox text="videoanalista:" />
              <TextBox text="redes sociales:" />
              <TextBox text="sponsor:" />
            </Section>
          </Grid>
          <Grid Grid item xs={12} sm={12} md={4} lg={4}>
            <Section title={`COMPETICION ${value}`}>
              <TextBox text="Nombre:" />
              <TextBox text="Confederacion:" />
              <TextBox text="federacion:" />
              <TextBox text="categoria:" />
              <TextBox text="formato:" />
              <TextBox text="temporadas jugadas:" />
            </Section>
          </Grid>
          <Grid Grid item xs={12} sm={12} md={12} lg={12}>
            <AddButton />
          </Grid>
        </Grid>
        PLANTEL <Edit />
        <TableDynamic data={data} headers={headers} />
      </TabPanel>
    </>
  );
};

function TabPanel(props) {
  const { children, value, index } = props;
  return <div>{value === index && <h1>{children}</h1>}</div>;
}

export default TeamsTable;
