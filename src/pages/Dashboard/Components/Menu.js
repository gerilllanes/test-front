import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

import translate from "../../../lang/lang";
import { Container, Grid } from "@material-ui/core";
import {getRandomColor} from "../../../utils/utils";
import RowResponsive from "../../../components/RowResponsive/RowResponsive";

const useStyles = makeStyles( () => ({
  flexcontainer: {
    display: "flex",
    padding: 0,
  },
  flexitem: {
    width: "10px",
    color: "white",
    height: "170px",
    marginTop: "5px",
    cursor: "pointer",
    marginLeft: "5px",
    marginRight: "5px",
    textAlign: "center",
    fontFamily: "Bebas-Bold",
    border: "3px solid white",
    backgroundColor: "#2d4144ab",
  },
  informacion: {
    color: "white",
  },
  link: {
    textDecoration: "none",
  },
  tagContainer: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  tagIcon: {
    width: '60px',
    height: '50px',
    display: 'flex',
    alignItems: 'center',
    margin: '0 1rem 1rem',
    justifyContent: 'center',
    clipPath: 'polygon(100% 0, 100% 75%, 50% 100%, 0 75%, 0 0)'
  }
}));

const infoModules = {
  panel_admin: { url: "/admin/users" },
  make_evaluation: { url: "/make-evaluation" },
  evaluation: { url: "/evaluation" },
  gps: { url: "/gps" },
  video_analysis: { url: "/video-analysis" },
  player: { url: "/player"},
  user: { url: "/user" },
  sponsor: { url: "/sponsor" },
  team: { url: "/team" },
  club: { url: "/club" },
  category: { url: "/category" },
  competition: { url: "/competition" },
  sport: { url: "/sport" }
};

const Menu = ({modules}) => {

  const classes = useStyles();

  const buildAcronym = (string) => {
    const matches = string.match(/\b(\w)/g); // ['J','S','O','N']
    return matches.join('');
  }

  const buildStyleRandomColor = () => {
    const color = getRandomColor();
    return {background: color, color: color}
  }

  const buildModuleTags = () => {
    
    return Object.keys(modules).map((inf, i) => {
      return (
        <Grid
          item
          xs={12}
          sm={4}
          md={6}
          lg={5}
          key={`${modules[inf]}-${i}`}
          className={classes.flexitem}
        >
          <Link
            to={infoModules[inf] ? infoModules[inf].url : "dashboard"}
            style={{ textDecoration: "none", fontSize: '1.4rem' }}
          >
          <div className={classes.tagContainer}>
              <div className={classes.tagIcon} style={buildStyleRandomColor()}><span style={{filter: 'invert(100%)', fontSize: '2rem'}}>{buildAcronym( translate(inf) )}</span></div>
              <div className={classes.informacion}>{`${ translate(inf) }`}</div>
          </div>
          </Link>
        </Grid>
      );
    });
  }

  return (
  <Container maxWidth="xl" className={classes.flexcontainer}>
    <Grid container spacing={1} className="dash-container">
      <RowResponsive 
        items={buildModuleTags()}
        className={'dash'}
        xs={{cols: 2, margin: 15 }}
        sm={{cols: 3, margin: 15 }}
        md={{cols: 4, margin: 20 }}
        lg={{cols: 5, margin: 15 }}
        xl={{cols: 6, margin: 15 }}
      />
    </Grid>
  </Container>
  );
};
export default Menu;