import React, { useContext } from "react";

import Menu from "./Components/Menu";
import UserContext from "../../context/UserContext/UserContext";

const Dashboard = () => {
  const userContext = useContext(UserContext);
  const modules = userContext.userModules;
  const user = userContext.userLogged;
  const fixModules = user.roleByDefaultClub.type === 'ROLE_CLUB_ADMIN' ? { ...{panel_admin: ["1", "2", "3", "4"]}, ...modules} : modules;
  return <Menu modules={fixModules} />;
};
export default Dashboard;