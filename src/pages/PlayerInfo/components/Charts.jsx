import LinearChart from "../../../components/LinearChart/LinearChart";
import RadarChartInfo from "../../../components/RadarChart/RadarChartInfo";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  // chart: {
  //   width: "100%",
  //   height: "25%",
  //   paddingTop: "30px",
  // },
}));

const Charts = ({
  dataRadar,
  data,
  handleClickYear,
  handleClickMonth,
  handleClickWeek,
  handleClickDay,
  handleClickLast,
  handleClickStaff,
  handleClickPlayer,
  handleClickAll,
  skillsNames,
  handleClickAutoevaluation,
  selectedDateFrom,
  inputValueFrom,
  dateFormatterFrom,
  onDateChangeFrom,
  selectedDateTo,
  inputValueTo,
  onDateChangeTo,
  dateFormatterTo,
  handleClickDates
}) => {
  return (
    <div style={{ display: "flex" }}>
      <RadarChartInfo dataRadar={dataRadar} />
      <div>
        <LinearChart
          data={data}
          handleClickYear={handleClickYear}
          handleClickMonth={handleClickMonth}
          handleClickWeek={handleClickWeek}
          handleClickDay={handleClickDay}
          handleClickLast={handleClickLast}
          handleClickStaff={handleClickStaff}
          handleClickPlayer={handleClickPlayer}
          handleClickAll={handleClickAll}
          handleClickAutoevaluation={handleClickAutoevaluation}
          skillsNames={skillsNames}
          selectedDateFrom={selectedDateFrom}
          inputValueFrom={inputValueFrom}
          dateFormatterFrom={dateFormatterFrom}
          onDateChangeFrom={onDateChangeFrom}
          dateFormatterTo={dateFormatterTo}
          selectedDateTo={selectedDateTo}
          inputValueTo={inputValueTo}
          onDateChangeTo={onDateChangeTo}
          handleClickDates={handleClickDates}
        />
      </div>
    </div>
  );
};

export default Charts;
