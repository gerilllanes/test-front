import field from "../../../assets/img/notposition.png";
const Field = () => {
  return (
    <div style={{ float: "right", marginRight: "3em" }}>
      <img
        src={field}
        alt="field"
        style={{
          display: "block",
          top: "3rem",
          right: "6rem",
          width: "75%",
          height: "55%",
        }}
      />
    </div>
  );
};

export default Field;
