import './Header.css'

const Header = ({playerInfo}) => {
    return (
        <div className='header-container'>
            <img alt="user" src='https://www.fcaktp.cu/wp-content/uploads/2019/05/default-user-image.png' className='player-image'/>
            <div className='title'>
                {playerInfo.name + ' ' + playerInfo.lastname}
            </div>
        </div>
    )
}
export default Header;