import { useState, useEffect, useContext } from "react";

import Field from "./components/Field";
import Header from "./components/Header";
import Grid from "@material-ui/core/Grid";
import Charts from "./components/Charts";
import PlayerEvaluation from "../../components/Evaluation/PlayerEvaluation";
import moment from "moment";

import { useParams } from "react-router-dom";

//CONTEXT
import PlayerContext from "../../context/PlayerContext/PlayerContext";
import SkillsContext from "../../context/SkillsContext/SkillsContext";
import MakeEvaluationWithVote from "../MakeEvaluation/MakeEvaluationWithVote";

const PlayerValoration = () => {
  const [skillNameState, setSkillNameState] = useState([]);
  const [normalizeArrayState, setNormalizeArrayState] = useState([]);
  const [normalizeArraySkills, setNormalizeArraySkills] = useState([]);
  const [params, setParams] = useState("");
  const playerContext = useContext(PlayerContext);
  const skillsContext = useContext(SkillsContext);
  const [selectedDateFrom, setDateFrom] = useState(moment());
  const [selectedDateTo, setDateTo] = useState(moment());
  const [inputValueFrom, setInputValueFrom] = useState(
    moment().format("DD-MM-YYYY")
  );
  const [inputValueTo, setInputValueTo] = useState(
    moment().format("DD-MM-YYYY")
  );

  const onDateChangeFrom = (date, value) => {
    setDateFrom(date);
    setInputValueFrom(value);
  };

  const onDateChangeTo = (date, value) => {
    setDateTo(date);
    setInputValueTo(value);
  };

  const dateFormatterFrom = (str) => {
    return str;
  };

  const dateFormatterTo = (str) => {
    return str;
  };

  const { getPlayerInfo, playerInfo } = playerContext;
  let {
    skillAverage,
    skillHistorical,
    skillLast,
    getSkillHistorical,
    getSkillAverage,
    getSkillLast,
  } = skillsContext;

  const { id } = useParams();

  let date = new Date();
  let day = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();

  useEffect(() => {
    getPlayerInfo(id);
    getSkillLast(id);
    getSkillAverage(`?user_voted=${id}`);
    getSkillHistorical(`?user_voted=${id}` + params);
  }, [params, id]);

  let dataRadar;
  let normalizedObject = {};
  let skillsNames = [];
  let normalizedArray = [];
  let resultados = [];
  let lastDate = [];
  let newSkillsNames = [];

  if (skillLast) {
    skillLast.map((date) => {
      lastDate = date.date_evaluation;
    });
  }
  useEffect(() => {
    if (skillHistorical) {
      functions();
    }
  }, [skillHistorical]);

  const functions = () => {
    if (skillHistorical) {
      skillHistorical.forEach((element) => {
        Object.keys(element.dates).forEach((date) => {
          if (normalizedObject[date]) {
            let normalizedTemp = {
              ...normalizedObject[date],
              ...{ name: date, [element.name]: element.dates[date].average },
            };
            normalizedObject = { ...normalizedObject, [date]: normalizedTemp };
          } else {
            normalizedObject = {
              ...normalizedObject,
              [date]: {
                name: date,
                [element.name]: element.dates[date].average,
              },
            };
          }
          if (!skillsNames.includes(element.name)) {
            skillsNames = [...skillsNames, element.name];
          }
        });
      });
      setSkillNameState(skillsNames);
      Object.keys(normalizedObject).forEach((key) => {
        normalizedArray = [...normalizedArray, normalizedObject[key]];
      });
      setNormalizeArrayState(normalizedArray);
      resultados = skillHistorical.flatMap(({ dates }) => {
        return Object.entries(dates).map(([name, value]) =>
          Object.assign(
            { name },
            ...value.skills.map(({ name, avg }) => ({
              [name]: parseFloat(avg),
            }))
          )
        );
      });
      console.log("resultados", resultados);
      setNormalizeArraySkills(resultados);
    }
  };

  if (skillAverage) {
    dataRadar = skillAverage.map((avg) => {
      avg.skills.map((average) => {
        return { name: average.name, avg: average.avg };
      });
      return { subject: avg.name, A: avg.average };
    });
  }

  const handleClickYear = () => {
    setParams(`&from_date=01-01-${year - 1}&to_date=31-12-${year}`);
  };

  const handleClickMonth = () => {
    setParams(
      `&from_date=${day}-${"0" + month - 1}-${year}&to_date=${day}-${
        "0" + month
      }-${year}`
    );
  };

  const handleClickWeek = () => {
    setParams(
      `&from_date=${day - 7}-${month}-${year}&to_date=${day}-${month}-${year}`
    );
  };

  const handleClickDay = () => {
    setParams(
      `&from_date=${day - 1}-${month}-${year}&to_date=${day}-${month}-${year}`
    );
  };

  const handleClickAll = () => {
    setParams(``);
  };

  const handleClickStaff = () => {
    setParams(`&voted_by_role=ROLE_ADMIN`);
  };

  const handleClickPlayer = () => {
    setParams(`&voted_by_role=ROLE_PLAYER`);
  };

  const handleClickAutoevaluation = () => {
    setParams(`&user_voter=${id}`);
  };

  const handleClickLast = () => {
    setParams(`&from_date=${lastDate}`);
  };

  const handleClickDates = () => {
    setParams(`&from_date=${inputValueFrom}&to_date=${inputValueTo}`);
  };

  const handleChangeLine = () => {
    setNormalizeArrayState(normalizeArraySkills);
    setSkillNameState([
      "acceleration",
      "agility",
      "ball control",
      "coordination",
      "jumping",
      "physical structure",
      "reactivity",
      "sprint speed",
      "stamina",
      "strength",
      "tanding elevation",
      "ball control",
      "dribbling",
      "free kick",
      "heading",
      "left foot",
      "long pass",
      "right foot",
      "shoot accurancy",
      "short pass",
      "shot power",
      "tactical skills",
      "authoritative",
      "concentration",
      "marking",
      "position",
      "unmarking",
      "vision",
      "concentration",
      "grounds",
      "spirit of sacrifice",
    ]);
  };

  return (
    <>
      <MakeEvaluationWithVote />
      <div style={{ width: "100%", overflow: "hidden", height: "100%" }}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={8} md={8} lg={8} xl={8}>
            <Header playerInfo={playerInfo} />
          </Grid>
          <Grid item xs={12} sm={8} md={4} lg={4} xl={4}>
            <Field />
          </Grid>
          <Grid item xs={12} sm={8} md={8} lg={8} xl={8}>
            <Charts
              skillsNames={skillNameState}
              dataRadar={dataRadar}
              data={normalizeArrayState}
              handleClickYear={handleClickYear}
              handleClickMonth={handleClickMonth}
              handleClickWeek={handleClickWeek}
              handleClickDay={handleClickDay}
              handleClickLast={handleClickLast}
              handleClickStaff={handleClickStaff}
              handleClickPlayer={handleClickPlayer}
              handleClickAll={handleClickAll}
              handleClickAutoevaluation={handleClickAutoevaluation}
              selectedDateFrom={selectedDateFrom}
              selectedDateTo={selectedDateTo}
              inputValueFrom={inputValueFrom}
              inputValueTo={inputValueTo}
              onDateChangeFrom={onDateChangeFrom}
              onDateChangeTo={onDateChangeTo}
              dateFormatterFrom={dateFormatterFrom}
              dateFormatterTo={dateFormatterTo}
              handleClickDates={handleClickDates}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
            <PlayerEvaluation
              handleChangeLine={handleChangeLine}
              skillAverage={skillAverage}
            />
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default PlayerValoration;
