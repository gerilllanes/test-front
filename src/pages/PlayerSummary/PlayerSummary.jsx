import { useEffect, useContext } from "react";

import Field from "../PlayerInfo/components/Field";
import Header from "../PlayerInfo/components/Header";
import PlayerResume from "./components/PlayerResume";
import PlayerIndicators from "./components/PlayerIndicators";
import Grid from "@material-ui/core/Grid";
import cancha from "../../assets/img/cancha.png";

import { useParams } from "react-router-dom";

//CONTEXT
import PlayerContext from "../../context/PlayerContext/PlayerContext";
import RadarChartInfo from "../../components/RadarChart/RadarChartInfo";
import PlayerCards from "./components/PlayerCards";

const PlayerSummary = () => {
  const playerContext = useContext(PlayerContext);

  const { getPlayerInfo, playerInfo } = playerContext;

  const { id } = useParams();

  useEffect(() => {
    getPlayerInfo(id);
  }, []);

  return (
    <div style={{ width: "100%", overflow: "hidden" }}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={8} md={8} lg={8} xl={8}>
          <Header playerInfo={playerInfo} />
        </Grid>
        <Grid item xs={12} sm={8} md={4} lg={4} xl={4}>
          <Field />
        </Grid>
        <Grid item xs={12} sm={8} md={8} lg={8} xl={8}>
          <PlayerResume />
        </Grid>
        <Grid item xs={12} md={12} lg={12}>
          <PlayerIndicators player={playerInfo} />
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4} xl={4}>
          <img src={cancha} alt="" style={{ height: "50%" }} />
        </Grid>
        <Grid item xs={12} sm={4} md={4} lg={4} xl={4}>
          <RadarChartInfo />
        </Grid>
        <Grid item xs={12} md={12} lg={4}>
          <PlayerCards />
        </Grid>
      </Grid>
    </div>
  );
};

export default PlayerSummary;
