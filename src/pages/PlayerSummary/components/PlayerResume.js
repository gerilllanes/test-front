import { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Grid } from "@material-ui/core";

import translate from "../../../lang/lang";

const useStyles = makeStyles(() => ({
  name: {
    fontSize: "24px",
    fontWeight: "bold",
    fontFamily: "Bebas-Bold",
    color: "#ffffff",
  },
  AppBar: {
    backgroundColor: "#1E4249",
    position: "relative",
  },
  text: {
    marginTop: "2px",
    fontFamily: "Bebas-Bold",
  },
  p: {
    fontSize: "1rem",
    padding: "5px",
    fontFamily: "Bebas-Bold",
  },
}));

const BasicPlayerInfo = ({ hiddenText }) => {
  const classes = useStyles();

  return (
    <div>
      <Grid container>
        <Grid item xs={6} md={6} lg={6}>
          {hiddenText ? (
            <></>
          ) : (
            <div className={classes.text}>
              <Typography className={classes.p}>
                {translate("nationality")}: - {translate("birthday")}: -{" "}
                {translate("male")} - {translate("dni")}:
              </Typography>
              <Typography className={classes.p}>
                {translate("passport")}:
              </Typography>
              <Typography className={classes.p}>
                {translate("telephone")} :
              </Typography>
              <Typography className={classes.p}>
                {translate("father")}/{translate("tutor")}:
              </Typography>
            </div>
          )}
        </Grid>
        <Grid item xs={6} md={6} lg={6}>
          {hiddenText ? (
            <></>
          ) : (
            <div className={classes.text}>
              <Typography className={classes.p}>
                {translate("market")}:
              </Typography>

              <Typography className={classes.p}>
                {translate("Federal rights")}:
              </Typography>

              <Typography className={classes.p}>
                {translate("Económic rights")}:
              </Typography>

              <Typography className={classes.p}>
                {translate("Image rights")}:
              </Typography>
            </div>
          )}
        </Grid>
      </Grid>
    </div>
  );
};

export default BasicPlayerInfo;
