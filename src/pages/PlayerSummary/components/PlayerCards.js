import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(() => ({
  container: {
    marginLeft: "30px",
  },
  cardHeader: {
    color: "white",
    borderRadius: "0",
    margin: "auto",
    padding: "10px",
    fontSize: "8px",
    fontFamily: "Bebas-Bold ",
  },
  cardDescription: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#315059",
    color: "cyan",
    height: "85px",
  },
  row1: {
    display: "inline-block",
    margin: "5px",
  },
  font: {
    fontFamily: "Bebas-Bold",
    color: "cyan",
  },
  fontText: {
    fontFamily: "Bebas-Bold",
    color: "white",
  },
  cardContent: {
    padding: "0 5px 5px !important",
  },
}));

const PlayerCards = ({ metrics, mediaQueryOnlyLg }) => {
  let color;
  const classes = useStyles();
  return (
    <>
      <div
        className={classes.container}
        style={{
          position: mediaQueryOnlyLg ? "absolute" : "",
          marginLeft: mediaQueryOnlyLg ? "30px" : "0px",
          marginTop: mediaQueryOnlyLg ? "0px" : "40px",
          textAlign: mediaQueryOnlyLg ? "" : "center",
        }}
      >
        <div className={classes.row1}>
          <Card
            style={{
              borderRadius: "0",
              width: "200px",
            }}
          >
            <CardHeader
              title={"Holis"}
              titleTypographyProps={{
                align: "center",
                variant: "body1",
                style: {
                  fontFamily: "Bebas-Bold",
                },
              }}
              subheaderTypographyProps={{ align: "center" }}
              className={classes.cardHeader}
              style={{
                backgroundColor: color,
              }}
            />
            <CardContent
              className={classes.cardContent}
              style={{ backgroundColor: color }}
            >
              <div className={classes.cardDescription}>
                <Typography
                  className={classes.font}
                  component="h2"
                  variant="h3"
                >
                  XXXXX
                </Typography>
                <Typography
                  className={classes.fontText}
                  variant="h6"
                  style={{ marginLeft: "15px", color: "#fff" }}
                >
                  XXXXX
                </Typography>
              </div>
            </CardContent>
          </Card>
        </div>
        {/* {metrics.map((metric, index) => {
          let distance = null;
          if (index === 2 || index === 3 || index === 5) {
            distance =
              metric.value >= 1000 ? metric.value / 1000 : metric.value;
          }
          switch (index) {
            case 0:
              color = "#f92f3b";
              break;
            case 2 || 3:
              color = "#28bb29";
              break;
            case 4 || 5:
              color = "#2594b4";
              break;
            default:
          }
          return (
            <div className={classes.row1} key={index}>
              <Card
                style={{
                  borderRadius: "0",
                  width: "200px",
                }}
              >
                <CardHeader
                  title={metric.label}
                  titleTypographyProps={{
                    align: "center",
                    variant: "body1",
                    style: {
                      fontFamily: "Bebas-Bold",
                    },
                  }}
                  subheaderTypographyProps={{ align: "center" }}
                  className={classes.cardHeader}
                  style={{
                    backgroundColor: color,
                  }}
                />
                <CardContent
                  className={classes.cardContent}
                  style={{ backgroundColor: color }}
                >
                  <div className={classes.cardDescription}>
                    <Typography
                      className={classes.font}
                      component="h2"
                      variant="h3"
                    >
                      {distance === null ? metric.value : distance.toFixed(2)}
                    </Typography>
                    <Typography
                      className={classes.fontText}
                      variant="h6"
                      style={{ marginLeft: "15px", color: "#fff" }}
                    >
                      {distance === null ? metric.unit : `K${metric.unit}`}
                    </Typography>
                  </div>
                </CardContent>
              </Card>
            </div>
          );
        })} */}
      </div>
    </>
  );
};

export default PlayerCards;
