import React, { useContext, useState } from "react";
import UserContext from "../../context/UserContext/UserContext";
import AlertSuccess from "../../components/Alerts/AlertSuccess";
import AlertError from "../../components/Alerts/AlertError";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import swal from "sweetalert";

const useStyles = makeStyles((theme) => ({
  form: {
    width: "100%", // Fix IE 11 issue.
    height: "100%",
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Recover = () => {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const [succesAlert, setSuccesAlert] = useState(false);
  const [errorAlert, setErrorAlert] = useState(false);
  const userContext = useContext(UserContext);
  const { putRecoverPassword, recoverPassword } = userContext;

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    putRecoverPassword("email=" + email)
      .then(() => {
        if (recoverPassword) {
          console.log(recoverPassword);
          setEmail("");
          setLoading(false);
          swal({
            title: "Done!",
            text: recoverPassword.data.data,
            icon: "success",
          });
        }
      })
      .catch(() => {
        setErrorAlert(true);
        setLoading(false);
      });
  };

  return (
    <>
      {succesAlert && (
        <AlertSuccess text="Email con su nueva contraseña enviado con exito" />
      )}

      {errorAlert && (
        <AlertError
          text="No se pudo enviar el mail con su nueva contraseña. Por favor intentelo
          de nuevo."
        />
      )}
      <form className={classes.form} noValidate>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          label="Correo Electronico"
          name="email"
          onChange={(event) => setEmail(event.target.value)}
          value={email}
        />
        {/* <Footer primaryText="Go back" primaryRoute="/login" /> */}

        {loading ? (
          <CircularProgress />
        ) : (
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={(e) => handleSubmit(e)}
          >
            Recuperar Contraseña
          </Button>
        )}
      </form>
    </>
  );
};

export default Recover;
