import { useEffect, useContext } from "react";

import TableList from "../../components/TableList/TableList";
import Avatar from "@material-ui/core/Avatar";
import { useHistory } from "react-router-dom";

//CONTEXT
import PlayerContext from "../../context/PlayerContext/PlayerContext";

const Player = () => {
  const playerContext = useContext(PlayerContext);
  const { getPlayers, players } = playerContext;

  const history = useHistory();

  const data = players.map((datas) => ({
    Photo: <Avatar alt={datas.user.name} src={datas.user.img_file} />,
    ID: datas.id,
    Name: `${datas.user.name} ${datas.user.lastname}`,
    Team: datas.team.name,
    Foot: datas.skillful_foot,
  }));
  //setPlayerId(datas.ID)

  const goToPlayerInfo = (id) => {
    console.log("id", id);
    history.push(`/player/summary/${id}`);
  };

  const columns = [
    {
      title: "",
      field: "Photo",
    },
    {
      title: "Name",
      field: "Name",
    },
    {
      title: "Team",
      field: "Team",
    },
    {
      title: "Foot",
      field: "Foot",
    },
  ];

  useEffect(() => {
    getPlayers();
  }, []);

  return (
    <div>
      <TableList
        columns={columns}
        data={data}
        rowClick={goToPlayerInfo}
      />
    </div>
  );
};

export default Player;
