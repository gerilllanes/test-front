import React, { useState, useEffect, useContext } from "react";
import PlayerContext from "../../context/PlayerContext/PlayerContext";
import EvaluationContext from "../../context/EvaluationContext/EvaluationContext";
import CreateEvaluationDialog from "./components/CreateEvaluationDialog";
import SkillsContext from "../../context/SkillsContext/SkillsContext";
import swal from "sweetalert";

const MakeEvaluationWithVote = () => {
  const playerContext = useContext(PlayerContext);
  const evaluationContext = useContext(EvaluationContext);
  const skillsContext = useContext(SkillsContext);
  const { getPlayers, players } = playerContext;
  const { getEvaluationType, evaluationTypes, postEvaluation, evaluation } =
    evaluationContext;
  const { getSkillGroup, skillGroup } = skillsContext;
  const [form, setForm] = useState({
    evaluationType: "",
    userVoted: "",
    subType: "",
    skills: "",
    textNote: "",
    team: 1,
    convocation: "",
  });

  const subType = [
    "pre-match",
    "post-match",
    "pre-training",
    "post-training",
    "spontaneous",
  ];

  const handleOnChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };
  const handleChangeSkillValues = (skills) => {
    setForm({ ...form, skills });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(evaluation);
    postEvaluation(form).then(() => {
      if (evaluation) {
        swal({
          title: "Done!",
          text: "No funciona pa",
          icon: "success",
        });
      }
    });
  };

  useEffect(() => {
    getPlayers();
    getEvaluationType();
    getSkillGroup();
  }, []);

  console.log(form);
  let skills = evaluationTypes.map((skills) => skills.skills_group);
  console.log(skillGroup, "skillGroup");
  return (
    <>
      <CreateEvaluationDialog
        players={players}
        evaluationTypes={evaluationTypes}
        skillGroup={skillGroup}
        subTypesList={subType}
        values={form}
        handleSubmit={handleSubmit}
        onChange={handleOnChange}
        handleChangeSkillValues={handleChangeSkillValues}
      />
    </>
  );
};

export default MakeEvaluationWithVote;
