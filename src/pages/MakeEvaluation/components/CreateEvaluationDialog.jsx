import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  AppBar,
  Dialog,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Toolbar,
  Typography,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import MicIcon from "@material-ui/icons/Mic";
import FabButton from "../../../components/FabButton/FabButton";
import AddIcon from "@material-ui/icons/Add";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import PlayerEvaluationVote from "../../../components/Evaluation/PlayerEvaluationVote";

const useStyles = makeStyles((theme) => ({
  select: {
    marginBottom: "20px",
    fontFamily: "Bebas-Bold !important",
  },
  input: {
    marginTop: theme.spacing(1),
    fontFamily: "Bebas-Bold !important",
  },
  container1: {
    display: "flex",
  },
  contentSelect: {
    width: "100%",
    display: "flex",
    fontFamily: "Bebas-Bold !important",
  },
  contentRecording: {
    width: "30%",
    margin: "auto",
    textAlign: "center",
  },
  recordingBtn: {
    width: "100px",
    height: "100px",
    backgroundColor: "rgb(155, 245, 251)",
    "&:hover": {
      backgroundColor: "rgb(155, 245, 251)",
    },
  },
  recordingIcon: {
    width: "50px",
    height: "50px",
    color: "#424242",
  },
  appBar: {
    background: "none",
  },
  appBarSpacer: theme.mixins.toolbar,
  btn: {
    color: "#E1F5E9",
    bordebrRadius: 0,
    "&:hover": {
      color: "#B1EFF4",
    },
    fontFamily: "Bebas-Bold !important",
  },
  ratingStar: {
    marginTop: "30px",
  },
  openModalBtn: {
    fontFamily: "Bebas-Bold",
  },
  dialogTitle: {
    fontFamily: "Bebas-Bold !important",
    color: "#ffffff",
    marginLeft: "10px",
    flexGrow: 1,
    color: "white",
  },
  comments: {
    marginTop: "10px",
  },
  inputs: {
    color: "white",
    border: "cyan",
  },
}));

const CreateEvaluationDialog = ({
  players,
  evaluationTypes,
  skillGroup,
  subTypesList,
  handleSubmit,
  values,
  onChange,
  handleChangeSkillValues
}) => {
  const [idSkill, setIdSkill] = useState(0);
  const [open, setOpen] = useState(false);
  const classes = useStyles();
  const handleShowSkillGroup = (e) => {
    const skillOptions = e.target.value;
    setIdSkill(skillOptions);
  };
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <FabButton icon={<AddIcon />} handleClick={handleClickOpen} />
      <Dialog open={open} keepMounted onClose={handleClose} fullScreen>
        <div className="background-main">
          <AppBar className={classes.appBar} elevation={0}>
            <Toolbar>
              <Typography variant="h4" className={classes.dialogTitle}>
                Nueva Evaluación
              </Typography>
              <IconButton
                edge="start"
                color="inherit"
                onClick={handleClose}
                aria-label="close"
              >
                <HighlightOffIcon
                  style={{ color: "rgb(155, 245, 251)" }}
                  fontSize="large"
                />
              </IconButton>
            </Toolbar>
          </AppBar>
          <form onSubmit={handleSubmit}>
            <div className={classes.appBarSpacer} />
            <div className={classes.container1}>
              <div className={classes.contentSelect}>
                <FormControl className={classes.select} fullWidth>
                  <InputLabel
                    className={classes.select}
                    id="playerEvaluationLabel"
                    InputLabelProps={{
                      classes: {
                        root: classes.inputs,
                      },
                    }}
                    InputProps={{
                      classes: { root: classes.inputs },
                    }}
                  >
                    Player To Evaluate:{" "}
                  </InputLabel>
                  <Select
                    name="userVoted"
                    labelId="playerEvaluationLabel"
                    label="Player To Evaluate:"
                    autoFocus
                    value={values.userVoted}
                    className={classes.input}
                    onChange={(e) => onChange(e)}
                    InputLabelProps={{
                      classes: {
                        root: classes.inputs,
                      },
                    }}
                    InputProps={{
                      classes: { root: classes.inputs },
                    }}
                  >
                    {players.map((player, i) => {
                      return (
                        <MenuItem
                          className={classes.input}
                          value={player.user.id}
                          key={i}
                        >
                          {player.user.name} {player.user.lastname}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
                <FormControl
                  className={classes.select}
                  style={{ marginLeft: "15px" }}
                  fullWidth
                >
                  <InputLabel
                    className={classes.select}
                    id="labelTypeEvaluation"
                    InputLabelProps={{
                      classes: {
                        root: classes.inputs,
                      },
                    }}
                    InputProps={{
                      classes: { root: classes.inputs },
                    }}
                  >
                    Evaluation Type:{" "}
                  </InputLabel>
                  <Select
                    name="evaluationType"
                    labelId="labelTypeEvaluation"
                    value={values.evaluationType}
                    label="Evaluation Type "
                    className={classes.input}
                    autoFocus
                    onChange={(e) => {
                      onChange(e);
                    }}
                  >
                    {evaluationTypes.map((evaluationType, i) => {
                      return (
                        <MenuItem
                          className={classes.input}
                          value={evaluationType.id}
                          key={i}
                        >
                          {evaluationType.name}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
                <FormControl
                  className={classes.select}
                  style={{ marginLeft: "15px" }}
                  fullWidth
                >
                  <InputLabel
                    className={classes.input}
                    id="labelTypeEvaluation"
                    InputLabelProps={{
                      classes: {
                        root: classes.inputs,
                      },
                    }}
                    InputProps={{
                      classes: { root: classes.inputs },
                    }}
                  >
                    Sub Types:{" "}
                  </InputLabel>
                  <Select
                    name="subType"
                    labelId="labelTypeEvaluation"
                    label="Sub Types "
                    className={classes.input}
                    autoFocus
                    defaultValue=""
                    value={values.subType}
                    onChange={(e) => onChange(e)}
                  >
                    {subTypesList.map((subType, i) => {
                      return (
                        <MenuItem
                          className={classes.input}
                          value={subType}
                          key={i}
                        >
                          {subType}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </div>
              <div className={classes.contentRecording}>
                <IconButton className={classes.recordingBtn}>
                  <MicIcon className={classes.recordingIcon} />
                </IconButton>
              </div>
            </div>
            <PlayerEvaluationVote skillGroup={skillGroup} handleChangeSkillValues={handleChangeSkillValues} />

            <div className={classes.comments}>
              <TextField
                name="textNote"
                label="Comentarios"
                multiline
                rows={4}
                rowsMax={4}
                variant="outlined"
                fullWidth
                className={classes.input}
                value={values.textNote}
                onChange={(e) => onChange(e)}
              />
            </div>
            <Button
              variant="outlined"
              color="primary"
              onClick={handleClose}
              className={classes.btn}
              size="large"
            >
              Cancel
            </Button>
            <Button
              variant="outlined"
              color="primary"
              className={classes.btn}
              size="large"
              type="submit"
            >
              Add Evaluation
            </Button>
          </form>
        </div>
      </Dialog>
    </>
  );
};

export default CreateEvaluationDialog;
