import React, { useState, useEffect } from "react";

import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Collapse from "@material-ui/core/Collapse";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import Box from "@material-ui/core/Box";
import Chip from '@material-ui/core/Chip';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

const useStyles = makeStyles((theme) => ({
  tableContainer: {
    background: "none !important",
    marginLeft: '10px'
  },
  headers: {
    borderBottom: "none !important",
    fontFamily: "Bebas-Bold",
    fontSize: "1.5rem",
  },
  rows: {
    borderBottom: "none !important",
    fontFamily: "Bebas-Bold",
    fontSize: "1.2rem",
    color: "white",
  },
  chip: {
      backgroundColor: "rgb(155, 245, 251)",
      marginRight: '2px',
      marginTop: '2px',
      fontFamily: "Bebas-Bold",
      fontSize: '15px'
  }
}));

const EvaluationRow = ({ evaluation }) => {
  const [open, setOpen] = useState(false);

  const classes = useStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.rows}>
        <TableCell>
          <IconButton size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell className={classes.rows}>
          {evaluation.name}
        </TableCell>
        <TableCell className={classes.rows}>{evaluation.description}</TableCell>
        <TableCell className={classes.rows}>{evaluation.sport}</TableCell>
        <TableCell className={classes.rows}>
          {evaluation.role_voted}
        </TableCell>
        <TableCell className={classes.rows} >
        {evaluation.role_voter.map((rv, i) => (
          <p style={{width: '100%'}} key={i}>{rv}</p>
        ))}
        </TableCell>
        <TableCell className={classes.rows} style={{display: 'flex'}}>
          <IconButton>
            <DeleteIcon style={{color: 'cyan'}} fontSize="small"/>
          </IconButton>
          <IconButton>
            <EditIcon style={{color: 'cyan'}} fontSize="small"/>
          </IconButton>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto">
            <Box margin={1}>
              <TableHead className={classes.headers}>
                <TableRow>
                  <TableCell className={classes.rows}>
                    Grupos de habilidades
                  </TableCell>
                  <TableCell className={classes.rows}>Habilidades</TableCell>
                </TableRow>
                {evaluation.skills_group.map((sg, i) => (
                  <TableRow>
                    <TableCell className={classes.rows} key={i}>{sg.name}</TableCell>
                    <TableCell className={classes.rows}>
                        {
                            sg.skills.map((s,i) => (
                                <Chip label={s.name} className={classes.chip} key={i}/>
                            ))
                        }
                    </TableCell>
                  </TableRow>
                ))}
              </TableHead>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
};

export const EvaluationsTable = ({ getEvaluationType, evaluationTypes }) => {
  const classes = useStyles();
  const headers = [
    "",
    "Name",
    "Description",
    "Sport",
    "Evaluated Profile",
    "Evaluator Profile",
    "Actions",
  ];

  useEffect(() => {
    getEvaluationType();
  }, []);

  return (
    <TableContainer
      elevation={0}
      className={classes.tableContainer}
      variant="outlined"
    >
      <Table>
        <TableHead className={classes.rows}>
          <TableRow className={classes.rows}>
            {headers.map((header, key) => {
              return (
                <TableCell className={classes.headers} key={header}>
                  {header}
                </TableCell>
              );
            })}
          </TableRow>
        </TableHead>
        <TableBody>
          {evaluationTypes.map((evaluation, i) => (
            <EvaluationRow evaluation={evaluation} key={i}/>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default EvaluationsTable;
