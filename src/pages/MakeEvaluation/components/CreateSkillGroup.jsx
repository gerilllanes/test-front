import { useEffect, useState } from "react";

import TextField from "@material-ui/core/TextField";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Chip from "@material-ui/core/Chip";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

const CreateSkillGroup = ({
  evaluationForm,
  handleChangeEvaluation,
  handleAddSkillGroup,
  getSkillGroup,
  skillGroup,
}) => {
  const [id, setId] = useState("");
  const [skill, setSkill] = useState({ name: "" });
  const [skills, setSkills] = useState([]);
  const [skillsGroup, setSkillsGroup] = useState({
    id: "",
    name: "",
    description: "",
    skills: [],
  });
  const [skillGroupSelected, setSkillGroupSelected] = useState("");
  const [viewForm, setViewForm] = useState(false);

  const addNewSkill = () => {
    setSkills((skills) => [...skills, skill]);
  };

  const newSkillGroup = (e) => {
    setSkillsGroup({ ...skillsGroup, [e.target.id]: e.target.value });
  };

  const handleChange = () => {
    setSkillsGroup({ ...skillsGroup, skills: skills });
  };

  useEffect(() => {
    getSkillGroup();
  }, []);

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      {!viewForm ? (
        <>
          <h2>Asignar grupo de habilidades</h2>
          <FormControl>
            <Select value={skillGroupSelected}>
              {skillGroup.map((skill, i) => (
                <MenuItem
                  value={skill.name}
                  onChange={(e) => setSkillGroupSelected(e.target.value)}
                  key={i}
                >
                  {skill.name}
                </MenuItem>
              ))}
            </Select>
            <Button
              variant="outlined"
              onClick={() => setViewForm(true)}
              style={{
                color: "white",
                borderColor: "white",
                marginTop: "0.5rem",
              }}
            >
              Crear un grupo
            </Button>
          </FormControl>
        </>
      ) : (
        <>
          <h2>Crear un grupo</h2>
          <TextField
            id="evaluationType"
            label="Tipo de Evaluacion"
            value={evaluationForm.name}
            disabled
            onChange={(e) => newSkillGroup(e)}
          />
          <TextField
            id="name"
            label="Nombre"
            onChange={(e) => newSkillGroup(e)}
          />
          <TextField
            id="description"
            label="Descripcion"
            onChange={(e) => newSkillGroup(e)}
          />
          <TextField
            id="skills"
            label="Habilidades"
            onChange={(e) => setSkill({ ...skill, id, name: e.target.value })}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton onClick={addNewSkill}>
                    <AddCircleIcon />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
          <div
            style={{
              border: "1px solid",
              width: "95%",
              marginTop: "1rem",
              padding: "1rem",
              marginLeft: "0.5rem",
            }}
          >
            <div style={{ fontSize: "20px" }}>Habilidades a agregar</div>
            {skills.length &&
              skills.map((skill, i) => (
                <Chip
                  label={skill.name}
                  style={{
                    marginTop: "1rem",
                    marginRight: "0.5rem",
                    fontFamily: "Bebas-Bold",
                    fontSize: "15px",
                    backgroundColor: "cyan",
                  }}
                  onDelete={() => console.log("das")}
                  key={i}
                />
              ))}
            <div>
              <Button
                onClick={handleChange}
                variant="outlined"
                style={{
                  color: "white",
                  borderColor: "white",
                  marginTop: "1rem",
                }}
              >
                Crear habilidades
              </Button>
              <Button
                onClick={() => handleAddSkillGroup(skillsGroup)}
                variant="outlined"
                style={{
                  color: "white",
                  borderColor: "white",
                  marginTop: "1rem",
                }}
                disabled={!skillsGroup.length ? true : false}
              >
                Crear grupo de habilidades
              </Button>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default CreateSkillGroup;
