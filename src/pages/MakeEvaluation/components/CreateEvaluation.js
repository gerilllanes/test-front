import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";

const CreateEvaluation = ({
  getEvaluationType,
  evaluationTypes,
  handleChangeEvaluation,
  evaluationForm,
  postEvaluationType,
}) => {

  useEffect(() => {
    getEvaluationType();
  }, []);
  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <div style={{ padding: "1rem" }}>
        <h1>Crear evaluacion</h1>
      </div>
      <TextField
        id="evaluation-name"
        label="Nombre"
        value={evaluationForm.name}
        name="name"
        onChange={(e) => handleChangeEvaluation(e)}
      />
      <TextField id="evaluation-description" label="Descripcion" name="description" onChange={(e) => handleChangeEvaluation(e)}/>
      <FormControl>
        <InputLabel id="evaluation-sport">Deporte</InputLabel>
        <Select labelId="evaluation-sport" id="evaluation-sport" value="">
          <MenuItem value="futbol">Futbol</MenuItem>
        </Select>
      </FormControl>
      <FormControl>
        <InputLabel id="evaluation-evaluate-profile">
          Perfil evaluado
        </InputLabel>
        <Select
          labelId="evaluation-evaluate-profile"
          id="evaluation-evaluate-profile"
          value={evaluationForm.role_voted}
          name="role_voted"
          onChange={(e) => handleChangeEvaluation(e)}
        >
          <MenuItem value="ROLE_PLAYER">ROLE_PLAYER</MenuItem>
        </Select>
      </FormControl>
      <FormControl>
        <InputLabel id="evaluation-evaluator-profile`">
          Perfil evaluador
        </InputLabel>
        <Select labelId="evaluation-sport" id="evaluation-sport" name="role_voter" value={evaluationForm.role_voter} onChange={(e) => handleChangeEvaluation(e)}>
          <MenuItem value="ROLE_PLAYER">ROLE_PLAYER</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
};

export default CreateEvaluation;
