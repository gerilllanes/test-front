import React, { useState, useContext } from "react";

import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import CreateSkillGroup from "./components/CreateSkillGroup";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { makeStyles } from "@material-ui/core/styles";
import EvaluationsTable from "./components/EvaluationsTable";
import CreateEvaluation from "./components/CreateEvaluation";
import EvaluationContext from "../../context/EvaluationContext/EvaluationContext";
import SkillsContext from "../../context/SkillsContext/SkillsContext";

const useStyles = makeStyles((theme) => ({
  button: {
    backgroundColor: "rgb(155, 245, 251)",
    color: "#757575",
    "&:hover": {
      backgroundColor: "rgb(155, 245, 251)",
      color: "#757575",
      cursor: "default",
    },
    width: "50%",
    padding: "5px",
    fontFamily: "Bebas-Bold",
    fontSize: "16px",
  },
  buttonBottom: {
    color: "white",
    borderColor: "white",
  },
}));

const MakeEvaluation = () => {
  const classes = useStyles();
  const [changeView, setChangeView] = useState(false);
  const [evaluationForm, setEvaluationForm] = useState({
    name: "",
    description: "",
    skills_group: [],
    role_voted: "",
    role_voter: "",
    club: "1",
    sport: "1",
  });

  const evaluationContext = useContext(EvaluationContext);
  const skillsContext = useContext(SkillsContext)

  const {
    getEvaluationType,
    evaluationTypes,
    postEvaluationType
  } = evaluationContext;

  const {
    getSkillGroup,
    skillGroup
  } = skillsContext

  const handleChangeEvaluation = (e) => {
    setEvaluationForm({
      ...evaluationForm,
      [e.target.name]: e.target.value,
    });
  };

  const handleAddSkillGroup = (skillGroup) => {
    setEvaluationForm({
      ...evaluationForm,
      skills_group: skillGroup
    })
  }

  console.log(evaluationForm);
  return (
    <>
      <Grid container style={{overflowX: 'hidden'}}>
        <Grid item xs={3} xl={3}>
          <ButtonGroup
            size="small"
            variant="contained"
            color="primary"
            aria-label="small outlined button group"
            disableElevation
            style={{width: '100%'}}
          >
            <Button className={classes.button} disabled={changeView}>
              Evaluacion
            </Button>
            <Button className={classes.button} disabled={!changeView}>
              Grupo de Habilidades
            </Button>
          </ButtonGroup>
          {!changeView ? (
            <CreateEvaluation
              getEvaluationType={getEvaluationType}
              evaluationTypes={evaluationTypes}
              handleChangeEvaluation={handleChangeEvaluation}
              evaluationForm={evaluationForm}
              postEvaluationType={postEvaluationType}
            />
          ) : (
            <CreateSkillGroup
              handleChangeEvaluation={handleChangeEvaluation}
              evaluationForm={evaluationForm}
              getSkillGroup={getSkillGroup}
              skillGroup={skillGroup}
              handleAddSkillGroup={handleAddSkillGroup}
            />
          )}
          <div
            style={
              changeView
                ? {
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: "15px",
                  }
                : { float: "right", marginTop: "15px" }
            }
          >
            {changeView && (
              <Button
                variant="outlined"
                color="secondary"
                onClick={() => setChangeView(!changeView)}
                className={classes.buttonBottom}
              >
                Anterior
              </Button>
            )}
            <Button
              variant="outlined"
              color="secondary"
              onClick={() => setChangeView(!changeView)}
              className={classes.buttonBottom}
            >
              {changeView ? "Crear Evaluacion" : "Siguiente"}
            </Button>
          </div>
        </Grid>
        <Grid item xs={9} xl={9}>
          <EvaluationsTable
            getEvaluationType={getEvaluationType}
            evaluationTypes={evaluationTypes}
          />
        </Grid>
      </Grid>
    </>
  );
};
export default MakeEvaluation;
