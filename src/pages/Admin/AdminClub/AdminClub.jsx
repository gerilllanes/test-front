import AdminLayout from "../../../components/Admin/AdminLayout/AdminLayout";
import SocialData from "./components/SocialData";
import Grid from '@material-ui/core/Grid';
import Sponsors from "./components/Sponsors";
import Achievements from "./components/Achievements";
import DirectiveCommission from "./components/DirectiveCommission";
import Sports from "./components/Sports";
import Headquarters from "./components/Headquarters";
import ActionsButtons from "./components/ActionsButtons";

const AdminClub = () => {
    return (
        <AdminLayout>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={4} lg={4}>
                    <SocialData />
                    <Sponsors />
                    <Achievements />
                </Grid>
                <Grid item xs={12} sm={12} md={8} lg={8}>
                    <DirectiveCommission />
                    <Sports />
                    <Headquarters />
                    <ActionsButtons />
                </Grid>
            </Grid>
        </AdminLayout>
    )
}

export default AdminClub