import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
  buttonOutlined: {
    borderColor: "#5EE8FF",
    borderRadius: 0,
    width: "100%",
    fontFamily: "Bebas-Bold",
    fontSize: 24,
    color: "#5EE8FF",
  },
  buttonContained: {
    backgroundColor: "#5EE8FF",
    borderRadius: 0,
    width: "100%",
    fontFamily: "Bebas-Bold",
    fontSize: 24,
    "&:hover": {
      backgroundColor: "rgba(94, 232, 255, 0.8)",
    },
  },
});

const ActionsButtons = () => {
  const classes = useStyles();
  return (
    <Grid container spacing={2}>
      <Grid item xs={6}>
        <Button variant="outlined" className={classes.buttonOutlined}>
          Cancel
        </Button>
      </Grid>
      <Grid item xs={6}>
        <Button variant="contained" className={classes.buttonContained}>
          Save
        </Button>
      </Grid>
    </Grid>
  );
};

export default ActionsButtons;
