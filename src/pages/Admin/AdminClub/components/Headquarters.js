import Section from '../../../../components/Section/Section'
import AddButton from './AddButton'
import TableDynamic from './TableDynamic'

const Headquarters = () => {
    const headers = [
        'NOMBRE',
        'CAPACIDAD',
        'DOMICILIO',
        'TIPO',
        'HABILITACION'
    ]

    const data = [
        {
            apellido: 'illanes',
            nombre: 'german',
            cargo: 'el capi',
            telefono: '333',
            email: 'germi@ger.com'
        }
    ]

    return (
        <Section title='Headquarters'>
            <TableDynamic headers={headers} data={data}/>
            <AddButton />
        </Section>
    )
}

export default Headquarters