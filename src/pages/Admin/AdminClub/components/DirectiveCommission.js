import Section from '../../../../components/Section/Section'
import AddButton from './AddButton'
import TableDynamic from './TableDynamic'

const DirectiveCommission = () => {
    const headers = [
        'APELLIDO',
        'NOMBRE',
        'CARGO',
        'TELEFONO',
        'EMAIL'
    ]

    const data = [
        {
            apellido: 'illanes',
            nombre: 'german',
            cargo: 'el capi',
            telefono: '333',
            email: 'germi@ger.com'
        },
        {
            apellido: 'illanes',
            nombre: 'german',
            cargo: 'el capi',
            telefono: '333',
            email: 'germi@ger.com'
        },
        {
            apellido: 'illanes',
            nombre: 'german',
            cargo: 'el capi',
            telefono: '333',
            email: 'germi@ger.com'
        },
        {
            apellido: 'illanes',
            nombre: 'german',
            cargo: 'el capi',
            telefono: '333',
            email: 'germi@ger.com'
        },
    ]
    return (
        <Section title='Directive Commission'>
            <TableDynamic headers={headers} data={data} />
            <AddButton />
        </Section>
    )
}

export default DirectiveCommission