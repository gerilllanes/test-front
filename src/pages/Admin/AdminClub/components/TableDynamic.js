import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

const useStyles = makeStyles({
  tableHead: {
    background: "linear-gradient(#1E1E1E, #121212)",
  },
  tableCell: {
    color: "white",
    fontFamily: "Bebas-Bold",
    fontSize: "18px",
    border: "none",
  },
  tableContent: {
    color: "white",
    fontFamily: "Bebas-Bold",
    border: "none",
    fontSize: "18px",
    background: "rgba(33,33,33, 0.8)",
  },
  tableContent2: {
    color: "white",
    fontFamily: "Bebas-Bold",
    border: "none",
    fontSize: "18px",
    background: "rgba(33,33,33, 0.5)",
  },
});

const TableDynamic = ({ headers, data }) => {
  const classes = useStyles();
  return (
    <TableContainer>
      <Table className={classes.table}>
        <TableHead className={classes.tableHead}>
          <TableRow>
            {headers.map((th) => (
              <TableCell align="center" className={classes.tableCell}>
                {th}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((d, i) => {
            let arrValues = [];
            for (const key in d) {
              arrValues.push(d[key]);
            }
            return (
              <TableRow className={i % 2 === 0 ? classes.tableContent : classes.tableContent2}>
                {arrValues.map((value, i) => (
                  <TableCell align="center" style={{border: 'none', color: 'white', fontFamily: "Bebas-Bold", fontSize: '18px'}}>
                    {value}
                  </TableCell>
                ))}
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default TableDynamic;
