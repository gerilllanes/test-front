import Section from '../../../../components/Section/Section'
import AddButton from './AddButton'
import TableDynamic from './TableDynamic'

const Achievements = () => {

    const headers = [
        'TORNEO',
        'PUESTO',
        'YEAR'
    ]

    const data = [
        {
            nombre: 'Copa america',
            puesto: 'primero pa',
            anio: '2021'
        },
    ]
    return (
        <Section title='Achievements'>
            <TableDynamic headers={headers} data={data} />
            <AddButton />
        </Section>
    )
}

export default Achievements