import { makeStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';


const useStyles = makeStyles({
    button: {
        backgroundColor: 'rgba(33, 33, 33, 0.2)',
        width: '100%',
        marginTop: '0.5rem',
        borderRadius: 0,
        '&:hover': {
            backgroundColor: 'rgba(33, 33, 33, 0.5)'
        },
        color: 'white',
        height: '3.563rem'
    }
})


const AddButton = () => {
    const classes = useStyles()
    return (
        <Button variant="contained" className={classes.button}><AddIcon /></Button>
    )   
}

export default AddButton