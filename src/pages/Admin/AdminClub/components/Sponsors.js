import Section from '../../../../components/Section/Section'
import AddButton from './AddButton'
import TableDynamic from './TableDynamic'

const Sponsors = () => {
    const headers = [
        'SPONSOR',
        'VALOR CONTRATO',
        'VIGENTES (S/N)'
    ]

    const data = [
        {
            nombre: 'sponsor',
            valor: '33',
            vigente: 'se'
        },
        {
            nombre: 'sponsor',
            valor: '33',
            vigente: 'se'
        }
    ]
    return (
        <Section title='Sponsors'>
            <TableDynamic headers={headers} data={data} />
            <AddButton />
        </Section>
    )
}

export default Sponsors