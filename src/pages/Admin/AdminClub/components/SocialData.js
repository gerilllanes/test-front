import Section from "../../../../components/Section/Section";
import TextBox from "../../../../components/Section/TextBox";
import AvatarProfile from "../../../../components/Section/AvatarProfile";
import CardBox from "../../../../components/Section/CardBox";
import img from "../../../../assets/img/banderas/españa.png";
import Grid from "@material-ui/core/Grid";
import SocialMedia from "../../../../components/Section/SocialMedia";

const SocialData = () => {
  return (
    <>
      <Section title="Social Data">
        <Grid container>
          <Grid item xs={6}>
            <AvatarProfile />
            <CardBox
              img={img}
              title="pais"
              textPrimary="BARCELONA, ESPANA"
              textSecondary="HOLIWIS"
            />
            <SocialMedia />
          </Grid>
          <Grid item xs={6}>
            <TextBox text="NOMBRE CORTO:" />
            <TextBox text="FUNDACION:" />
            <TextBox text="SOCIOS" />
            <TextBox text="TEL. CONTACTO:" />
            <TextBox text="EMAIL: " />
            <TextBox text="WEB: " />
            <TextBox text="ESTADIO: " />
            <TextBox text="CAPACIDAD: " />
          </Grid>
        </Grid>
      </Section>
    </>
  );
};

export default SocialData;
