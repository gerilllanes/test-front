import Section from '../../../../components/Section/Section'
import AddButton from './AddButton'
import TableDynamic from './TableDynamic'

const Sports = () => {
    const headers = [
        'APELLIDO',
        'NOMBRE',
        'CARGO',
        'TELEFONO',
        'EMAIL'
    ]

    const data = [
        {
            apellido: 'illanes',
            nombre: 'german',
            cargo: 'el capi',
            telefono: '333',
            email: 'germi@ger.com'
        },
        {
            apellido: 'illanes',
            nombre: 'german',
            cargo: 'el capi',
            telefono: '333',
            email: 'germi@ger.com'
        },
    ]
    return (
        <Section title='Sports'>
            <TableDynamic headers={headers} data={data} />
            <AddButton />
        </Section>
    )
}

export default Sports