import { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import translate from "../../../../lang/lang"

import ActiveUsers from "./ActiveUsers";
import PendingUsers from "./PendingUsers";
import CreateUser from "./CreateUser";

const TabPanel = (props) => {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <p>{children}</p>
        </Box>
      )}
    </div>
  );
};

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const a11yProps = (index) => {
  return {
    id: `wrapped-tab-${index}`,
    "aria-controls": `wrapped-tabpanel-${index}`,
  };
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: "#0a1a24",
    fontFamily: "Bebas-Bold",
    margin: 3,
    marginTop: -10,
    width: "100%"
  },
  appBar: {},
  tabs: {
    backgroundColor: "#0a1a24",
  },
  tab: {
    color: "#9bf5fb",
    fontFamily: "Bebas-Bold",
    fontSize: 25,
    minWidth: 500,
    width: 800,
    margin: 10,
  },
}));

const UsersTable = () => {
  const classes = useStyles();
  const [value, setValue] = useState(0);

  const [createUser, setCreateUser] = useState(false);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <>
      {!createUser ? (
        <div className={classes.root}>
          <AppBar elevation={0} position="static" className={classes.appBar}>
            <Tabs
              value={value}
              onChange={handleChange}
              className={classes.tabs}
              TabIndicatorProps={{ style: { backgroundColor: "#9bf5fb" } }}
            >
              <Tab
                label={translate("Active Users")}
                {...a11yProps(0)}
                className={classes.tab}
              />
              <Tab
                label={translate("Pending Users")}
                {...a11yProps(1)}
                className={classes.tab}
              />
            </Tabs>
          </AppBar>
          <TabPanel value={value} index={0}>
            <ActiveUsers setCreateUser={setCreateUser} />
          </TabPanel>
          <TabPanel value={value} index={1} className={classes.item}>
            <PendingUsers />
          </TabPanel>
        </div>
      ) : (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <CreateUser />
        </div>
      )}
    </>
  );
};

export default UsersTable;
