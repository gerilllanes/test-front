import { useState, useContext } from "react";
import UserContext from "../../../../context/UserContext/UserContext";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import UserForm from "../../../../components/UserForm/UserForm";
import DemographicData from "../../../../components/UserForm/UserProfile/Components/DemographicData";
import Container from "@material-ui/core/Container";
import ScreenThree from "../../../../components/UserForm/UserProfile/Components/ScreenThree";

const useStyles = makeStyles({
  stepperContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  button: {
    backgroundColor: "rgb(155, 245, 251)",
    height: "70%",
    margin: "1.2em",
    fontFamily: "Bebas-Bold",
    fontSize: "20px",
  },
  active: {
    "& $active": {
      color: "rgb(155, 245, 251)",
    },
    "& $completed": {
      color: "rgb(155, 245, 251)"
    },
    "& $disabled": {
      color: "#ccc"
    }
  },
});

const CreateUser = () => {
  const history = useHistory();
  const userContext = useContext(UserContext);
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [userForm, setUserForm] = useState({
    email: "",
    club: "",
    name: "",
    lastname: "",
    role: "",
    area: 0,
    alternative_email: "",
    birthdate: "",
    img_file: "",
    language: "",
    dni: "",
    passport: "",
    sex: "",
    private_phone: 0,
    telephone_contact: 0,
    address: "",
    city_of_residence: "",
    city_of_birth: "",
    zip_code: "",
    country_of_residence: "",
    country_of_birth: "",
  });

  const handleChangeUser = (e) => {
    setUserForm({ ...userForm, [e.target.name]: e.target.value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    userContext.postUserAdd(userForm);
    history.push("/dashboard");
  };
  const steps = [0, 0, 0];

  const content = {
    0: <UserForm handleChangeUser={handleChangeUser} />,
    1: <DemographicData handleChangeUser={handleChangeUser} />,
    2:
      userForm.role === "ROLE_PLAYER" ? (
        <ScreenThree handleChangeUser={handleChangeUser} />
      ) : null,
  };

  const showForm = content[activeStep] || 0;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  console.log(userForm)

  return (
    <div>
      <Grid container>
        <Grid item xs={12}>
          <Container>{showForm}</Container>
          <div className={classes.stepperContainer}>
            <Button
              variant="contained"
              className={classes.button}
              onClick={handleBack}
              disabled={activeStep === 0}
            >
              Back
            </Button>
            <Stepper activeStep={activeStep} style={{ background: "none" }}>
              {steps.map((step) => (
                <Step                 classes={{
                  root: classes.active
                }}>
                  <StepLabel
                    StepIconProps={{
                      classes: {
                        active: classes.active,
                        completed: classes.active,
                        disabled: classes.active
                      },
                    }}
                  >
                    {step}
                  </StepLabel>
                </Step>
              ))}
            </Stepper>
            {userForm.role !== "ROLE_PLAYER" && activeStep === 1 ? (
              <Button
                disabled={activeStep === 0}
                onClick={handleSubmit}
                className={classes.button}
              >
                Submit
              </Button>
            ) : userForm.role === "ROLE_PLAYER" && activeStep === 2 ? null : (
              <Button
                variant="contained"
                className={classes.button}
                onClick={handleNext}
                disabled={activeStep === 2}
              >
                Next
              </Button>
            )}
            {userForm.role === "ROLE_PLAYER" && activeStep === 2 && (
              <Button
                disabled={activeStep === 0}
                onClick={handleSubmit}
                className={classes.button}
              >
                Submit
              </Button>
            )}
          </div>
        </Grid>
      </Grid>
    </div>
  );
};
export default CreateUser;
