import { useState, useEffect, useContext } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Tooltip from "@material-ui/core/Tooltip";
import BlockIcon from "@material-ui/icons/Block";
import CachedIcon from "@material-ui/icons/Cached";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Spinner from "./../../../../components/Spinner/Spinner";
import UserContext from "../../../../context/UserContext/UserContext";
import translate from "../../../../lang/lang";
import TablePagination from "@material-ui/core/TablePagination";
import SearchTable from "./SearchTable";

const useStyles = makeStyles({
  container: {
    borderRadius: 0,
    height: "25rem",
    backgroundColor: "transparent"
  },
  table: {
    minWidth: 650,
  },
  head: {
    backgroundColor: "#102e2f",
  },
  body: {
    backgroundColor: "#2b565f",
  },
  cell: {
    border: "5px solid #0a1a24",
    borderTop: 0,
    borderLeft: 0,
    color: "white",
    margin: 20,
    fontFamily: "Bebas-Bold",
    fontSize: 15,
    padding: 10,
    letterSpacing: 1,
  },
  form: {
    display: "flex"
  },
  icon: {
    padding: 3,
    color: "#9bf5fb",
  },
  caption: {
    display: "none",
  },
  search: {
    height: 30,
  },
  button: {
    backgroundColor: "#2b565f",
    margin: 5,
    width: "20%",
    padding: 2,
    color: "white",
    borderRadius: 0,
    float: "right",
    "&:hover": {
      backgroundColor: "none",
      color: "#9bf5fb",
    },
    fontFamily: "Bebas-Bold",
    fontSize: "20px",
  },
});

const IconTooltip = withStyles({
  tooltip: {
    color: "white",
    backgroundColor: "#0a1a24",
    fontFamily: "Bebas-Bold",
    borderRadius: 0,
    letterSpacing: 1,
  },
})(Tooltip);

const searchingTerm = (search) => {
  return function (x) {
    return x.email.includes(search) || !search;
  };
};

const ActiveUsers = ({ setCreateUser }) => {
  const classes = useStyles();
  const [search, setSearch] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage] = useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const userContext = useContext(UserContext);

  const { getActiveUsers, activeUsers, blockUser, putRecoverPassword } = userContext;

  useEffect(() => {
    (async () => {
      await getActiveUsers();
      await setIsLoaded(true);
    })();
  }, [activeUsers]);

  if (!isLoaded) {
    return <Spinner />;
  } else {
    return (
      <>
        <div className={classes.form}>
          <SearchTable
            onChange={(event) => {
              setSearch(event.target.value);
            }}
            value={search}
          />
          <Button
            className={classes.button}
            flexDirection="row-reverse"
            onClick={() => setCreateUser(true)}
          >
            Crear usuario
          </Button>
        </div>
        <TableContainer
          component={Paper}
          className={classes.container}
          elevation={0}
        >
          <Table className={classes.table}>
            <TableHead>
              <TableRow className={classes.head}>
                <TableCell className={classes.cell}>{translate("User")}</TableCell>
                <TableCell className={classes.cell}>{translate("Name")}</TableCell>
                <TableCell className={classes.cell}>{translate("Club")}</TableCell>
                <TableCell className={classes.cell}>{translate("Role")}</TableCell>
                <TableCell className={classes.cell}>{translate("Condition")}</TableCell>
                <TableCell className={classes.cell}>{translate("Actions")}</TableCell>
              </TableRow>
            </TableHead>
            {(rowsPerPage > 0
              ? activeUsers.slice(
                page * rowsPerPage,
                page * rowsPerPage + rowsPerPage
              )
              : activeUsers
            )
              .filter(searchingTerm(search))
              .map((data) => (
                <TableBody>
                  <TableRow key={data.id} className={classes.body}>
                    <TableCell className={classes.cell}>{data.email}</TableCell>
                    <TableCell className={classes.cell}>
                      {data.name} {data.lastname}
                    </TableCell>
                    <TableCell className={classes.cell}>{data.club}</TableCell>
                    <TableCell className={classes.cell}>
                      {translate(data.role)}
                    </TableCell>
                    <TableCell className={classes.cell}>
                      {translate(data.active)}
                    </TableCell>
                    <TableCell className={classes.cell}>
                      <IconButton className={classes.icon}>
                        <IconTooltip
                          title="Change Password"
                          onClick={() => putRecoverPassword("email=" + data.email)}>
                          <CachedIcon />
                        </IconTooltip>
                      </IconButton>
                      <IconButton
                        className={classes.icon}
                        onClick={() => blockUser('id=' + data.id)}
                      >
                        <IconTooltip title="Block">
                          <BlockIcon />
                        </IconTooltip>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                </TableBody>
              ))}
          </Table>
          <TablePagination
            component="div"
            style={{ color: "white", backgroundColor: "#0a1a24" }}
            classes={{
              caption: classes.caption,
            }}
            rowsPerPageOptions={[5]}
            count={activeUsers.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
          />
        </TableContainer>
      </>
    );
  }
};

export default ActiveUsers;
