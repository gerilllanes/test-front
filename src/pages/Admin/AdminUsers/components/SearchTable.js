import { makeStyles } from "@material-ui/core";
import InputBase from "@material-ui/core/InputBase";

const useStyles = makeStyles(() => ({
  input: {
    backgroundColor: "#2b565f",
    width: 250,
    margin: 2,
    marginLeft: 0,
    padding: 11,
    color: "white",
    fontFamily: "Bebas-Bold",
    letterSpacing: 1,
  },
}));

const SearchTable = ({ onChange, value }) => {
  const classes = useStyles();

  return (
    <>
      <InputBase
        placeholder="Search"
        inputProps={{
          className: classes.input,
        }}
        onChange={onChange}
        value={value}
      />
    </>
  );
};

export default SearchTable;
