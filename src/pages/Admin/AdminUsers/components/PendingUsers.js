import { useState, useEffect, useContext } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ThumbDownIcon from "@material-ui/icons/ThumbDown";
import Spinner from "./../../../../components/Spinner/Spinner";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import translate from "../../../../lang/lang";
import UserContext from "../../../../context/UserContext/UserContext";
import TablePagination from "@material-ui/core/TablePagination";
import SearchTable from "./SearchTable";

const useStyles = makeStyles({
  container: {
    borderRadius: 0,
    height: "25rem",
    backgroundColor: "transparent"
  },
  table: {
    minWidth: 650,
  },
  head: {
    backgroundColor: "#102e2f",
  },
  body: {
    backgroundColor: "#2b565f",
  },
  form: {
    margin: 3,
    marginLeft: 0
  },
  cell: {
    border: "5px solid #0a1a24",
    borderTop: 0,
    borderLeft: 0,
    color: "white",
    margin: "20px",
    fontFamily: "Bebas-Bold",
    fontSize: 15,
    padding: 10,
    letterSpacing: 1,
  },
  icon: {
    padding: 3,
    color: "#9bf5fb",
  },
  caption: {
    display: "none",
  },
  search: {
    height: 30,
  },
});

const IconTooltip = withStyles({
  tooltip: {
    color: "white",
    backgroundColor: "#0a1a24",
    fontFamily: "Bebas-Bold",
    borderRadius: 0,
    letterSpacing: 1,
  },
})(Tooltip);

const searchingTerm = (search) => {
  return function (x) {
    return x.email.includes(search) || !search;
  };
};

const PendingUsers = () => {
  const classes = useStyles();
  const [search, setSearch] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage] = useState(5);
 
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const userContext = useContext(UserContext);

  const { getUsersPend, usersPend } = userContext;

  useEffect(() => {
    (async () => {
      await getUsersPend();
      await setIsLoaded(true);
    })();
  }, [usersPend]);

  if (!isLoaded) {
    return <Spinner />;
  } else {
    return (
      <>
        <div className={classes.form}>
        <SearchTable
            onChange={(event) => {
              setSearch(event.target.value);
            }}
            value={search}
          />
        </div>
        <TableContainer
          component={Paper}
          className={classes.container}
          elevation={0}
        >
          <Table className={classes.table}>
            <TableHead>
              <TableRow className={classes.head}>
                <TableCell className={classes.cell}>{translate("User")}</TableCell>
                <TableCell className={classes.cell}>{translate("Lastname")}</TableCell>
                <TableCell className={classes.cell}>{translate("Club")}</TableCell>
                <TableCell className={classes.cell}>{translate("Role")}</TableCell>
                <TableCell className={classes.cell}>{translate("Dni")}</TableCell>
                <TableCell className={classes.cell}>{translate("Actions")}</TableCell>
              </TableRow>
            </TableHead>
            {
            (rowsPerPage > 0
              ? usersPend.slice(
                  page * rowsPerPage,
                  page * rowsPerPage + rowsPerPage
                )
              : usersPend
            )
            .filter(searchingTerm(search))
            .map((data) => (
                <TableBody>
                  <TableRow key={data.id} className={classes.body}>
                    <TableCell className={classes.cell}>{data.email}</TableCell>
                    <TableCell className={classes.cell}>
                      {data.name} {data.lastname}
                    </TableCell>
                    <TableCell className={classes.cell}>{data.club}</TableCell>
                    <TableCell className={classes.cell}>
                      {translate(data.role)}
                    </TableCell>
                    <TableCell className={classes.cell}>{data.dni}</TableCell>
                    <TableCell className={classes.cell}>
                      <IconButton className={classes.icon} onClick={() => console.log('d', data.role)}>
                        <IconTooltip title="Accept">
                          <ThumbUpIcon />
                        </IconTooltip>
                      </IconButton>
                      <IconButton className={classes.icon}>
                        <IconTooltip title="Ignore">
                          <ThumbDownIcon />
                        </IconTooltip>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                </TableBody>
              ))}
          </Table>
          <TablePagination
            component="div"
            style={{ color: "white", backgroundColor: "#0a1a24" }}
            classes={{
              caption: classes.caption,
            }}
            rowsPerPageOptions={[5]}
            count={usersPend.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
          />
        </TableContainer>
      </>
    );
  }
};

export default PendingUsers;
