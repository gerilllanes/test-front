import AdminLayout from "../../../components/Admin/AdminLayout/AdminLayout";
import UsersTable from "./components/UsersTable";
import { useHistory } from "react-router-dom";

const AdminUsers = () => {
  const history = useHistory();
  return (
    <AdminLayout history={history}>
      <UsersTable />
    </AdminLayout>
  );
};

export default AdminUsers;
