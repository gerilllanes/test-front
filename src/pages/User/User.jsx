import { useState, useEffect, useContext } from "react";
import UserContext from "../../context/UserContext/UserContext";
import PlayerContext from "./../../context/PlayerContext/PlayerContext";
import { makeStyles } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import translate from "../../lang/lang";

import Header from "../../components/Header/Header";
import Section from "../../components/Section/Section";
import TextBox from "../../components/Section/TextBox";
import CardBox from "../../components/Section/CardBox";
import AvatarProfile from "../../components/Section/AvatarProfile";
import Spinner from "../../components/Spinner/Spinner";

const useStyles = makeStyles(() => ({
  root: {
    height: "100vh",
  },
  header: {
    margin: "0.6rem",
    display: "flex",
  },
  userBox: {
    justifyContent: "end",
  },
  container: {
    display: "flex",
    padding: 0,
  },
  grid: {
    margin: "0.6rem",
  },
  section: {
    display: "flex",
    padding: 0,
  },
}));

const User = () => {
  const classes = useStyles();
  const [isLoaded, setIsLoaded] = useState(false);

  const userContext = useContext(UserContext);
  const playerContext = useContext(PlayerContext);

  const userLogged = userContext.userLogged;
  const { getPlayerInfo, playerInfo } = playerContext;

  useEffect(() => {
    (async () => {
      await getPlayerInfo(playerInfo.id);
      await setIsLoaded(true);
    })();
  }, [playerInfo]);

  useEffect(() => {
    document.body.classList.add("background-main");
    return () => {
      document.body.classList.remove("background-main");
    };
  });

  if (!isLoaded) {
    return <Spinner />;
  } else {
    return (
      <>
        <Box className={classes.root}>
          <Box className={classes.header}>
            <Header title={translate("My profile")} />
          </Box>
          <Container maxWidth="xl" className={classes.container}>
            <Grid
              item
              xs={12}
              md={6}
              lg={6}
              className={classes.grid}
            >
              <Section title={translate("User data")}>
                <Container maxWidth="xl" className={classes.section}>
                  <Grid item xs={3} md={3} lg={3}>
                    <AvatarProfile />
                    <CardBox
                      title={translate("City of Birth")}
                      textPrimary={userLogged.cityOfBirth}
                      textSecondary={userLogged.birthdate}
                    />
                    <CardBox
                      title={translate("Residence")}
                      textPrimary={userLogged.cityOfResidence}
                      textSecondary={userLogged.countryOfResidence}
                    />
                  </Grid>
                  <Grid item xs={9} md={9} lg={9}>
                    <TextBox result={userLogged.name} />
                    <TextBox
                      text={translate("Email:")}
                      result={userLogged.email}
                    />
                    <TextBox
                      text={translate("Password:")}
                      result={userLogged.password}
                    />
                    <TextBox result={userLogged.roleByDefaultClub.name} />
                    <TextBox
                      text={translate("Roles and Positions:")}
                      result={translate(userLogged.roleByDefaultClub.type)}
                    />
                    <TextBox
                      text={translate("Sex:")}
                      result={translate(userLogged.sex)}
                    />
                    <TextBox text={translate("Dni:")} result={userLogged.dni} />
                    <TextBox
                      text={translate("Passport:")}
                      result={userLogged.passport}
                    />
                    <TextBox
                      text={translate("Contact:")}
                      result={userLogged.telephoneContact}
                    />
                  </Grid>
                </Container>
              </Section>
            </Grid>
            {userLogged.roleByDefaultClub.type === 'ROLE_PLAYER' ?
              playerInfo.length &&
              playerInfo.map((player) => (
                <Grid key={player.user_id} item xs={12} md={3} lg={3} className={classes.grid}>
                  <Section title={translate("Sports data")}>
                    <TextBox
                      text={translate("Sport:")}
                      result={player.sport_name}
                    />
                    <TextBox text={translate("Arrival:")} result="" />
                    <TextBox
                      text={translate("Category:")}
                      result={player.category_team_name}
                    />
                    <TextBox
                      text={translate("Dorsal:")}
                      result={player.user_id}
                    />
                    <TextBox text={translate("Size:")} result={player.weight} />
                    <TextBox
                      text={translate("Footwear:")}
                      result={player.skillful_foot}
                    />
                    <TextBox
                      text={translate("Skiful Foot:")}
                      result={player.skillful_foot}
                    />
                    <TextBox
                      text={translate("Height:")}
                      result={player.height}
                    />
                    <TextBox
                      text={translate("Weight:")}
                      result={player.weight}
                    />
                  </Section>
                </Grid>
              )) : null}
          </Container>
        </Box>
      </>
    );
  }
};

export default User;
