import { useEffect } from "react";

import { makeStyles } from "@material-ui/core/styles";
import { Container, Grid } from "@material-ui/core";

import Video from "../../components/VideoAnalysis/Video";
import Players from "../../components/VideoAnalysis/Players";
import Parameters from "../../components/VideoAnalysis/Parameters";
import TableStadistics from "../../components/VideoAnalysis/TableStadistics";

const useStyles = makeStyles(() => ({
  container: {
    padding: "5px",
    fontFamily: "Bebas-Bold",
    display: "flex",
    position: "center",
  },
  table: {
    overflowX: "hidden",
    height: "30vh"
  },
  grid: {
    overflowX: "hidden",
    height: "93vh"
  },
}));

const VideoAnalysis = () => {
  const classes = useStyles();
  return (
    <>
      <Container maxWidth="xl" className={classes.container} spacing={2}>
        <Grid item xs={9} md={9} lg={9}>
          <TableStadistics className={classes.table} />
          <Video />
        </Grid>
          <Grid className={classes.grid} item  xs={1} md={1} lg={1}>
            <Players />
          </Grid>
          <Grid className={classes.grid} item xs={3} md={3} lg={2}>
            <Parameters title="2020/2021" />
          </Grid>
      </Container>
    </>
  );
};

export default VideoAnalysis;
