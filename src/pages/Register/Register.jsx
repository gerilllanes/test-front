import React, { useState, useContext } from "react";
import UserContext from "../../context/UserContext/UserContext";
import FormRegister from "../Register/components/FormRegister/FormRegister";
import AlertInfo from "../../components/Alerts/AlertInfo";

const Register = () => {
  const [form, setForm] = useState({
    email: "",
    club: "",
    name: "",
    lastname: "",
    dni: "",
    role: "ROLE_TUTOR",
    link_approved: "",
    link_rejected: "",
  });
  const userContext = useContext(UserContext);
  const handleOnChange = (e) => {
    setForm({ ...form, [e.name]: e.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    userContext.postRegister(form);
  };
  return (
    <>
      <AlertInfo
        text="Recuerda que algún representante debe informarte el código de
            identificación del club para poder registrarte."
      />
      <FormRegister
        onChange={handleOnChange}
        handleSubmit={handleSubmit}
        buttonText={"Register"}
        values={form}
      >
        {/* <Footer
          primaryText="Go back"
          primaryRoute="/login"
          secondaryText="Terms and conditions"
          secondaryRoute="/terms"
        /> */}
      </FormRegister>
    </>
  );
};

export default Register;
