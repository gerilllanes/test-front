import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { FormControl, Grid, InputLabel, Select } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  form: {
    width: "100%",
    height: "100%",
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  input: {
    marginTop: "16px",
  },
  inputs: {
    color: "white",
    border: "cyan",
  },
}));

const FormRegister = ({
  handleSubmit,
  onChange,
  values,
  buttonText,
  children,
}) => {
  const classes = useStyles();

  let roles = [
    {
      id: 1,
      label: "Tutor",
    },
    {
      id: 2,
      label: "Doctor",
    },
    {
      id: 3,
      label: "Trainer",
    },
    {
      id: 4,
      label: "Player",
    },
    {
      id: 5,
      label: "Pyschologist",
    },
  ];

  return (
    <>
      <form className={classes.form} validate="true" onSubmit={handleSubmit}>
        <Grid container spacing={1}>
          <Grid item md={6}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Name"
              name="name"
              value={values.name}
              onChange={(e) => onChange(e.target)}
              InputLabelProps={{
                classes: {
                  root: classes.inputs,
                },
              }}
              InputProps={{
                classes: { root: classes.inputs },
              }}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Email"
              name="email"
              value={values.email}
              onChange={(e) => onChange(e.target)}
              InputLabelProps={{
                classes: {
                  root: classes.inputs,
                },
              }}
              InputProps={{
                classes: { root: classes.inputs },
              }}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Club ID"
              name="club"
              value={values.club}
              onChange={(e) => onChange(e.target)}
              InputLabelProps={{
                classes: {
                  root: classes.inputs,
                },
              }}
              InputProps={{
                classes: { root: classes.inputs },
              }}
            />
          </Grid>
          <Grid item md={6}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Lastname"
              name="lastname"
              value={values.lastname}
              onChange={(e) => onChange(e.target)}
              InputLabelProps={{
                classes: {
                  root: classes.inputs,
                },
              }}
              InputProps={{
                classes: { root: classes.inputs },
              }}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="DNI"
              name="dni"
              value={values.dni}
              onChange={(e) => onChange(e.target)}
              InputLabelProps={{
                classes: {
                  root: classes.inputs,
                },
              }}
              InputProps={{
                classes: { root: classes.inputs },
              }}
            />
            <FormControl className={classes.input} fullWidth>
              <InputLabel
                id="role-label"
                variant="outlined"
                required
                InputLabelProps={{
                  classes: {
                    root: classes.inputs,
                  },
                }}
                InputProps={{
                  classes: { root: classes.inputs },
                }}
              >
                Rol
              </InputLabel>
              <Select
                labelId="role-label"
                variant="outlined"
                label="rol"
                value={values.role}
                InputLabelProps={{
                  classes: {
                    root: classes.inputs,
                  },
                }}
                InputProps={{
                  classes: { root: classes.inputs },
                }}
              >
                {roles.map((rol) => {
                  return (
                    <MenuItem value={roles} key={rol.id}>
                      <ListItemText primary={rol.label} />
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </Grid>
        </Grid>

        {children}

        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
        >
          {buttonText}
        </Button>
      </form>
    </>
  );
};

export default FormRegister;
