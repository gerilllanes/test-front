import { useState, useEffect, useContext, /*useCallback*/ } from "react";

import Work from "./Components/Work";
import translate from "../../lang/lang";
import Charts from "./Components/Charts";
import Grid from "@material-ui/core/Grid";
import useFetch from "../../hooks/useFetch";
import Modal from '@material-ui/core/Modal';
import AddChart from "./Components/AddChart";
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from "@material-ui/core/styles";
import useQuerystring from "../../hooks/useQuerystring";
import FiltersMetrics from "./Components/FiltersMetrics";
import { DateRangePicker } from "materialui-daterange-picker";
import UserContext from "../../context/UserContext/UserContext";
import PlayerContext from "../../context/PlayerContext/PlayerContext";

const useStyles = makeStyles( (theme) => ({
    container: {
        '& .side-center': {
            overflowY: 'auto',
            maxHeight: 'calc(100vh - 9rem)'
        },
        '& .side-right': {
            right: '1rem',
            position: 'absolute',
            width: 'calc(33% - 5rem)',
            transform: 'translateY(-6rem)',
            '& .MuiGrid-item': {
                padding: '7.5px 0'
            },
            '& :hover .players-select-quick-buttom': {
                opacity: .9
            },
            '& .players-select-quick-buttom': {
                opacity: 0,
                cursor: 'pointer',
                transition: 'all 1s',
                position: 'absolute',
                borderRadius: '5px',
                background: '#0A1A24',
                padding: '5px 10px 3px',
                transform: 'translate(-100%, 30px)'
            }
        },
        '& .gps-graphic': {
            '& .highcharts-a11y-proxy-container button': {
                backgroundColor: 'transparent !important'
            },
            '& text': {
                fill: '#fff !important',
                '& tspan': {
                    stroke: '#000 !important'
                }
            },
            '& .highcharts-drillup-button text': {
                fill: '#000000 !important'
            },
            margin: '2px 1px',
            padding: '15px',
            display: 'flex',
            borderRadius: '10px',
            flexDirection: 'column',
            justifyContent: 'center',
            border: '2px solid #5DE6E6',
            minHeight: 'calc((100vh / 2.1) - 4rem)',
            maxHeight: 'calc((100vh / 2.1) - 4rem)',
            '& .gps-graphic-metric-select': {
                margin: '0 0 15px',
                '& .addMetricLabel': {
                    margin: '0 5px 0 0'
                },
                '& #metrics-options': {
                    borderRadius: '5px'
                }
            },
            '& .options-group-container': {
                padding: '5px',
                margin: '0 0 10px',
                borderRadius: '5px',
                border: '1px solid #ffffff',
            },
            '& button': {
                opacity: '.8 !important',
                padding: '5px',
                display: 'flex',
                color: '#ffffff !important',
                cursor: 'pointer',
                alignItems: 'center',
                backgroundColor: '#0A1A24 !important',
                justifyContent: 'center',
                '& span': {
                    margin: '0 5px 0 0'
                }
            },
            '& .highcharts-figure-container': {
                height: 'calc((100vh / 2.2) - 4rem)',
            }
        },
        '& .grid-avatars': {
            overflow: 'auto',
            transform: 'translateY(7px)',
            maxHeight: 'calc(100vh - 2.5rem)',
        },
        '& .avatar-container': {
            display: 'flex',
            justifyContent: 'center',
            '& .MuiAvatar-root': {
                width: '4rem',
                height: '4rem',
                padding: '.2rem',
                cursor: 'pointer',
                margin: '.4rem 0',
                background: '#ffffff'
            }
        },
        '& .filter-grid':{
            paddingRight: '1px !important'
        },
        '& .work-grid':{
            paddingLeft: '1px !important'
        }
    },
    paper: {
        top: '50%',
        left: '50%',
        color: '"#ffffff',
        position: 'absolute',
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        transform: 'translate(-50%, -50%)',
        '& .MuiGrid-root': {
            background: '#1c4f50'
        },
        '& .materialui-daterange-picker-MuiPaper-root-7': {
            background: '#1c4f50'
        },
        '& *': {
            color: '#ffffff'
        },
        '& .materialui-daterange-picker-makeStyles-highlighted-113':{
            backgroundColor: 'rgb(0 0 0 / 50%)'
        },
        '& .materialui-daterange-picker-makeStyles-filled-112': {
            backgroundColor: '#2e7272'
        },
        '& .materialui-daterange-picker-makeStyles-outlined-111': {
            border: '1px solid #081717'
        },
        '& .MuiPaper-elevation0': {
            backgroundColor: '#1c4f50'
        },
        '& ul':{
            backgroundColor: '#1c4f50'
        },
        '& button [clasṣ*="filled"]': {
            backgroundColor: '#081717'
        },
        '& .MuiPaper-root.MuiPaper-rounded': {
            left: '50% !important',
            transform: 'translateX(-50%) !important',
        }
    }
}));

const options = {
    //time: {label: 'duration', unit: 'min.', color: '#FBB431'},
    total_distance: {label: 'distance', unit: 'm', color: '#F04735'},
    total_energy: {label: 'energy', unit: 'j/kg', color: '#709234'},
    average_p: {label: 'avg met power', unit: 'W/kg', color: '#6894A2'},
    eq_dist_perc: {label: 'eq distance index', unit: '%', color: '#338DFF'},
    average_v: {label: 'avg speed', unit: 'm/s', color: '#7133FF'},
    average_power_aer: {label: 'avg vo₂', unit: 'W/kg', color: '#E933FF'},
    anaerobic_energy: {label: 'an energy', unit: 'J/kg', color: '#33FF9C'},
    an_energy_perc: {label: 'an index', unit: '%', color: '#FFBB33'},
    equivalent_distance: {label: 'eq distance', unit: 'm', color: '#FF8D33'},
    recovery_average_time: {label: 'MPE rec avg time', unit: 's', color: '#FF6433'},
    events_av_duration: {label: 'MPE avg time', unit: 's', color: '#CEFF33'},
    recovery_average_power: {label: 'MPE rec avg power', unit: 'W/kg', color: '#33FF71'},
    events_av_power: {label: 'MPE avg power', unit: 'W/kg', color: '#339CFF'},
    active_muscle_load: {label: 'AML', unit: 'J/kg', color: '#5B33FF'},
    average_active_muscle_power: {label: 'avg AMP', unit: 'W/kg', color: '#9633FF'},
    eccentric_index: {label: 'eccentric index', unit: ' 1-2', color: '#FF3390'}
}

const Gps = () => {

    let myTimeout = null;

    const [clearDependencies, setClearDependencies] = useState(false);
    const [query, querystring, addParams, removeParams] = useQuerystring();
    const classes = useStyles();

    const [metricsData, setMetricsData] = useState({
        //duration: {name: 'duration', color: '#FBB431', options:{average: false, maximun: false, minimun: false, total: false} },
        distance: {name: 'distance', color: '#F04735', options:{average: false, maximun: false, minimun: false, total: false}},
        energy: {name: 'energy', color: '#709234' , options:{average: false, maximun: false, minimun: false, total: false}},
        average_p: {name: 'avg met power', color: '#6894A2', options:{average: false, maximun: false, minimun: false, total: false}},
        average_v: {name: 'avg speed', color: '#7133FF', options:{average: false, maximun: false, minimun: false, total: false}},
        average_power_aer: {name: 'avg vo₂', color: '#E933FF', options:{average: false, maximun: false, minimun: false, total: false}},
        anaerobic_energy: {name: 'an energy', color: '#33FF9C', options:{average: false, maximun: false, minimun: false, total: false}},
        an_energy_perc: {name: 'an index', color: '#FFBB33', options:{average: false, maximun: false, minimun: false, total: false}},
        equivalent_distance: {name: 'eq distance', color: '#FF8D33', options:{average: false, maximun: false, minimun: false, total: false}},
        recovery_average_time: {name: 'MPE rec avg time', color: '#FF6433', options:{average: false, maximun: false, minimun: false, total: false}},
        events_av_duration: {name: 'MPE avg time', color: '#CEFF33', options:{average: false, maximun: false, minimun: false, total: false}},
        events_av_power: {name: 'MPE avg power', color: '#339CFF', options:{average: false, maximun: false, minimun: false, total: false}},
        active_muscle_load: {name: 'AML', color: '#5B33FF', options:{average: false, maximun: false, minimun: false, total: false}},
        average_active_muscle_power: {name: 'avg AMP', color: '#9633FF', options:{average: false, maximun: false, minimun: false, total: false}},
        eccentric_index: {name: 'eccentric index', color: '#FF3390', options:{average: false, maximun: false, minimun: false, total: false}},
        recovery_average_power: {name: 'MPE rec avg power', color: '#33FF71', options:{average: false, maximun: false, minimun: false, total: false}},
        eq_dist_perc: {name: 'eq distance index', color: '#338DFF', options:{average: false, maximun: false, minimun: false, total: false}}
    });

    // filter group by
    const [stateGroupBy, setStateGroupBy] = useState('player');
    // if get tracks or sessions
    const [group, setGroup] = useState(query.get("group") === 'sessions' ? true : false);
    // fill traces that can be sessions
    const [tracks, fetchTracks] = useFetch();
    // set tracks unselect
    const [tracksUnselect, setTrackUnselect] = useState([]);
    // fill sessions that can contain tracks
    const [sessions, fetchSessions] = useFetch();
    // fill tracks of sessions
    const [tracksOfSessions, fetchTracksOfSessions] = useState(null);
    // set sessions unselect
    const [sessionsUnselect, setSessionsUnselect] = useState([]);
    // set sessions unselect
    const [tracksOfSessionsUnselect, setTrackOfSessionsUnselect] = useState([]);
    // set players unselect
    const [playersUnselected, setPlayersUnselected] = useState([]);
    // set teams
    const [teams, setTeams] = useState({});
    // set teams unselect
    const [teamsUnselect, setTeamsUnselect] = useState([]);
    const [athleteSessions, fecthAtheleteSessions] = useFetch();
    const [charts, setCharts] = useState([]);
    const [refreshChart, setRefreshChart] = useState(false);
    const [filterList, setFilterList] = useState([]);
    const [filterSelected, setFilterSelected] = useState(null);
    const [fieldPosition, setFieldPosition] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);

    const newDate = new Date();
    newDate.setMonth(newDate.getMonth()-12);

    const fromDate = query.get('from_date') ? query.get('from_date').split("-") : null;
    const [dateRange, setDateRange] = useState({startDate: new Date( fromDate ? `${fromDate[2]}-${fromDate[1]}-${fromDate[0]}` : newDate ), endDate: new Date() });
    const [openDataPickerModal, setOpenDataPickerModal] = useState(false);
    const toggleDataPicker = () => setOpenDataPickerModal(!openDataPickerModal);

    const userContext = useContext(UserContext);
    const {userLogged} = userContext;

    const playerContext = useContext(PlayerContext);
    const { getPlayers, players } = playerContext;

    const handleAddChart = (config) => {

        //console.log('chartTest1', [...charts, config]);
        //console.log('chartTest2', filterList[filterSelected]);
        let filterBy = {};

        Object.entries(filterList[filterSelected].metrics).forEach( key => {
            Object.entries(filterList[filterSelected].metrics[key[0]].options).forEach( keyMetric => {
                let newMetricItem = null;                
                if( keyMetric[1] ){
                    newMetricItem = {[key[0]] : {name: filterList[filterSelected].metrics[key[0]].name, color: filterList[filterSelected].metrics[key[0]].color } };
                    if( filterBy[ keyMetric[0] ] ){
                        newMetricItem = {...filterBy[keyMetric[0]].metrics, ...newMetricItem};
                        filterBy = {...filterBy, [keyMetric[0]]: { metrics: newMetricItem } };
                    }else{
                        filterBy = {...filterBy, [keyMetric[0]]: { metrics: newMetricItem } };
                    }
                }
            });
        });
        //console.log('key2', {desc: filterList[filterSelected].desc, fieldPosition: filterList[filterSelected].fieldPosition, groupBy: filterList[filterSelected].groupBy, filter: filterBy} );
        setCharts([...charts, {desc: filterList[filterSelected].desc, fieldPosition: filterList[filterSelected].fieldPosition, groupBy: filterList[filterSelected].groupBy, filter: filterBy}]);
    }

    const filterPlayersAvailable = (tracksSelected, unselected) => {
        let newPlayersWithTracksSelected = {};
        if(tracksSelected && players){
            let newTrackSelected = [];
            let newTracksOfSessions = [];
            if( !Array.isArray(tracksSelected)) {
                // filter tracks with sessions selected
                Object.keys(tracksSelected).forEach( (key, index) => {
                    if( !sessionsUnselect.includes(index) ){
                        newTracksOfSessions = [...newTracksOfSessions, ...tracksOfSessions[key] ];
                    }
                });
            }else{
                newTracksOfSessions = tracksSelected
            }
            // filter tracks unselected
            newTracksOfSessions.forEach( (nToS, index) => {
                if( !unselected.includes( (index) ) ){
                    newTrackSelected = [...newTrackSelected, ...[nToS] ];
                }
            });
            // filter player with track unselected
            newTrackSelected.forEach( trk => {
                if(!teamsUnselect.includes(trk.team.id * 1) ){
                    const thisPlayer = players.filter( p => p.id === trk.player.id)[0];
                    let posInclude = fieldPosition.length === 11 ? true : false;
                    if(thisPlayer){
                        thisPlayer.positions.forEach( pos => {
                            if( fieldPosition.includes( (pos.number_pos * 1) ) ){
                                //console.log('include', pos.number_pos);
                                posInclude = true;
                            }
                        });
                        if(posInclude){
                            newPlayersWithTracksSelected = {...newPlayersWithTracksSelected, [`${trk.player.id}-${trk.team.id}`] : {...thisPlayer, team: trk.team } };
                        }
                    }
                }
            });
            // convert newPlayersWithTracksSelected object ti array
            newPlayersWithTracksSelected = Object.keys(newPlayersWithTracksSelected).map( key => {
                return newPlayersWithTracksSelected[key];
            })
        }else{
            newPlayersWithTracksSelected = [];
        }
        return newPlayersWithTracksSelected;
    }

    const handleSetPlayersUnselected = (playerId, include) => {
        if(!include){
            setPlayersUnselected([...playersUnselected, playerId ]);
        }else{
            setPlayersUnselected( playersUnselected.filter( p => { return p !== playerId } ) );
        }
    }

    const handleFilterPlayersAvailable = () => {
        return filterPlayersAvailable( group ? tracksOfSessions : tracks, group ? tracksOfSessionsUnselect : tracksUnselect);
    }

    const handleSelectAllPlayer = () => {

        let playersWithTracksSelected = handleFilterPlayersAvailable();
        if( playersUnselected.length === playersWithTracksSelected.length || playersUnselected.length > 0 ){
            setPlayersUnselected([]);
        }else{
            playersWithTracksSelected = playersWithTracksSelected.filter( p => {
                return p;
            });
            playersWithTracksSelected = playersWithTracksSelected.map( p => {
                return p.id;
            });
            setPlayersUnselected(playersWithTracksSelected);
        }

    }

    const buildAvatars = () => {
        let playersWithTracksSelected = handleFilterPlayersAvailable();
        return playersWithTracksSelected.map( (p, index) => {
            if(p && p.id){
                const include = playersUnselected.includes(p.id);
                return  p ? <div key={`avatar-${index}`} className="avatar-container" onClick={ () => handleSetPlayersUnselected(p.id, include) }>
                            <Tooltip title={`${p.short_name} / ${p.team.name}`} arrow>
                                <Avatar alt={p.short_name} src={p.user.img_file} className={`${ include ? '' : 'selected'}`} style={{border: include ? 'inherit' : `2px solid ${p.color}`, boxShadow: include ? 'inherit' : `0 0 3px 1px #fff, 0 0 3px 2px ${p.color}` }} />
                            </Tooltip>
                        </div>
                        :
                        <div key={`avatar-${index}`}></div>
            }else{
                return <div key={`avatar-${index}`}></div>
            }
        });
    }

    const handleSetGroup = () => {
        fetchTracksOfSessions(null);
        addParams({group: group ? 'tracks' : 'sessions'});
        setGroup(!group);
    }

    const handleChangePosition = (fieldPositionValue) => {
        setFilterSelected(null);
        setFieldPosition(fieldPositionValue);
    }

    const setSaveFilter = (desc) => {
        const newFilter = { desc, metrics: metricsData, groupBy: stateGroupBy, fieldPosition: fieldPosition };
        const newFilterCollection = [...filterList, newFilter];
        setFilterList(newFilterCollection);
        setFilterSelected(newFilterCollection.length > 1 ? newFilterCollection.length - 1 : 0);
        console.log('new filter', newFilter);
    }

    const handleSelectFilter = (index) => {
        setFilterSelected(index);
        setStateGroupBy(filterList[index].groupBy);
        setMetricsData(filterList[index].metrics);
        setFieldPosition(filterList[index].fieldPosition);
        //console.log('filter select', filterList[index], metricsData );
    }

    useEffect( () => {
        if( tracks.length > 0 && athleteSessions && athleteSessions.tracks && athleteSessions.tracks.length > 0 ){
            if(myTimeout){clearTimeout(myTimeout);}
            if(!refreshChart){setRefreshChart(true);}
            myTimeout = setTimeout( () => {
                setRefreshChart(false);
            }, 2000);
            console.log('athleteSessions', athleteSessions);
        }
    }, [athleteSessions, tracks, querystring]);

    useEffect( () => {
        if(players.length > 1){ console.log('players', players) }
    }, [players]);

    useEffect( () => {
        
        if(tracks && tracks.length > 0){
            const tracksIds = tracks.map( t => { return t.id }).join(',') ;
            fecthAtheleteSessions(`/api/gps/athlete-sessions/array/tracks?ids=${tracksIds}`);
            let teamsInTracks = {};
            tracks.forEach( track => {
                if(!teamsInTracks[track.team.id]){
                    teamsInTracks = {...teamsInTracks, [track.team.id]: track.team};
                }
            });
            setTeams(teamsInTracks);
        }
    }, [tracks]);

    useEffect( () => {
        if(tracksOfSessions && tracksOfSessions.length > 0){
            let tracksMerge = [];
            tracksOfSessions.forEach( tos => {
                tracksMerge = [...tracksMerge, ...tos];
            });
            const tracksIds = tracksMerge.map( t => { return t.id }).join(',') ;
            fecthAtheleteSessions(`/api/gps/athlete-sessions/array/tracks?ids=${tracksIds}`);
        }

    }, [tracksOfSessions]);

    useEffect( () => {
        if(sessions.length > 0 && !tracksOfSessions){
            let fetches = [];
            let newSessionsTrack = {};
            sessions.forEach( (ses, xd) => {
                fetches[xd] = fetch( `${process.env.REACT_APP_API_URL}/api/gps/track/team-session/${ses.id}?limit=10000`).then( function(response) {
                        return response.json();
                    });
            });
            Promise.all(fetches).then(function(values) {
                sessions.forEach( (ses, xd) => {
                    newSessionsTrack = {...newSessionsTrack, [ses.id]: values[xd].data };
                });
                fetchTracksOfSessions(newSessionsTrack);
            });
        }
    }, [sessions]);
    
    useEffect( () => {
        const from = dateRange.startDate.getDate() + "-" + (dateRange.startDate.getMonth() + 1) + "-" + dateRange.startDate.getFullYear();
        const to = dateRange.endDate.getDate() + "-" + (dateRange.endDate.getMonth() + 1) + "-" + dateRange.endDate.getFullYear();
        if( query.get('from_date') !== from || query.get('to_date') !== to ){
            addParams({
                from_date: from,
                to_date: to
            });
        }
    }, [dateRange, addParams, query]);

    useEffect( () => {
        if(clearDependencies){
            if(group === false){
                fetchTracks(`/api/gps/track`, {limit: 10000, from_date: query.get('from_date'), to_date: query.get('to_date'), ...userLogged.defaultClub ? {club:userLogged.defaultClub} : {} });
            }else{
                fetchTracksOfSessions(null);
                fetchTracks(`/api/gps/track`, {limit: 10000, from_date: query.get('from_date'), to_date: query.get('to_date'), ...userLogged.defaultClub ? {club:userLogged.defaultClub} : {} });
                setTimeout( () => {
                    fetchSessions(`/api/gps/team-session`, {limit: 10000, from_date: query.get('from_date'), to_date: query.get('to_date'), ...userLogged.defaultClub ? {club:userLogged.defaultClub} : {} });
                }, 500);
            }
        }
    }, [query]);

    useEffect( () => {
        if(!clearDependencies){
            if(players.length < 1 && userLogged){
                const querystring = `${userLogged.defaultClub ? '?club='+userLogged.defaultClub+'&limit=10000' : ''}`;
                getPlayers(querystring);
            }
            setClearDependencies(true);
        }
    }, [clearDependencies, getPlayers, group, players, userLogged, query]);

    useEffect( () => {
        const querystring = `${userLogged.defaultClub ? '?club='+userLogged.defaultClub+'&limit=10000' : ''}`;
        getPlayers(querystring);
        setClearDependencies(true);
    }, []);

    if(false){
        console.log(removeParams)
        console.log(querystring);
        console.log(tracks);
    }

    return (
    <Grid container direction="row" className={classes.container}>
        <Grid item xs={12} md={8} className="side-center">
            <Grid container direction="row">
                <Grid item xs={12} >

                    <Grid container direction="row">
                        { !refreshChart &&                         
                            <Charts
                                group={group}
                                teams={teams}
                                tracks={tracks}
                                charts={charts}
                                options={options}
                                players={players}
                                sessions={sessions}
                                setCharts={setCharts}
                                querystring={querystring}
                                teamsUnselect={teamsUnselect}
                                tracksUnselect={tracksUnselect}
                                athleteSessions={athleteSessions}
                                sessionsUnselect={sessionsUnselect}
                                playersUnselected={playersUnselected}
                                tracksOfSessionsUnselect={tracksOfSessionsUnselect}
                                handleFilterPlayersAvailable={handleFilterPlayersAvailable}
                            />
                        }
                        <Grid item xs={12} md={6} >
                            <AddChart
                                options={options}
                                filterList={filterList}
                                handleAddChart={handleAddChart}
                                filterSelected={filterSelected}
                                athleteSessions={athleteSessions}
                            />
                        </Grid>
                    </Grid>

                </Grid>
                { false &&
                <Grid item xs={12} >
                    <div className="gps-graphic">GPS 3</div>
                </Grid>
                }
                { false &&
                <Grid item xs={12} >
                    <div >AVG TABLE</div>
                </Grid>
                }
            </Grid>
        </Grid>
        <Grid item xs={12} md={5} className="side-right">
            <Grid container direction="row">
                <div className="players-select-quick-buttom" onClick={ () => handleSelectAllPlayer() }>{translate('select all')}</div>
                <Grid item xs={12} md={4} className="grid-avatars" >
                    {buildAvatars()}
                </Grid>
                <Grid item xs={12} md={4} className="filter-grid">
                    
                    <FiltersMetrics
                        filterList={filterList}
                        metricsData={metricsData}
                        setSaveFilter={setSaveFilter} 
                        filterSelected={filterSelected}
                        setMetricsData={setMetricsData}
                        setFilterSelected={setFilterSelected}
                        handleSelectFilter={handleSelectFilter}
                    />

                </Grid>
                <Grid item xs={12} md={4} className="work-grid">
                    
                    <Work
                        group={group}
                        teams={teams}
                        tracks={tracks}
                        sessions={sessions}
                        dateRange={dateRange}
                        setGroup={handleSetGroup}
                        stateGroupBy={stateGroupBy}
                        fieldPosition={fieldPosition}
                        teamsUnselect={teamsUnselect}
                        tracksUnselect={tracksUnselect}
                        setStateGroupBy={setStateGroupBy}
                        tracksOfSessions={tracksOfSessions}
                        setTeamsUnselect={setTeamsUnselect}
                        toggleDataPicker={toggleDataPicker}
                        setTrackUnselect={setTrackUnselect}
                        sessionsUnselect={sessionsUnselect}
                        setFilterSelected={setFilterSelected}
                        setSessionsUnselect={setSessionsUnselect}
                        handleChangePosition={handleChangePosition}
                        tracksOfSessionsUnselect={tracksOfSessionsUnselect}
                        setTrackOfSessionsUnselect={setTrackOfSessionsUnselect}
                    />

                </Grid>
            </Grid>
        </Grid>

        <Modal
            open={openDataPickerModal}
            onClose={toggleDataPicker}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            <div className={classes.paper}>
                <DateRangePicker
                    open={true}
                    toggle={toggleDataPicker}
                    onChange={ (range) => setDateRange(range)}
                    wrapperClassName={{dataPickerIssues: "dataPickerIssues"}}
                />
            </div>
        </Modal>
    </Grid>
    );
};
export default Gps;