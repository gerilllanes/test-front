import React, {useState} from 'react';

import translate from "../../../lang/lang";
//import Radio from '@material-ui/core/Radio';
import AddCircleIcon from '@material-ui/icons/AddCircle';

const AddChart = (props) => {

    const { handleAddChart, options, athleteSessions, filterList, filterSelected } = props;
    //const defaultState = {metric: 'time', groupBy: 'player', filterBy: 'average'};
    //const [state, setState] = useState(defaultState);

    const addChart = () => {
        if( athleteSessions && athleteSessions.team_sessions ){
            handleAddChart();
            //handleAddChart(state);
            //setState(defaultState);
        }
    };
    
    return (
        <div className="gps-graphic">
            { filterSelected !== null && filterList.length > 0 ?
                <button onClick={ () => addChart() }>
                    <span>
                        {translate('add chart')}{ ` (${filterList[filterSelected].desc})` }
                    </span>
                    <AddCircleIcon />
                </button>
            :
                <button><span>{ filterList.length > 0 ? translate(`select a filter`) : translate('add your first filter')}</span></button>
            }
        </div>
    );
};
export default AddChart;