import React, {useState} from 'react';

import Box from '@material-ui/core/Box';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import translate from "../../../lang/lang";
import Radio from '@material-ui/core/Radio';
import Switch from '@material-ui/core/Switch';
import AppBar from '@material-ui/core/AppBar';
import Tooltip from '@material-ui/core/Tooltip';
import Checkbox from '@material-ui/core/Checkbox';
import { withStyles, makeStyles } from "@material-ui/core/styles";
import FooballFieldPositions from '../../../components/FooballFieldPositions/FooballFieldPositions';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
        {value === index && (
            <Box className="tab-box">
                {children}
            </Box>
        )}
        </div>
    );
}

const useStyles = makeStyles( () => ({

    workContainer: {
        cursor: 'pointer',
        background: '#1e4249a8',
        '& .datapicker-container': {
            margin: '2px 0 0'
        },
        '& .title': {
            display: 'flex',
            fontSize: '1rem',
            background: '#0A1A24',
            padding: '4px 7.5px 0',
            justifyContent: 'center'
        },
        '& .datapicker-to': {
            display: 'flex'
        },
        '& .datapicker-item': {
            display: 'flex',
            justifyContent: 'space-around',
            '& .datapicker-label': {
                fontSize: '.7rem',
                margin: '0 0 -4px'
            }
        },
        '& .border-bottom': {
            borderBottom: '1px solid'
        },
        '& .select-track-session-container': {
            display: 'flex',
            minHeight: '2.8rem',
            justifyContent: 'space-around',
            '& .select-track-session-item': {
                fontSize: '.8rem',
                margin: '2px 0 0'
            },
            '& .select-track-session-item-switch': {
                margin: '8px 0 0',
                position: 'absolute',
                '& .MuiSwitch-colorSecondary.Mui-checked': {
                    color: '#0A1A24',
                },
                '& .MuiSwitch-colorSecondary.Mui-checked + .MuiSwitch-track': {
                    backgroundColor: '#143338'
                }
            }
        },
        '& .track-session-list-container': {
            height: '8rem',
            //width: '100%',
            border: '1px solid #ffffff',
            padding: '4px',
            '& .track-item-container': {
                display: 'flex',
                justifyContent: 'space-between',
                '& .check-select-all-track-container': {
                    '& .label-all': {
                        top: '-2px',
                        cursor: 'pointer',
                        position: 'relative',
                    },
                    '& .check-select-all-tracks': {
                        margin: '2px 5px',
                        cursor: 'pointer',
                        '&:checked': {
                            background: 'pink'
                        }
                    },
                },
                '& .track-item-label':{
                    width: '100%',
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                    textOverflow: 'ellipsis',
                },
                '& .MuiCheckbox-colorSecondary.Mui-checked': {
                    padding: 0,
                    color: '#76a6af',
                },
                '& .MuiCheckbox-root.MuiCheckbox-colorSecondary': {
                    padding: 0
                }
            },
            '& .tabs-track-session-container': {
                '& .MuiTab-root': {
                    width: '50%',
                    padding: '2px',
                    fontWeight: 700,
                    fontSize: '.55rem',
                    minWidth: 'inherit',
                    minHeight: 'inherit'
                }
            },
            '& .MuiAppBar-positionStatic': {
                backgroundColor: '#0a1a24',
                '& .MuiTabs-root': {
                    minHeight: 'inherit',
                    '& .MuiTabs-scroller': {
                        '& > :nth-child(2)' : {
                            backgroundColor: '#5de6e6',
                        }
                    }
                }
            },
            '& .TabPanel-Container': {
                padding: '2px 0',
                overflow: 'auto',
                maxHeight: 'calc(100% - 15px)'
            }
        },
        '& .track-session-list-container.overflow': {
            overflow: 'auto'
        },
        '& .gps-graphic-metric-group': {
            padding: '6px 0',
            '& .gps-graphic-metric-group-title': {
                padding: '0 4px'
            },
            '& .options-group-container': {
                display: 'flex',
                flexWrap: 'wrap',
                '& .options-group-container-item': {
                    '& label': {
                        cursor: 'pointer'
                    },
                    '& .radio-button-player-label': {
                        fontSize: '.8rem'
                    }
                }
            },
            '& .MuiRadio-root': {
                padding: '2px 4px 2px 2px'
            }
        },
        '& .gps-filter-by-team-container': {

            '& .track-item-container': {
                padding: '4px',
                display: 'flex',
                justifyContent: 'space-between',
                '& .label-all': {
                    margin: '0 4px'
                }
            },
            '& .team-list-container': {
                overflow: 'auto',
                maxHeight: '69px',
                '& .team-item-container': {
                    display: 'flex',
                    justifyContent: 'space-between',
                    '& .team-item': {
                        maxWidth: '75%',
                        overflow: 'hidden',
                        margin: '0 0 0 4px',
                        whiteSpace: 'nowrap',
                        textOverflow: 'ellipsis',
                    },
                    '& .team-item-check': {
                        '& .MuiButtonBase-root': {
                            padding: '0 4px 0 0',
                            '& .MuiIconButton-label': {
                                color: '#76a6af'
                            }
                        },
                        '& .MuiCheckbox-colorSecondary.Mui-checked': {
                            color: '#76a6af'
                        }
                    }
                }
            }
        },
        '& .gps-filter-by-position-container': {
            display: 'flex',
            padding: '4px 0 0px',
            justifyContent: 'center',
        }
    },
    customWidth: {
        maxWidth: 200,
    },
}));

const Work = (props) => {

    const classes = useStyles();
    const [trackSessionTabs, setTrackSessionTabs] = useState(false);

    const {
        group,
        teams,
        tracks,
        setGroup,
        sessions,
        dateRange,
        stateGroupBy,
        fieldPosition,
        teamsUnselect,
        tracksUnselect,
        setStateGroupBy,
        setTeamsUnselect,
        toggleDataPicker,
        setTrackUnselect,
        tracksOfSessions,
        sessionsUnselect,
        setFilterSelected,
        setSessionsUnselect,
        handleChangePosition,
        tracksOfSessionsUnselect,
        setTrackOfSessionsUnselect
    } = props;

    const fixDuration = (timeStamp) => {
        const newDate = new Date(timeStamp * 1000).toUTCString();
        return newDate.split(' ')[4];
    }

    const fixTimestampToHours = (timeStamp) => {
        let newDate = new Date(timeStamp * 1000).toUTCString();
        /*
        const h = newDate.getHours();
        const m = newDate.getMinutes();
        return `${h < 10 ? `0${h}`:h}:${m < 10 ? `0${m}`:m}`;
        */
        newDate = newDate.split(' ')[4];
        return newDate.substring(0, newDate.length - 3);
    }

    const HtmlTooltip = withStyles( (theme) => ({
        tooltip: {
            maxWidth: 360,
            fontSize: '.6rem',
            backgroundColor: '#0A1A24',
            border: '1px solid #dadde9',
            color: '#ffffff',
        },
    }))(Tooltip);

    const handleTrackChange = (index) => {
        if( tracksUnselect.includes(index) ){
            setTrackUnselect(tracksUnselect.filter( trkUns => { return trkUns !== index }));
        }else{
            setTrackUnselect([...tracksUnselect, index ]);
        }
    }

    const handleSessionChange = (index) => {
        if( sessionsUnselect.includes(index) ){
            setSessionsUnselect(sessionsUnselect.filter( trkUns => { return trkUns !== index }));
        }else{
            setSessionsUnselect([...sessionsUnselect, index ]);
        }
    }

    const handleTrackOfSessionChange = (index) => {
        if( tracksOfSessionsUnselect.includes(index) ){
            setTrackOfSessionsUnselect(tracksOfSessionsUnselect.filter( trkUns => { return trkUns !== index }));
        }else{
            setTrackOfSessionsUnselect([...tracksOfSessionsUnselect, index ]);
        }
    }

    const buildSessionsList = () => {

        return sessions.map( (session, index) => {
            return  <div key={`session-${index}`} className="track-item-container">

                        { false && 
                        <Tooltip title={`${translate('date')}: ${session.start_timestamp} ${translate('player')}: ${session.club.name} `} classes={{ tooltip: classes.customWidth }} placement="left">
                            <div className="track-item-label">{session.club.name}</div>
                        </Tooltip>
                        }

                        <HtmlTooltip
                            title={
                                <>
                                <div style={{display: 'flex', justifyContent : 'space-around'}}>
                                    <div>
                                        <em>{"Session:"}</em> <b>{session.id}</b>
                                    </div>
                                    <span><em>{`${translate('N°_tracks')}: `}</em><b>{session.n_tracks}</b></span>
                                    <span>{session.start_timestamp}</span>
                                </div><br/>
                                <div style={{display: 'flex', justifyContent: 'space-around'}}>
                                    <span><b>{translate('club')}: </b>{session.club.name}</span>
                                    <span><b>{translate('team')}: </b>{session.team.name}</span>
                                </div>
                                <br/>
                                <b><u>GPS {translate('data')}</u></b><br/><br/>
                                <div style={{display: 'flex'}}>
                                    <div style={{margin: '0 15px 0 0'}}>
                                        { session.notes && session.notes !== '' && <><b>{translate('notes')}: </b><span>{session.notes}</span><br/></> }
                                        <b>{translate('total_time')}: </b><span>{session.total_time}</span><br/>
                                        <b>{translate('duration')}: </b><span>{session.duration}</span><br/>
                                        <b>{translate('total_energy')}: </b><span>{session.total_energy}</span><br/>
                                        <b>{translate('recovery_average_time')}: </b><span>{session.recovery_average_time}</span><br/>
                                        <b>{translate('recovery_average_power')}: </b><span>{session.recovery_average_power}</span><br/>
                                        <b>{translate('power_events')}: </b><span>{session.power_events}</span><br/>
                                        <b>{translate('max_values_speed')}: </b><span>{session.max_values_speed}</span><br/>
                                        <b>{translate('max_values_acc')}: </b><span>{session.max_values_acc}</span><br/>
                                    </div>
                                    <div style={{margin: '0 0 0 15px'}}>
                                        <b>{translate('equivalent_distance_index')}: </b><span>{session.equivalent_distance_index}</span><br/>
                                        <b>{translate('equivalent_distance')}: </b><span>{session.equivalent_distance}</span><br/>
                                        <b>{translate('average_v')}: </b><span>{session.average_v}</span><br/>
                                        <b>{translate('average_power')}: </b><span>{session.average_power}</span><br/>
                                        <b>{translate('average_muscular_power')}: </b><span>{session.average_muscular_power}</span><br/>
                                        <b>{translate('average_hr')}: </b><span>{session.average_hr}</span><br/>
                                        <b>{translate('average_distance')}: </b><span>{session.average_distance}</span><br/>
                                        <b>{translate('anaerobic_energy')}: </b><span>{session.anaerobic_energy}</span><br/>
                                    </div>
                                </div>
                                </>
                            }
                            placement="left"
                        >
                            <div className="track-item-label">{session.start_timestamp}</div>
                        </HtmlTooltip>

                        <div className="track-item-check">
                            <Checkbox
                                checked={!sessionsUnselect.includes(index)}
                                onChange={ () => handleSessionChange(index) }
                            />
                        </div>
                    </div>
        });
    }

    const buildTracksList = (trk) => {
        
        return trk.map( (track, index) => {
            
            return  <div key={`track-${index}`} className="track-item-container">
                        { false && 
                        <Tooltip title={`${translate('date')}: ${track.start_timestamp} ${translate('player')}: ${track.player.short_name} `} classes={{ tooltip: classes.customWidth }} placement="left">
                            <div className="track-item-label">{track.player.short_name}</div>
                        </Tooltip>
                        }

                        <HtmlTooltip
                            title={
                                <>
                                <div style={{display: 'flex', justifyContent : 'space-around'}}>
                                    <div>
                                        <div></div><em>{"Track:"}</em> <b>{track.id}</b>
                                    </div>
                                    <span>{track.start_timestamp ? track.start_timestamp : track.timestamp }</span>
                                </div><br/>
                                <b>{translate('club')}: </b><span>{track.club.name}</span><br/>
                                <b>{translate('team')}: </b><span>{track.team.name}</span><br/>
                                <b>{translate('player')}: </b><span>{track.player.short_name}</span><br/>
                                <br/>
                                <b><u>GPS {translate('data')}</u></b><br/><br/>
                                { track.device && <><em>{"Device: "}</em> <b>{track.device}</b></> }
                                <b>{translate('timezone')}: </b><span>{track.timezone}</span><br/>
                                <b>{translate('start time')}: </b><span>{fixTimestampToHours(track.timestamp_utc)}</span><br/>
                                <b>{translate('duration')}: </b><span>{fixDuration(track.duration)}</span><br/>
                                <b>{translate('sample count')}: </b><span>{track.sample_count}</span><br/>
                                <b>{'dt'}: </b><span>{track.dt}</span><br/>
                                <b>{translate('event slope')}: </b><span>{track.event_slope}</span><br/>
                                { track.notes && track.notes !== '' && <><b>{translate('notes')}: </b><span>{track.notes}</span><br/></> }
                                </>
                            }
                            placement="left"
                        >
                            <div className="track-item-label">{track.player.short_name}</div>
                        </HtmlTooltip>

                        <div className="track-item-check">
                            <Checkbox
                                checked={ group ? !tracksOfSessionsUnselect.includes(index) : !tracksUnselect.includes(index)}
                                onChange={ () => group ? handleTrackOfSessionChange(index) : handleTrackChange(index) }
                            />
                        </div>
                    </div>
        });
    }

    const handleSelectAllTracks = (checked, type) => {
        if(type === 'tracks_of_sessions'){
            if(checked){
                setTrackOfSessionsUnselect([]);
            }else{
                setTrackOfSessionsUnselect(tracks.map( (track, index) => {return  index} ));
            }
        }else if(type === 'sessions'){
            if(checked){
                setSessionsUnselect([]);
            }else{
                setSessionsUnselect(sessions.map( (session, index) => {return  index} ));
            }
        }else{
            if(checked){
                setTrackUnselect([]);
            }else{
                setTrackUnselect(tracks.map( (track, index) => {return  index} ));
            }
        }
    }

    const buildTracksOfSessionsList = () => {

        if(tracksOfSessions){
            let newTracksOfSessions = [];
            Object.keys(tracksOfSessions).forEach( (key, index) => {
                if( !sessionsUnselect.includes(index) ){
                    newTracksOfSessions = [...newTracksOfSessions, ...tracksOfSessions[key] ];
                }
            });
            return  <>
                        <div className="track-item-container" style={{marginBottom: '2px', background: '#20505a'}}>
                            <span>{`Total: ${newTracksOfSessions.length}`}</span>
                            <div className="check-select-all-track-container">
                                <label htmlFor="check-select-all-tracks" className="label-all">all</label>
                                <input
                                    type="checkbox"
                                    id="check-select-all-tracks"
                                    className="check-select-all-tracks"
                                    onChange={ (e) => handleSelectAllTracks(e.target.checked, 'tracks_of_sessions') }
                                    checked={tracksOfSessionsUnselect.length === 0 ? true : false}
                                />
                            </div>
                        </div>
                        {buildTracksList(newTracksOfSessions)}
                    </>
        }else{
            return [];
        }
    }

    const handleSelectAllTeams = (checked) => {
        if(checked){
            setTeamsUnselect([]);
        }else{
            setTeamsUnselect(Object.keys(teams).map( (key, xd) => xd ) );
        }
    }

    const handleTeamCheckChange = (xd, checked) => {
        if(checked){
            setTeamsUnselect( teamsUnselect.filter( team => team !== xd ) );
        }else{
            setTeamsUnselect([...teamsUnselect, xd]);
        }
    }

    const buildTeamsAvailable = () => {

        if(teams){
            return  <>
                    <div className="track-item-container" style={{marginBottom: '2px', background: '#20505a'}}>
                        <span>{`Total: ${Object.keys(teams).length}`}</span>
                        <div className="check-select-all-track-container">
                            <label htmlFor="check-select-all-teams" className="label-all">all</label>
                            <input
                                type="checkbox"
                                id="check-select-all-teams"
                                className="check-select-all-tracks"
                                onChange={ (e) => handleSelectAllTeams(e.target.checked) }
                                checked={teamsUnselect.length === 0 ? true : false}
                            />
                        </div>
                    </div>
                    <div className="team-list-container">
                        {   Object.keys( teams ).map( key => {
                                const unselectTeam = teamsUnselect.includes((key * 1));
                                return  <div key={`team-index${key}`} className="team-item-container">
                                            <div className="team-item">
                                                {teams[key].name}
                                            </div>
                                            <div className="team-item-check">
                                                <Checkbox
                                                    checked={ !unselectTeam }
                                                    onChange={ (e) => handleTeamCheckChange( (key * 1), e.target.checked) }
                                                />
                                            </div>
                                        </div>
                            })
                        }
                    </div>
                </>
        }else{
            return <></>
        }


    }

    function a11yProps(index) {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    }

    return (
        <div className={classes.workContainer} >
            <div className="title">{translate('work')}</div>
            <div className="datapicker-container border-bottom" onClick={ () => toggleDataPicker() }>
                <div className="datapicker-item">
                    <span className="datapicker-label">{translate('from')}</span>
                    <span className="datapicker-value">
                        {dateRange.startDate.toLocaleString('en-US', {
                            day: 'numeric',
                            year: 'numeric',
                            month: 'long'
                        })}
                    </span>
                </div>
                <div className="datapicker-item">
                    <span className="datapicker-label">{translate('to')}</span>
                    <span className="datapicker-value">
                        {dateRange.endDate.toLocaleString('en-US', {
                            day: 'numeric',
                            year: 'numeric',
                            month: 'long'
                        })}
                    </span>
                </div>
            </div>
            <div className="select-track-session-container border-bottom">
                <div className="select-track-session-item">{ group ? translate('sessions') : translate('tracks')}</div>
                <div className="select-track-session-item-switch">
                    <Switch
                        checked={group}
                        onChange={ () => setGroup(!group)}
                        name="track_session"
                    />
                </div>
                <div className="select-track-session-item">{translate('group')}</div>
            </div>
            <div className={`track-session-list-container${!group ? ' overflow': ''}`}>
                { group ? <>
                    <AppBar className="tabs-track-session-container" position="static">
                        <Tabs value={trackSessionTabs ? 1 : 0} onChange={ () => setTrackSessionTabs(!trackSessionTabs)} aria-label="simple tabs example">
                            <Tab className="tab-item" label={`${translate('sessions')}`} {...a11yProps(0)} />
                            <Tab className="tab-item" label={`${translate('tracks')}`} {...a11yProps(1)} />
                        </Tabs>
                    </AppBar>
                    <TabPanel className="TabPanel-Container" value={trackSessionTabs ? 1 : 0} index={0}>
                        <div className="track-session-list-container-tab">
                            <div className="track-item-container" style={{marginBottom: '2px', background: '#20505a'}}>
                                <span>{`Total: ${sessions.length}`}</span>
                                <div className="check-select-all-track-container">
                                    <label htmlFor="check-select-all-sessions" className="label-all">all</label>
                                    <input
                                        type="checkbox"
                                        id="check-select-all-sessions"
                                        className="check-select-all-tracks"
                                        onChange={ (e) => handleSelectAllTracks(e.target.checked, 'sessions') }
                                        checked={sessionsUnselect.length === 0 ? true : false}
                                    />
                                </div>
                            </div>
                            {buildSessionsList()}
                        </div>
                    </TabPanel>
                    <TabPanel className="TabPanel-Container" value={trackSessionTabs ? 1 : 0} index={1}>
                        <div className="track-session-list-container-tab">
                            {buildTracksOfSessionsList()}
                        </div>
                    </TabPanel>
                </>        
                :
                    <>
                    <div className="track-item-container" style={{marginBottom: '2px', background: '#20505a'}}>
                        <span>{`Total: ${tracks.length}`}</span>
                        <div className="check-select-all-track-container">
                            <label htmlFor="check-select-all-sessions" className="label-all">all</label>
                            <input
                                type="checkbox"
                                id="check-select-all-sessions"
                                className="check-select-all-tracks"
                                onChange={ (e) => handleSelectAllTracks(e.target.checked, 'tracks') }
                                checked={tracksUnselect.length === 0 ? true : false}
                            />
                        </div>
                    </div>
                    {buildTracksList(tracks)}
                    </>
                }
            </div>

            <div className="gps-graphic-metric-group">
                <div className="gps-graphic-metric-group-title">{translate('group by')}:</div>
                <div className="options-group-container">
                    <div className="options-group-container-item">
                        <Radio
                            size="small"
                            value="player"
                            color="default"
                            id="radio-button-player"
                            name="radio-button-player"
                            onChange={ () => { setFilterSelected(null); setStateGroupBy('player'); } }
                            checked={stateGroupBy === 'player'}
                        />
                        <label className="radio-button-player-label" htmlFor="radio-button-player">{translate('player')}</label>
                    </div>
                    <div className="options-group-container-item">
                        <Radio
                            size="small"
                            value="track"
                            color="default"
                            id="radio-button-track"
                            name="radio-button-track"
                            onChange={ () => { setFilterSelected(null); setStateGroupBy('track')} }
                            checked={stateGroupBy === 'track'}
                        />
                        <label className="radio-button-player-label" htmlFor="radio-button-track">{translate('track')}</label>
                    </div>
                    <div className="options-group-container-item">
                        <Radio
                            size="small"
                            value="date"
                            color="default"
                            id="radio-button-date"
                            name="radio-button-date"
                            onChange={ () => { setFilterSelected(null); setStateGroupBy('date');} }
                            checked={stateGroupBy === 'date'}
                        />
                        <label className="radio-button-player-label" htmlFor="radio-button-date">{translate('date')}</label>
                    </div>
                </div>
            </div>

            <div className="gps-filter-by-team-container">
                <div className="title">{translate('team')}</div>
                { buildTeamsAvailable() }
            </div>

            <div className="gps-filter-by-position-container">
                <FooballFieldPositions onChange={handleChangePosition} value={fieldPosition} />
            </div>

        </div>
    );
};
export default Work;