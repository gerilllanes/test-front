import React from 'react';

import Grid from "@material-ui/core/Grid";
import { fixSecoundsToHourFormat, fixHourFormatToSecounds } from "../../../utils/utils";
import Highchart from "../../../components/Highcharts/HighChart";

const calcStringTime = (time) => {
    let seconds = time.split(":");
    if(seconds.length === 3){
        seconds = ( ((seconds[0] * 1) * 3600) + ((seconds[1] * 1) * 60) + (seconds[2] * 1) );
    }else{
        seconds = ( ((seconds[0] * 1) * 60) + (seconds[1] * 1) );
    }
    return seconds;
}

const Charts = (props) => {

    const {
        //filterTeam,
        //teams,
        group,
        charts,
        players,
        options, 
        sessions,
        //teamsUnselect,
        tracksUnselect,
        athleteSessions,
        sessionsUnselect,
        playersUnselected,
        tracksOfSessionsUnselect,
        handleFilterPlayersAvailable
    } = props;

    const fixMetricName = (metric) => {
        if(metric === 'duration'){
            metric = 'time';
        }else if(metric === 'distance'){
            metric = 'total_distance';
        }else if(metric === 'energy'){
            metric = 'total_energy';
        }
        return metric;
    }

    const isTrackFilter = (player, playersAvailable, aS, ats) => {
        
        const playerIsSelectable = Object.keys(playersAvailable).filter( key => {
            return player.id === playersAvailable[key].id; 
        })[0];
        let unselected = true;
        if(playerIsSelectable && player && !playersUnselected.includes(player.id) ){

            /*
            console.log('infilter', player);
            console.log(teams);
            console.log(teamsUnselect);
            */

            unselected = false;
            const session = sessions.filter( ses => {
                return ses.id === aS.team_session_id 
            })[0];
            // verify by session unselect and tracks
            let trackIsUnselected = null;
            if(session){
                const sessionIsUnselected = sessionsUnselect.filter( sesUn => {
                    return sessions[sesUn].id === session.id;
                })[0];
                unselected = sessionIsUnselected ? true : false;
                trackIsUnselected = tracksOfSessionsUnselect.filter( trk => {
                    return aS.track_id === ( ats[trk] ? ats[trk].track_id : null );
                })[0];
            }else{
                trackIsUnselected = tracksUnselect.filter( trk => {
                    return aS.track_id === ats[trk].track_id;
                })[0];
            }
            unselected = trackIsUnselected ? true : unselected;
        }
        return unselected;    
    }

    const buildChartPlayersFilterMetricGroupByPlayer = (chart, xd, playersAvailable) => {
        
        let chartMetric = Object.keys(chart.filter[Object.keys(chart.filter)[0]].metrics)[0];
        const chartFilterBy = Object.keys(chart.filter)[0];

        chartMetric = fixMetricName(chartMetric);

        let seriePlayer = {};
        let serieDrill = {};
        const divider = chartMetric === 'time' ? 60 : 1;
        const ats = group ? athleteSessions.team_sessions : athleteSessions.tracks;
        
        ats.forEach( aS => {
            
            const player = players.filter( p => { return p.id === aS.player_id })[0];
            const isTrackFilterValue = isTrackFilter(player, playersAvailable, aS, ats);
            if(!isTrackFilterValue){    
                //let beforeSeg = null;                
                let valueY = 0;
                let metricValue = aS[chartMetric] ? aS[chartMetric] : 0;
                let drillValue = (metricValue * 1);
                let labelSerie = null;
                labelSerie = aS['time'];
                labelSerie = labelSerie ? calcStringTime(labelSerie) : 0;
                if(seriePlayer[aS.player_id]){
                    if(chartFilterBy === 'average'){
                        if(chartMetric === 'time'){
                            metricValue = aS[chartMetric];
                            //labelSerie = metricValue;
                            valueY = calcStringTime(metricValue);
                            valueY = seriePlayer[aS.player_id].y + valueY;
                            drillValue = (calcStringTime(metricValue) / 60);
                        }else{
                            //labelSerie = aS['time'];
                            //labelSerie = labelSerie ? calcStringTime(labelSerie) : 0;
                            valueY = (seriePlayer[aS.player_id].y + (metricValue * 1));
                        }
                        labelSerie = ((seriePlayer[aS.player_id].label * 1) + labelSerie);
                    }else if(chartFilterBy === 'maximun'){
                        if( seriePlayer[aS.player_id].y > (metricValue * 1) ){
                            valueY = seriePlayer[aS.player_id].y;
                            labelSerie = (seriePlayer[aS.player_id].label * 1);
                        }else{
                            valueY = metricValue;
                            labelSerie = labelSerie;
                        }
                    }else if(chartFilterBy === 'minimun'){
                        if( seriePlayer[aS.player_id].y < (metricValue * 1) ){
                            valueY = seriePlayer[aS.player_id].y;
                            labelSerie = (seriePlayer[aS.player_id].label * 1);
                        }else{
                            valueY = metricValue;
                            labelSerie = labelSerie;
                        }
                    }else{ // Total
                        valueY = seriePlayer[aS.player_id].y + (metricValue * 1);
                        labelSerie = labelSerie + (seriePlayer[aS.player_id].label * 1);
                    }
                    //beforeSeg = fixHourFormatToSecounds(seriePlayer[aS.player_id].label);
                    //console.log('transform hour', seriePlayer[aS.player_id].label, ' --> ', beforeSeg);
                    serieDrill = {...serieDrill, [aS.player_id]: {...serieDrill[aS.player_id], data: [...serieDrill[aS.player_id].data, ...[ [`track ${aS.track_id}`,  drillValue ]  ] ] } }
                    seriePlayer = {...seriePlayer, [aS.player_id]: {...seriePlayer[aS.player_id], y: valueY, count: seriePlayer[aS.player_id].count + (chartFilterBy === 'average' ? 1 : 0), label: labelSerie } }
                }else{
                    //labelSerie = aS['time'];
                    //labelSerie = labelSerie ? calcStringTime(labelSerie) : 0;
                    valueY = metricValue ? (metricValue * 1) : 0;
                    if(chartFilterBy === 'average'){
                        if(chartMetric === 'time'){
                            metricValue = aS[chartMetric];
                            //labelSerie = metricValue;
                            valueY = metricValue ? calcStringTime(metricValue) : 0;
                            drillValue = (calcStringTime(metricValue) / 60);
                        }
                    }
                    seriePlayer = {...seriePlayer, [aS.player_id]: {name: player.short_name ? player.short_name : '', color: player.color, y: valueY, drilldown: player.id, count: 1, label: `${labelSerie}` } }
                    serieDrill = {...serieDrill, [aS.player_id]: {name: player.short_name ? player.short_name : '', id: player.id, data: [ [`track ${aS.track_id}`, drillValue] ] } }
                }
            }
        });
        const serie_Player = Object.keys(seriePlayer).map( sp => { return {...seriePlayer[sp], y: ((seriePlayer[sp].y / seriePlayer[sp].count)/divider), label: fixSecoundsToHourFormat((seriePlayer[sp].label / seriePlayer[sp].count) * 1) } } );
        const serie_Drill = Object.keys(serieDrill).map( dr => serieDrill[dr] );
        const config = {
            chart: {
                type: 'column',
                backgroundColor: 'transparent',
            },
            height: '200px',
            title: {
                text: ''
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: `${chartFilterBy} ${options[chartMetric].label} ${options[chartMetric].unit}`
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        //format: `{point.y:.1f} ${options[chartMetric].unit}`
                        rotation: -90,
                        format: `{point.label}`,
                        y: -25,
                        x: 0
                    }
                }
            },
            tooltip: {
                borderWidth: 2,
                backgroundColor: '#102E2F',
                headerFormat: '{series.name}<br>',
                pointFormatter: function () {
                    if (this.series.chart.drilldownLevels && this.series.chart.drilldownLevels.length > 0) {
                        return `${this.name}: <b>${this.y.toFixed(2)} ${options[chartMetric].unit}</b> of ${options[chartMetric].label}`;
                    } else {
                        return `${this.name}: <b>${this.y.toFixed(2)} ${options[chartMetric].unit}</b> of ${chartFilterBy} ${options[chartMetric].label}<br/>Duration: ${this.label}`;
                    }
                },
                style:{
                    fontSize:'15px'
                }
            },
            series: [
                {
                    name: "Players",
                    colorByPoint: true,
                    data: serie_Player
                }
            ],
            drilldown: {
                series: serie_Drill,
            }
        }
        return <Highchart id={`mychart-${xd}`} type="chart" drilldown={true} config={config} />
        //return <div>{`chart of`}</div>
    }

    const buildChartPlayersFilterTwoMetricsGroupByPlayer = (chart, xd, playersAvailable) => {
        
        let chartMetric = Object.keys(chart.filter[Object.keys(chart.filter)[0]].metrics);

        let chartFilterBy = Object.keys(chart.filter)[0];

        if(Object.keys(chart.filter).length > 1){
            chartFilterBy = [chartFilterBy, Object.keys(chart.filter)[1] ];
            chartMetric = [...chartMetric, Object.keys(chart.filter[Object.keys(chart.filter)[1]].metrics)[0] ];
        }else{
            chartFilterBy = [chartFilterBy, chartFilterBy ];
        }

        chartMetric[0] = fixMetricName(chartMetric[0]);
        chartMetric[1] = fixMetricName(chartMetric[1]);

        //console.log('chartMetric', chartMetric);
        //console.log('chartMetric', chart.filter);

        let series = [
            {
                name: `${options[chartMetric[0]].label}`,
                color: 'rgba(148,261,63,.9)',
                data: [],
                tooltip: {
                    valuePrefix: '',
                    valueSuffix: ` ${options[chartMetric[0]].unit}`
                },
                pointPadding: 0.3,
                pointPlacement: 0,
                yAxis: 0
            }, {
                name: `${options[chartMetric[1]].label}`,
                color: 'rgba(116,60,261,.9)',
                data: [],
                tooltip: {
                    valuePrefix: '',
                    valueSuffix: ` ${options[chartMetric[1]].unit}`
                },
                pointPadding: 0.3,
                pointPlacement: 0.1,
                yAxis: 1
            }
        ];

        let categories = {};
        const ats = group ? athleteSessions.team_sessions : athleteSessions.tracks;
        ats.forEach( aS => {
            const player = players.filter( p => { return p.id === aS.player_id })[0];
            const isTrackFilterValue = isTrackFilter(player, playersAvailable, aS, ats);
            if(!isTrackFilterValue){
                if(categories[player.id]){
                    categories = {...categories, [player.id] : {name: player.short_name, values: [...categories[player.id].values, { [chartMetric[0]]: aS[chartMetric[0]], [chartMetric[1]]: aS[chartMetric[1]] } ] } };
                }else{
                    categories = {...categories, [player.id] : {name: player.short_name, values: [{ [chartMetric[0]]: aS[chartMetric[0]], [chartMetric[1]]: aS[chartMetric[1]] } ] } };
                }
            }
        });
        //console.log('cat', categories)
        Object.keys(categories).forEach( cat => {
            series = series.map( (ser, xd) => {
                let value = 0;
                if(categories[cat].values.length < 2){
                    value = categories[cat].values[0][chartMetric[xd]] * 1;
                }else{
                    if(chartFilterBy[xd] === 'average'){
                        value = categories[cat].values.reduce( (accumulator, currentValue) => {
                            let accum = accumulator[chartMetric[xd]] ? (accumulator[chartMetric[xd]] * 1) : accumulator;
                            return accum + (currentValue[chartMetric[xd]] * 1)
                        });
                        value = value / ( categories[cat].values.length > 0 ? categories[cat].values.length : 1);
                    }else if(chartFilterBy[xd] === 'total'){
                        value = categories[cat].values.reduce( (accumulator, currentValue) => {
                            let accum = accumulator[chartMetric[xd]] ? (accumulator[chartMetric[xd]] * 1) : accumulator;
                            return accum + (currentValue[chartMetric[xd]] * 1)
                        });
                    }else if(chartFilterBy[xd] === 'minimun'){
                        value = categories[cat].values.reduce( (accumulator, currentValue) => {
                            let accum = accumulator[chartMetric[xd]] ? (accumulator[chartMetric[xd]] * 1) : accumulator;
                            let calc = accum < (currentValue[chartMetric[xd]] * 1);
                            calc = calc ? accum : (currentValue[chartMetric[xd]] * 1);
                            return calc
                        });
                    }else{
                        value = categories[cat].values.reduce( (accumulator, currentValue) => {
                            let accum = accumulator[chartMetric[xd]] ? (accumulator[chartMetric[xd]] * 1) : accumulator;
                            let calc = accum > (currentValue[chartMetric[xd]] * 1);
                            calc = calc ? accum : (currentValue[chartMetric[xd]] * 1);
                            return calc
                        });
                    }
                }
                if(typeof value === 'string'){ value = value * 1;}
                return {...ser, data: [...ser.data, (1 * ( value ? value.toFixed(2) : 0 ) ) ]};
            });
        });
        //console.log('cat', series);
        categories = Object.keys(categories).map( key => {
            return categories[key].name;
        });
        let config = {
            chart: {
                type: 'column',
                backgroundColor: 'transparent',
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: categories
            },
            yAxis: [
                {
                    min: 0,
                    labels: {
                        format: `{value} ${options[chartMetric[0]].unit}`
                    },
                    title: {
                        text: `${chartFilterBy[0]} ${options[chartMetric[0]].label}`
                    },
                },
                {
                    min: 0,
                    labels: {
                        format: `{value} ${options[chartMetric[1]].unit}`
                    },
                    title: {
                        text: `${chartFilterBy[1]} ${options[chartMetric[1]].label}`
                    },
                    opposite: true
                }
            ],
            legend: {
                shadow: false
            },
            tooltip: {
                //shared: true,
                backgroundColor: '#102E2F',
                style:{
                    fontSize:'15px'
                }
            },
            plotOptions: {
                column: {
                grouping: false,
                shadow: false,
                    borderWidth: 0
                }
            },
            series: series
        }
        return <Highchart id={`mychart-${xd}`} type="chart" drilldown={true} config={config} />
    }

    const buildChartPlayersFiltersMetricGroupByPlayer = (chart, xd, playersAvailable) => {
        const serieColors = [
            {color: 'rgba(165,170,217,.9)', pointPlacement: -0.2},
            {color: 'rgba(126,86,134,.9)', pointPlacement: -0.1},
            {color: 'rgba(248,161,63,.9)', pointPlacement: 0},
            {color: 'rgba(186,60,61,.9)', pointPlacement: 0.1}
        ];
        let chartMetric = Object.keys(chart.filter[Object.keys(chart.filter)[0]].metrics)[0];
        let series = Object.keys(chart.filter).map( key => {return key});

        chartMetric = fixMetricName(chartMetric);
        
        series = series.map( (filter, xd) => {
            return {name: filter, color: serieColors[xd].color, data: [], pointPadding: 0.3, pointPlacement: serieColors[xd].pointPlacement };
        });
        let categories = {};
        const ats = group ? athleteSessions.team_sessions : athleteSessions.tracks;
        ats.forEach( aS => {
            const player = players.filter( p => { return p.id === aS.player_id })[0];
            const isTrackFilterValue = isTrackFilter(player, playersAvailable, aS, ats);
            if(!isTrackFilterValue){
                if(categories[player.id]){
                    categories = {...categories, [player.id] : {name: player.short_name, values: [...categories[player.id].values, aS[chartMetric] ] } };
                }else{
                    categories = {...categories, [player.id] : {name: player.short_name, values: [aS[chartMetric]] } };
                }
            }
        });
        Object.keys(categories).forEach( cat => {
            series = series.map( ser => {
                let value = 0;
                if(ser.name === 'average'){
                    value = categories[cat].values.reduce( (accumulator, currentValue) => { return (accumulator * 1) + (currentValue * 1) });
                    value = value / ( categories[cat].values.length > 0 ? categories[cat].values.length : 1);
                }else if(ser.name === 'total'){
                    value = categories[cat].values.reduce( (accumulator, currentValue) => { return (accumulator * 1) + (currentValue * 1) });
                }else if(ser.name === 'minimun'){
                    value = categories[cat].values.reduce( (accumulator, currentValue) => { return ( 1 * ( (accumulator * 1) < (currentValue * 1) ? accumulator : currentValue ) ) });
                }else{
                    value = categories[cat].values.reduce( (accumulator, currentValue) => { return ( 1 * ( (accumulator * 1) > (currentValue * 1) ? accumulator : currentValue ) ) });
                }
                if(typeof value === 'string'){ value = value * 1;}
                return {...ser, data: [...ser.data, (1 * ( value ? value.toFixed(2) : 0 ) ) ]};
            });
        });
        categories = Object.keys(categories).map( key => {
            return categories[key].name;
        });
        let config = {
            chart: {
                type: 'column',
                backgroundColor: 'transparent',
            },
            title: {
                text: ``
            },
            xAxis: {
                categories: categories
            },
            yAxis: [
                {
                    min: 0,
                    labels: {
                        format: `{value} ${options[chartMetric].unit}`
                    },
                    title: {
                        text: options[chartMetric].label
                    }
                }
            ],
            legend: {
                shadow: false
            },
            tooltip: {
                shared: true,
                backgroundColor: '#102E2F',
                style:{
                    fontSize:'15px'
                }
            },
            plotOptions: {
                column: {
                grouping: false,
                shadow: false,
                    borderWidth: 0
                }
            },
            series: series
        }
        return <Highchart id={`mychart-${xd}`} type="chart" drilldown={true} config={config} />
    }

    const buildChartPlayersFiltersMetricsGroupByPlayer = (chart, xd) => {

        let config = {
            chart: {
                zoomType: 'xy',
                backgroundColor: 'transparent'
            },
            title: {
                text: 'Average Monthly Weather Data for Tokyo',
                align: 'left'
            },
            subtitle: {
                text: 'Source: WorldClimate.com',
                align: 'left'
            },
            xAxis: [{
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}°C',
                    style: {
                        color: 'red'
                    }
                },
                title: {
                    text: 'Temperature',
                    style: {
                        color: 'blue'
                    }
                },
                opposite: true
        
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Rainfall',
                    style: {
                        color: 'green'
                    }
                },
                labels: {
                    format: '{value} mm',
                    style: {
                        color: 'pink'
                    }
                }
        
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Sea-Level Pressure',
                    style: {
                        color: 'purple'
                    }
                },
                labels: {
                    format: '{value} mb',
                    style: {
                        color: 'gray'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: 'rgba(255,255,255,0.25)'
            },
            series: [{
                name: 'Rainfall',
                type: 'column',
                yAxis: 1,
                data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
                tooltip: {
                    valueSuffix: ' mm'
                }
        
            }, {
                name: 'Sea-Level Pressure',
                type: 'spline',
                yAxis: 2,
                data: [1016, 1016, 1015.9, 1015.5, 1012.3, 1009.5, 1009.6, 1010.2, 1013.1, 1016.9, 1018.2, 1016.7],
                marker: {
                    enabled: false
                },
                dashStyle: 'shortdot',
                tooltip: {
                    valueSuffix: ' mb'
                }
        
            }, {
                name: 'Temperature',
                type: 'spline',
                data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
                tooltip: {
                    valueSuffix: ' °C'
                }
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            floating: false,
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom',
                            x: 0,
                            y: 0
                        },
                        yAxis: [{
                            labels: {
                                align: 'right',
                                x: 0,
                                y: -6
                            },
                            showLastLabel: false
                        }, {
                            labels: {
                                align: 'left',
                                x: 0,
                                y: -6
                            },
                            showLastLabel: false
                        }, {
                            visible: false
                        }]
                    }
                }]
            }
        }
        return <Highchart id={`mychart-${xd}`} type="chart" drilldown={true} config={config} />
    }

    const buildChartPlayersFilterMetricsGroupByPlayer = (chart, xd, playersAvailable) => {

        let chartFilter = Object.keys(chart.filter)[0];
        let chartMetric = Object.keys(chart.filter[chartFilter].metrics).map( metric => {
            return fixMetricName(metric);
        });       

        const buildValueToCategory = (aS, values = {}) => {
            chartMetric.forEach( metric => {
                let value = aS[metric] * 1;
                if(values[metric]){
                    if(chartFilter === 'average' || chartFilter === 'total'){
                        value = value + (values[metric].value * 1);
                    }else if(chartFilter === 'minimun'){
                        if(value > values[metric].value){
                            value = values[metric].value;
                        }
                    }else{
                        if(value < values[metric].value){
                            value = values[metric].value;
                        }
                    }
                    values = {...values, [metric]: {count: values[metric].count + 1, value: value } };
                }else{
                    values = {...values, [metric]: {count: 1, value: (aS[metric] * 1) } };
                }
            });
            return values;
        }

        let categories = {};
        const ats = group ? athleteSessions.team_sessions : athleteSessions.tracks;
        ats.forEach( aS => {
            const player = players.filter( p => { return p.id === aS.player_id })[0];
            const isTrackFilterValue = isTrackFilter(player, playersAvailable, aS, ats);
            if(!isTrackFilterValue){
                if(categories[player.id]){
                    categories = {...categories, [player.id] : {name: player.short_name, values: buildValueToCategory(aS, categories[player.id].values) } };
                }else{
                    categories = {...categories, [player.id] : {name: player.short_name, values: buildValueToCategory(aS, ) } };
                }
            }
        });

        let yAxis = chartMetric.map( (metric, xd) => {
            return {
                labels: {
                    format: `{value} ${options[metric].unit}`,
                    style: {
                        color: `${options[metric].color}`
                    }
                },
                title: {
                    text: `${options[metric].label}`,
                    style: {
                        color: `${options[metric].color}`
                    }
                },
                opposite: xd%2==0 ? false : true,
                gridLineWidth: 0
            }
        });

        let series = chartMetric.map( (metric, xd) => {
            let data = [];
            Object.keys(categories).forEach( cat => {
                
                if(chartFilter === 'average'){
                    data = [...data, categories[cat].values[metric] ? ( (categories[cat].values[metric].value / categories[cat].values[metric].count).toFixed(2) * 1 ) : 0 ];
                }else{
                    data = [...data, categories[cat].values[metric] ? ( categories[cat].values[metric].value.toFixed(2) * 1 ) : 0 ];
                }
            });
            return  {
                        name: `${options[metric].label}`,
                        type: 'column',
                        yAxis: xd,
                        data: data,
                        tooltip: {
                            valueSuffix: ` ${options[metric].unit}`
                        }
                    }
        });

        categories = Object.keys(categories).map( key => {
            return categories[key].name;
        });
        //console.log('categories', categories)
        let config = {
            chart: {
                zoomType: 'xy',
                backgroundColor: 'transparent'
            },
            title: {
                text: `${chartFilter} of various metrics for players`,
                align: 'left'
            },
            subtitle: {
                //text: 'Source: WorldClimate.com',
                //align: 'left'
            },
            xAxis: [{
                /*
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                */
                categories: categories,
                crosshair: true
            }],
            yAxis: yAxis /*yAxisTest*/,
            tooltip: {
                //shared: true,
                backgroundColor: '#102E2F',
                style:{
                    fontSize:'15px'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 440,
                verticalAlign: 'top',
                y: -10,
                floating: true,
                backgroundColor: 'rgba(255,255,255,0.25)'
            },
            series: series,
            responsive: {
                rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                    floating: false,
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0
                    },
                    yAxis: [{
                    labels: {
                        align: 'right',
                        x: 0,
                        y: -6
                    },
                    showLastLabel: false
                    }, {
                    labels: {
                        align: 'left',
                        x: 0,
                        y: -6
                    },
                    showLastLabel: false
                    }, {
                    visible: false
                    }]
                }
                }]
            }
        }
        return <Highchart id={`mychart-${xd}`} type="chart" drilldown={true} config={config} />
    }

    const handleDefineChart = (chart, xd) => {

        let methodBuildChart = 'buildChart';
        const playersCount = players.length - playersUnselected.length;

        if( playersCount > 1){
            methodBuildChart = methodBuildChart + 'Players';
        }else{
            methodBuildChart = methodBuildChart + 'Player';
        }
        //console.log('buildChart', xd, chart);
        if( Object.keys(chart.filter).length > 1 ){
            methodBuildChart = methodBuildChart + `Filters`;
        }else{
            methodBuildChart = methodBuildChart + 'Filter';
        }
        // check players
        let countMetrics = {};

        // check filters select (count: average, maximun, minimun, total)
        if( Object.keys(chart.filter).length > 1){
        }

        // count metrics selected
        Object.entries(chart.filter).forEach( filter => {
            Object.entries(filter[1].metrics).forEach( metric => {
                if( !countMetrics[metric[0]] ){
                    countMetrics = {...countMetrics, [metric[0]]: metric[1] };
                }
            });
        });
        // setCaseGroupBy

        //console.log(chart.filter)
        //console.log('countMetrics', countMetrics);

        if( Object.keys(countMetrics).length > 1){
            methodBuildChart = methodBuildChart + `${Object.keys(countMetrics).length === 2 ? 'TwoMetricsGroupBy' : 'MetricsGroupBy'}`+chart.groupBy.charAt(0).toUpperCase()+chart.groupBy.slice(1);
        }else{
            methodBuildChart = methodBuildChart + 'MetricGroupBy'+chart.groupBy.charAt(0).toUpperCase()+chart.groupBy.slice(1);
        }
        //console.log('countMetrics', Object.keys(countMetrics).length);
        //console.log('method', methodBuildChart);

        const playersAvailable = handleFilterPlayersAvailable();

		switch(methodBuildChart) {
            case 'buildChartPlayersFiltersMetricGroupByPlayer':
                return buildChartPlayersFiltersMetricGroupByPlayer(chart, xd, playersAvailable);
            case 'buildChartPlayersFilterMetricsGroupByPlayer':
                return buildChartPlayersFilterMetricsGroupByPlayer(chart, xd, playersAvailable);
            case 'buildChartPlayersFiltersMetricsGroupByPlayer':
                return buildChartPlayersFiltersMetricsGroupByPlayer(chart, xd);
			case 'buildChartPlayersFilterMetricGroupByPlayer':
                return buildChartPlayersFilterMetricGroupByPlayer(chart, xd, playersAvailable);
			case 'buildChartPlayersFilterTwoMetricsGroupByPlayer':
				return buildChartPlayersFilterTwoMetricsGroupByPlayer(chart, xd, playersAvailable);
			case 'buildChartPlayersFiltersTwoMetricsGroupByPlayer':
				return buildChartPlayersFilterTwoMetricsGroupByPlayer(chart, xd, playersAvailable);
			default:
                return <></>
		}
    }

    const mapCharts = () => {
        return charts.map( (ch, xd) => {
            if(ch.groupBy !== 'date'){
                return  <Grid key={`chart-${xd}`} item xs={12} className="chart-grid-container">
                            <div className="gps-graphic">{handleDefineChart(ch, xd)}</div>
                        </Grid>
            }else{
                return  <Grid key={`chart-${xd}`} item xs={12} className="chart-grid-container">
                            <div className="gps-graphic">GPS {xd +1}</div>
                        </Grid>
            }
        });
    };
    return (<>{mapCharts()}</>);
};
export default Charts;