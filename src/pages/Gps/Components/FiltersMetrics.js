import React from 'react';

import translate from "../../../lang/lang";
import Modal from '@material-ui/core/Modal';
import Tooltip from '@material-ui/core/Tooltip';
import useDebounce from '../../../hooks/useDebounce';
import { makeStyles } from "@material-ui/core/styles";
import AddCircleIcon from '@material-ui/icons/AddCircle';

const useStyles = makeStyles( () => ({

    filtersContainer: {
        borderRadius: '2px',
        borderTop: '1px solid #0a1a24',
        borderBottom: '1px solid #ffffff',
        '& .metrics-set-filters-container': {
            padding: '1px 0 0',
            background: '#1e4249a8',
            borderBottom: '1px solid',
            '& .metrics-set-filters-button-container': {
                margin: '5px 10px',
                padding: '1px',
                display: 'flex',
                cursor: 'pointer',
                borderRadius: '5px',
                border: '1px solid',
                alignItems: 'center',
                background: '#286b7b',
                justifyContent: 'space-around',
                '&:hover': {
                    opacity: .7
                }
            },
            '& .metrics-set-filters-list-container': {
                maxHeight: '60px',
                minHeight: '60px',
                overflowY: 'auto',
                '& .filter-item': {
                    display: 'flex',
                    alignItems: 'center',
                    padding: '2px 0 0 1px',
                    '&:hover': {
                        background: '#ffffff30'
                    },
                    '& .selected': {
                        content: '""',
                        width: '8px',
                        height: '8px',
                        borderRadius: '50%',
                        margin: '0 3px 3px 2px',
                        display: 'inline-block',
                        backgroundColor: '#69b6d5',
                    },
                    '& .filter-item-desc': {
                        margin: '0 5px',
                        cursor: 'pointer',
                        overflow: 'hidden',
                        whiteSpace: 'nowrap',
                        textOverflow: 'ellipsis',
                    }

                },
                '& .filter-item-selected': {
                    margin: '0 2px',
                    background: '#0D2530'
                }
            }
        },
        '& .metrics-filter-container': {
            overflow: 'auto',
            maxHeight: 'calc(100vh - 160px)',
        },
        '& .title': {
            display: 'flex',
            fontSize: '1rem',
            background: '#0A1A24',
            padding: '4px 7.5px 0',
            justifyContent: 'center'
        },
        '& .metric-item-container': {
            cursor: 'pointer',
            '& .metric-item-title': {
                display: 'flex',
                fontSize: '1.2rem',
                padding: '2px 0 0',
                textAlign: 'center',
                justifyContent: 'center',
            },
            '& .metric-item-body': {
                padding: '2px',
                lineHeight: '1.2',
                borderTop: '0 !important',
                '& .metric-item-body-filter': {
                    display: 'flex',
                    alignItems: 'center',
                    padding: '2px 0 0 1px',
                    '&:hover': {
                        background: '#ffffff30 !important'
                    },
                    '& .metric-item-body-filter-label': {
                        margin: '0 5px'
                    }
                },
                '& .selected': {
                    content: '""',
                    width: '8px',
                    height: '8px',
                    borderRadius: '50%',
                    margin: '0 3px 3px 2px',
                    display: 'inline-block',
                    backgroundColor: '#69b6d5',
                }
            }
        }
    },
    PaperModal: {
        top: '50%',
        left: '50%',
        padding: '15px',
        display: 'flex',
        borderRadius: '5px',
        position: 'absolute',
        alignItems: 'center',
        background: '#061618',
        flexDirection: 'column',
        fontFamily: 'Bebas-Bold',
        border: '1px solid #757373',
        transform: 'translate(-50%, -50%)',
        '& label': {
            color: '#fff',
            margin: '10px',
            fontSize: '1.3rem',
            textTransform: 'uppercase'
        },
        '& input': {
            width: '100%',
            borderRadius: '5px',
            padding: '5px'
        },
        '& .metrics-set-filters-button-save-container': {
            color: '#fff',
            margin: '15px',
            display: 'flex',
            cursor: 'pointer',
            borderRadius: '5px',
            border: '1px solid',
            padding: '2px 10px',
            alignItems: 'center',
            background: '#122e36',
            justifyContent: 'space-around',
            '&:hover': {
                opacity: .7
            },
            '& span': {
                margin: '4px 5px 0'
            }
        },
        '& .filters-description-container': {
            '& .description-title': {
                padding: '10px 0'
            },
            color: '#fff'
        }
    },
    customWidth: {
        maxWidth: 200,
    },
}));

const FiltersMetrics = (props) => {

    let myDebounce = null;
    const classes = useStyles();
    const setDebounce = useDebounce();
    const inputTextRef = React.useRef(null);
    const divFilterRef = React.useRef(null);
    const { metricsData, setMetricsData, setSaveFilter, filterList, filterSelected, handleSelectFilter, setFilterSelected } = props;
    const [open, setOpen] = React.useState(false);
    const [descriptionFilterText, setDescriptionFilterText] = React.useState('');

    const handleNewDescription = (e) => {
        if(myDebounce){
            clearTimeout(myDebounce);
        }else{
            myDebounce = setDebounce(setDescriptionFilterText, 500, e.target.value);
        }
    }

    const handleSaveFilter = () => {
        inputTextRef.current.value = '';
        setOpen(false);
        setDescriptionFilterText('');
        setSaveFilter(descriptionFilterText);
        setTimeout( () => divFilterRef.current.scrollTop = divFilterRef.current.scrollHeight, 200 );
    }

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSetMetrcisData = (m, op, val) => {
        const options = {...metricsData[m].options, [op]: val};
        const metric = {...metricsData[m], options: options};
        setFilterSelected(null);
        setMetricsData({...metricsData, [m]:metric});
    }

    const buildMetrics = () => {
        return Object.keys(metricsData).map( (m, xd) => {
            return  <div key={`metric-${xd}`} className="metric-item-container">
                        <div className="metric-item-title" style={{backgroundColor: metricsData[m].color}}><span>{metricsData[m].name}</span></div>
                        <div className="metric-item-body" style={{border: `2.5px solid ${metricsData[m].color}`}}>
                            { Object.keys(metricsData[m].options).map( (op, xdp) => {
                                return  <div 
                                            onClick={ () => handleSetMetrcisData(m, op, !metricsData[m].options[op] ) }
                                            className={`metric-item-body-filter`}
                                            key={`metric-option-${metricsData[m].name}-${xdp}`}
                                            style={{background: metricsData[m].options[op] === true ? '#0a1a24d9' : 'none' }}
                                        >
                                            <div className={`${ metricsData[m].options[op] === true ? 'selected' : 'unselected'}`}></div>
                                            <div className={`metric-item-body-filter-label`}>{op}</div>
                                        </div>
                            })}
                        </div>
                    </div>
        });
    }

    const checkMetricsSelected = () => {
        const countSelected = Object.keys(metricsData).filter( key => {
            const optionsSelected = Object.keys(metricsData[key].options).filter( keyOp => {
                return metricsData[key].options[keyOp];
            });
            return optionsSelected.length > 0;
        });
        return countSelected.length > 0;
    }

    return (
        <div className={classes.filtersContainer}>
            <div className="title"><span>{translate('set filter')}</span></div>
            <div className="metrics-set-filters-container">
                <div className="metrics-set-filters-button-container" onClick={handleOpen}><span>{translate('add filters')}</span><AddCircleIcon /></div>
                <div ref={divFilterRef} className="metrics-set-filters-list-container">

                    { filterList.length === 0 ?
                        <div className="filter-item" style={{marginLeft: '10px'}}>{translate('no filters saved...')}</div>
                    :
                        filterList.map( (filter, xd) => {
                            return  <Tooltip key={`filter-${xd}`} title={filter.desc} classes={{ tooltip: classes.customWidth }} placement="left">
                                        <div className={`filter-item${ filterSelected === xd ? ' filter-item-selected' : ''}`} onClick={ () => handleSelectFilter(xd) } >
                                            <div className={`${ filterSelected === xd ? 'selected' : 'unselected'}`}></div>
                                            <div className="filter-item-desc">{filter.desc}</div>
                                        </div>
                                    </Tooltip>

                        })
                    }

                </div>
            </div>
            <div className="metrics-filter-container">
                {buildMetrics()}
            </div>

            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                <div className={classes.PaperModal}>

                    { checkMetricsSelected() ? <>
                        <label htmlFor="filter-name">{translate('set description to new filter')}</label>
                        <input ref={inputTextRef} type="text" id="filter-name" name="filter-name" onChange={ (e) => handleNewDescription(e) } />
                        { descriptionFilterText !== '' &&
                        <div className="metrics-set-filters-button-save-container" onClick={ () => handleSaveFilter() }><span>{translate('save')}</span><AddCircleIcon /></div>
                        }

                        <div className="filters-description-container">
                            <div className="description-title">
                                <span>The filters that will be saved are:</span>
                            </div>
                            <lu>
                                <li>metrics by calculation ( average, maximum, minimum and / or total )</li>
                                <li>groups ( tracks or sessions )</li>
                                <li>group by (player, track or date)</li>
                                <li>positions on soccer field</li>
                            </lu>
                        </div></>
                    :
                        <label htmlFor="filter-name">{translate('you must select at least one metric')}</label>
                    }
                </div>
            </Modal>
        </div>
    );
};
export default FiltersMetrics;