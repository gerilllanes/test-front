import { useState, useEffect } from "react";
import axios from "axios";
// import useFetch from "../../hooks/useFetch";
import { makeStyles } from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles(() => ({
  formControl: {
    color: "white",
    fontFamily: "Bebas-Bold",
    letterSpacing: 1,
  },
  option: {
    background: "red",
  },
}));

const SwitchLang = () => {
  const classes = useStyles();
  // const [language, fetchLanguage] = useFetch();
  const [lang, setLang] = useState()
  const [error, setError] = useState(false);

  useEffect(() => {
    // fetchLanguage(`/api/user/switcher/language/us`, null, 'PUT', null, true)

    const BASE_API = process.env.REACT_APP_API_URL;
    const fetchLang = async () => {
      try {
        const config = {
          headers: {
            Authorization: `Bearer ${JSON.parse(
              localStorage.getItem("apiToken")
            )}`,
          },
        };
        const result = await axios.put(
          `${BASE_API}/api/user/switcher/language/us`,
          config
        );
        setLang(result.data.data);
      } catch (error) {
        setError(error);
      }
    };
    fetchLang();
  }, []);

  const handleChange = (event) => {
    setLang(event.target.value);
  };

  const changeLang = (lang) => {
    setLang(lang);
  };

  return (
    <>
      <FormControl>
        <Select native className={classes.formControl} onChange={handleChange}>
          <option aria-label="None"
            value={lang}
            className={classes.option}
            onClick={() => changeLang(lang.translate)} />
          <option>English</option>
          <option>Italian</option>
          <option>Spanish</option>
        </Select>
      </FormControl>
    </>
  );
};

export default SwitchLang;
