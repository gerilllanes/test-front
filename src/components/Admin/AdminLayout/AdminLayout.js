import { useEffect, useState } from "react";
import AppBar from "@material-ui/core/AppBar";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Toolbar from "@material-ui/core/Toolbar";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { Link, useHistory } from "react-router-dom";
import translate from "../../../lang/lang";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    fontFamily: "Bebas-Bold",
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up("sm")]: {
      width: `calc(98% - ${drawerWidth}px)`,
      backgroundColor: "rgba(0, 0, 0, 0.5)",
      margin: 10,
      marginLeft: 50,
      boxShadow: "none",
      position: "fixed",
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    border: "none",
    margin: 10,
    height: "95vh",
  },
  list: {
    backgroundColor: "#2b565f",
    margin: 5,
    width: "95%",
    padding: 2,
    color: "white",
    "&:hover": {
      backgroundColor: "none",
      color: "#9bf5fb",
    },
  },
  item: {
    marginRight: 20,
    marginLeft: 5,
    fontSize: 20,
  },
  content: {
    marginTop: "4rem",
    width: '83%'
  }
}));

const drawerWidth = 220;

const AdminLayout = (props) => {
  const history = useHistory();
  const { window, children } = props;

  const handleClick = () => {
    history.push("/dashboard");
  };

  const classes = useStyles();
  const theme = useTheme();

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <List>
        {[
          {
            list: translate("Users Management"),
            id: 0,
            page: "/admin/users",
          },
          {
            list: translate("Evaluations Management"),
            id: 1,
            page: "/admin/evaluations",
          },
          {
            list: translate("Categories Management"),
            id: 2,
            page: "/admin/categories",
          },
          {
            list: translate("Clubs Management"),
            id: 3,
            page: "/admin/club",
          },
          {
            list: translate("Teams Management"),
            id: 4,
            page: "/admin/team",
          },
        ].map((text) => (
          <Link
            to={text.page}
            style={{ textDecoration: "none", color: "white" }}
            key={text.id}
          >
            <ListItem button key={text.list} className={classes.list}>
              <ListItemText
                primary={text.list}
                disableTypography={true}
                className={classes.item}
              />
            </ListItem>
          </Link>
        ))}
      </List>
      <div className={classes.toolbar} />
      <List>
        <ListItem button className={classes.list}>
          <ListItemText
            primary="Back"
            disableTypography={true}
            className={classes.item}
            onClick={handleClick}
          />
        </ListItem>
      </List>
    </div>
  );

  useEffect(() => {
    document.body.classList.add("background-main");
    return () => {
      document.body.classList.remove("background-main");
    };
  });

  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <>
      <div className={classes.root}>
        <AppBar className={classes.appBar}>
          <Toolbar variant="dense">
            <h1>{translate("Admin Panel")}</h1>
          </Toolbar>
        </AppBar>
        <nav className={classes.drawer}>
          <Hidden smUp implementation="css">
            <Drawer
              container={container}
              variant="temporary"
              anchor={theme.direction === "rtl" ? "right" : "left"}
              open={mobileOpen}
              onClose={handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper,
              }}
              ModalProps={{
                keepMounted: true,
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper,
              }}
              variant="permanent"
              open
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>
        <main className={classes.content}>{children}</main>
      </div>
    </>
  );
};

export default AdminLayout;
