import React, {useEffect, useState} from 'react';

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles( (props) => ({

    fieldContainer: {
        width: (props) => props.width,
        height: (props) => props.height,
        border: (props) => props.border,
        fontSize: (props) => props.fontSize,
        position: (props) => props.position,
        background: (props) => props.background,
        '& .player-pos': {
            position: 'absolute',
            border: '1px solid #9CF4F5',
        },
        '& .player-pos.selected': {
            background: '#1f4a55'
        },
        '& .pos-one': {
            zIndex: 1,
            left: '50%',
            width: '40%',
            height: '12%',
            background: '#4B737D',
            transform: 'translateX(-50%)'
        },
        '& .pos-two': {
            top: 0,
            left: '50%',
            width: '70%',
            height: '35%',
            transform: 'translateX(-50%)',
            '& span': {
                margin: '15px 0 0'
            }
        },
        '& .pos-three': {
            top: 0,
            left: '100%',
            width: '15%',
            height: '35%',
            transform: 'translateX(-100%)'
        },
        '& .pos-four': {
            top: 0,
            left: '0',
            width: '15%',
            height: '35%',
        },
        '& .pos-five': {
            top: '50%',
            left: '50%',
            width: '70%',
            height: '30%',
            flexDirection: 'column',
            transform: 'translate(-50%, -50%)'
        },
        '& .pos-six': {
            top: '56%',
            left: '100%',
            width: '15%',
            height: '42%',
            transform: 'translate(-100%, -50%)'
        },
        '& .pos-seven': {
            left: 0,
            top: '88%',
            width: '15%',
            height: '24%',
            transform: 'translate(0, -50%)'
        },
        '& .pos-eight': {
            left: 0,
            top: '56%',
            width: '15%',
            height: '42%',
            transform: 'translate(0, -50%)'
        },
        '& .pos-nine': {
            left: '50%',
            top: '100%',
            width: '70%',
            height: '24%',
            flexDirection: 'column',
            transform: 'translate(-50%, -100%)'
        },
        '& .pos-team': {
            top: '76%',
            left: '50%',
            width: '70%',
            height: '12%',
            transform: 'translate(-50%, -100%)'
        },
        '& .pos-eleven': {
            top: '100%',
            left: '100%',
            width: '15%',
            height: '24%',
            transform: 'translate(-100%, -100%)'
        },
        '& .hz': {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        '& .vt': {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            '& .vt-item': {
                display: 'flex',
                transform: 'rotate(90deg)',
                '& span': {
                    margin: '0 2px'
                }
            }
        }
    }

}));

const FooballFieldPositions = (props) => {

    const {
        width = '108px',
        height = '176px',
        fontSize = '.65rem',
        position = 'relative',
        background = '#4B737D',
        border = '2px solid #fff',
        value = null,
        onChange
    } = props;

    const classes = useStyles({width, height, border, fontSize, position, background});

    const [state, setState] = useState([]);

    const handleSelect = (pos) => {

        let isIncluede = state.includes(pos);

        if(isIncluede){
            isIncluede = state.filter( st => st !== pos);
        }else{
            isIncluede = [...state, pos];
        }
        setState(isIncluede);
        if(onChange){
            onChange(isIncluede);
        }
    }

    return (
        <div className={classes.fieldContainer}>
            <div onClick={ () => handleSelect(1) } className={`player-pos pos-one hz${ value ? value.includes(1) ? ' selected' : '' : state.includes(1) ? ' selected' : ''}`}><span>PO</span></div>
            <div onClick={ () => handleSelect(2) } className={`player-pos pos-two hz${ value ? value.includes(2) ? ' selected' : '' : state.includes(2) ? ' selected' : ''}`}><span>DFC / LB</span></div>
            <div onClick={ () => handleSelect(3) } className={`player-pos pos-three vt${ value ? value.includes(3) ? ' selected' : '' : state.includes(3) ? ' selected' : ''}`}><div className="vt-item"><span>LI</span><span>/</span><span>DFI</span></div></div>
            <div onClick={ () => handleSelect(4) } className={`player-pos pos-four vt${ value ? value.includes(4) ? ' selected' : '' : state.includes(4) ? ' selected' : ''}`}><div className="vt-item"><span>LD</span><span>/</span><span>DFD</span></div></div>
            <div onClick={ () => handleSelect(5) } className={`player-pos pos-five hz${ value ? value.includes(5) ? ' selected' : '' : state.includes(5) ? ' selected' : ''}`}><div>MC</div><div>MCI/MCD</div><div>MDI/MDD</div></div>
            <div onClick={ () => handleSelect(6) } className={`player-pos pos-six vt${ value ? value.includes(6) ? ' selected' : '' : state.includes(6) ? ' selected' : ''}`}><div className="vt-item"><span>MI</span><span>/</span><span>MVI</span></div></div>
            <div onClick={ () => handleSelect(7) } className={`player-pos pos-seven vt${ value ? value.includes(7) ? ' selected' : '' : state.includes(7) ? ' selected' : ''}`}><div className="vt-item"><span>ED</span><span>/</span><span>SDD</span></div></div>
            <div onClick={ () => handleSelect(8) } className={`player-pos pos-eight vt${ value ? value.includes(8) ? ' selected' : '' : state.includes(8) ? ' selected' : ''}`}><div className="vt-item"><span>MD</span><span>/</span><span>MVD</span></div></div>
            <div onClick={ () => handleSelect(9) } className={`player-pos pos-nine hz${ value ? value.includes(9) ? ' selected' : '' : state.includes(9) ? ' selected' : ''}`}><div>DC</div><div>DI/DD</div></div>
            <div onClick={ () => handleSelect(10) } className={`player-pos pos-team hz${ value ? value.includes(10) ? ' selected' : '' : state.includes(10) ? ' selected' : ''}`}><span>MP / MCO</span></div>
            <div onClick={ () => handleSelect(11) } className={`player-pos pos-eleven vt${ value ? value.includes(11) ? ' selected' : '' : state.includes(11) ? ' selected' : ''}`}><div className="vt-item"><span>EI</span><span>/</span><span>SDI</span></div></div>
        </div>
    );
};

export default FooballFieldPositions;