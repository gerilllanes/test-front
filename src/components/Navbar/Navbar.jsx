//import { useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { Button, Hidden } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  root: {
    "& > *": {
      fontFamily: "Bebas-Bold",
    },
  },
  btn: {
    backgroundColor: "#1E4249",
    color: "#E1F5E9",
    borderRadius: 0,
    "&:hover": {
      backgroundColor: "#0A1A24",
      color: "#B1EFF4",
    },
    fontFamily: "Bebas-Bold !important",
    fontSize: "1.5rem !important",
  },
  active: {
    backgroundColor: "#0A1A24",
    color: "#B1EFF4",
    fontFamily: "Bebas-Bold",
    fontSize: "24px",
  },

  link: {
    textDecoration: "none",
  },
  margin: {
      marginBottom: '5px'
  }
}));

const Navbar = ({ id, page }) => {
//   const context = useContext(StoreContext);

  const classes = useStyles();

//   const [lang] = context.lang;
//   const [view, setView] = context.view;


  const items = [
    {
      page: 0,
      label: "Summary",
      url: `/player/0/${id}`,
    },
    {
      page: 1,
      label: "Valoration",
      url: `/player/1/${id}`,
    },
    {
      page: 2,
      label: "GPS",
      url: `/player/2/${id}`,
    },
    {
      page: 3,
      label: "Video Analysis",
      url: `/player/3/${id}`,
    },
    {
      page: 4,
      label: "Files",
      url: `/player/4/${id}`,
    },
  ];

  return (
    <>
      <div className={classes.margin}>
        <div className={classes.root}>
          {/* <Hidden only={["md", "lg", "xl"]}>
            <DrawerList id={id} items={items}  />
          </Hidden> */}
          <Hidden only={["xs", "sm"]}>
            {items.map((item) => {
              return (
                <Link to={item.url} className={classes.link} key={item.page}>
                  <Button
                    key={item.page}
                    className={
                      true === item.page ? classes.active : classes.btn
                    }
                    variant="contained"
                  >
                    {item.label}
                  </Button>
                </Link>
              );
            })}
          </Hidden>
        </div>
      </div>
    </>
  );
};

export default Navbar;

