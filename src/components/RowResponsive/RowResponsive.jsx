import React, { useMemo, useCallback } from "react";
import useWindowSize from "../../hooks/useWindowSize";

const style = {
  container: {
    display: "flex",
  },
};

/* xs Extra small devices (phones, 480px and down) */
//@media only screen and (max-width: 480px) {...}

/* sm Small devices (portrait tablets and large phones, 480px and up) */
//@media only screen and (min-width: 480px) {...}

/* md Medium devices (landscape tablets, 768px and up) */
//@media only screen and (min-width: 768px) {...}

/* lg Large devices (laptops/desktops, 992px and up) */
//@media only screen and (min-width: 992px) {...}

/* xl Extra large devices (large laptops and desktops, 1200px and up) */
//@media only screen and (min-width: 1200px) {...}

const RowResponsive = (props) => {
  // e.g: md = {cols: 4, margin: '20px'}
  const { items, className } = props;

  const [width, height, size] = useWindowSize();

  const getCols = useCallback(
    (row, indexRow, margin, top, bottom, colsWidth, cols) => {
      let i = row.length;

      while (i < cols) {
        row.push(
          <div
            style={{ ...row[0].props.style, opacity: 0 }}
            className={row[0].props.style}
            key={`${indexRow}${i}`}
          ></div>
        );
        i++;
      }

      return row.map((col, index) => {
        const newStyle = {
          margin:
            cols < 2
              ? `${margin}px`
              : index === 0
              ? `${top}px ${
                  size === "xs" && cols === 1 ? margin : margin / 2
                }px ${bottom}px ${margin}px`
              : index === row.length - 1
              ? `${top}px ${margin}px ${bottom}px ${margin / 2}px`
              : `${top}px ${margin / 2}px ${bottom}px`,
        };
        return React.cloneElement(col, {
          style: { ...col.props.style, ...newStyle, width: colsWidth, maxWidth: `calc(${colsWidth} + -15px)` },
        });
      });
    },
    [size]
  );

  const getRows = useMemo(() => {
    if (size) {
      let rows = [];
      let countCol = 0;
      let countItem = 0;
      let colsCollections = [];
      let cols =
        props[size] && props[size].cols
          ? props[size].cols
          : size === "xs"
          ? 1
          : size === "sm"
          ? 2
          : size === "md"
          ? 3
          : size === "lg"
          ? 4
          : 5;
      let margin = props[size] && props[size].margin ? props[size].margin : 15;
      let colsWidth = `${100 / cols}%`;

      while (countItem < items.length) {
        colsCollections.push(items[countItem]);
        countCol++;
        if (countCol === cols || countItem === items.length - 1) {
          rows.push(colsCollections);
          colsCollections = [];
          countCol = 0;
        }
        countItem++;
      }
      return rows.map((row, index) => {
        return (
          <div
            key={index}
            className={`row-container${className ? " " + className : ""}`}
            style={style.container}
          >
            {getCols(
              row,
              index,
              margin,
              index === 0 ? margin : margin / 2, //Top
              index === rows.length - 1 ? margin : margin / 2, //Bottom
              colsWidth,
              cols
            )}
          </div>
        );
      });
    }
  }, [props, items, size, getCols, className]);
  return (
    <>
      {
        <span style={{ display: "none" }}>
          Window size: {width} x {height} {size}
        </span>
      }
      {getRows}
    </>
  );
};
export default React.memo(RowResponsive);
