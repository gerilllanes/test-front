import { makeStyles, Typography } from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Box from "@material-ui/core/Box";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles(() => ({
  box: {
    borderRadius: 0,
    background: "rgba(30,30,30,0.6)",
    textAlign: "center",
    margin: "10px 6px",
    width: "120px"
  },
  title: {
    color: "#F8F8F8",
    padding: 0,
    fontFamily: "Bebas-Bold",
    fontSize: 20,
    letterSpacing: 1,
    backgroundColor: 'rgba(18, 18, 18, 0.5)',
    height: "2rem"
  },
  img: {
    width: "100%",
    height: "3.6rem",
  },
  textPrimary: {
    color: "#F8F8F8",
    fontFamily: "Bebas-Bold",
    fontSize: 15,
    letterSpacing: 1,
  },
  textSecondary: {
    color: "#919192",
    fontFamily: "Bebas-Bold",
    fontSize: 14,
    letterSpacing: 0.5,
  },
  content: {
    padding: 3,
    height: "3.2rem",
  },
}));

const CardBox = ({ title, img, alt, textPrimary, textSecondary }) => {
  const classes = useStyles();

  return (
    <>
      <Card className={classes.box} elevation="0">
        <CardHeader
          className={classes.title}
          title={<Typography className={classes.title}>{title}</Typography>}
        ></CardHeader>
        <Avatar variant="square" alt={alt} src={img} className={classes.img} />
        <Box className={classes.content}>
          <Typography className={classes.textPrimary}>{textPrimary}</Typography>
          <Typography className={classes.textSecondary}>
            {textSecondary}
          </Typography>
        </Box>
      </Card>
    </>
  );
};

export default CardBox;
