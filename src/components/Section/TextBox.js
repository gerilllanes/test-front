import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(() => ({
  box: {
    background:
      "linear-gradient(180deg, rgba(30,30,30,0.6) 0%, rgba(18,18,18,0.6) 100%);",
    padding: 2,
    paddingLeft: 6,
    margin: "10px 6px",
    display: "flex",
    height: "2.5rem"
  },
  text: {
    fontFamily: "Bebas-Bold",
    fontSize: 20,
    color: "#919192",
    letterSpacing: 1, 
    marginRight: 2
  },
  result: {
    fontFamily: "Bebas-Bold",
    fontSize: 20,
    color: "#F8F8F8",
    letterSpacing: 1,
  },
}));

const TextBox = ({ text, result }) => {
  const classes = useStyles();

  return (
    <>
      <Box className={classes.box}>
        <Typography className={classes.text}>{text}</Typography>
        <Typography className={classes.result}>{result}</Typography>
      </Box>
    </>
  );
};

export default TextBox;
