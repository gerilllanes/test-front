import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";

const useStyles = makeStyles(() => ({
  avatar: {
    height: "8.8rem",
    width: "120px",
    border: "3px solid rgba(0, 0, 0, 0.8)",
    margin: "10px 6px",
  },
}));

const AvatarProfile = ({ alt, img }) => {
  const classes = useStyles();

  return (
    <>
      <Avatar
        variant="square"
        alt={alt}
        src={img}
        className={classes.avatar}
        boxShadow={3}
      />
    </>
  );
};

export default AvatarProfile;
