import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import EditIcon from "@material-ui/icons/Edit";

const useStyles = makeStyles(() => ({
  edit: {
    backgroundColor: "rgba(65, 65, 65, 0.6)",
    borderRadius: 0,
    padding: 0,
    margin: 3,
  },
  icon: {
    color: "#F8F8F8",
  },
}));

const Edit = () => {
  const classes = useStyles();
  return <>
  <Button className={classes.edit}>
      <EditIcon  className={classes.icon} />
  </Button>
  </>;
};

export default Edit;
