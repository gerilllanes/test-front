import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Edit from "./Edit";

const useStyles = makeStyles(() => ({
  section: {
    borderRadius: 0,
    backgroundColor: "rgba(33, 33, 33, 0.5)",
    marginBottom: '1.5rem',
  },
  text: {
    fontFamily: "Bebas-Bold",
    fontSize: 24,
    flexGrow: 1,
    letterSpacing: 1
  },
  header: {
    background: "linear-gradient(#1E1E1E, #121212)",
    padding: 12,
    paddingLeft: '1.8rem',
    paddingRight: '1.5rem',
    display: "flex",
  },
  body: {
    padding: '1.5rem',
    overflowY: "hidden"
  },
}));

const Section = ({ title, width, children }) => {
  const classes = useStyles();

  return (
    <>
      <Box className={classes.section} width={width}>
        <Box className={classes.header}>
          <Typography className={classes.text}>{title}</Typography>
          <Edit />
        </Box>
        <Box className={classes.body}>
              {children}
        </Box>
      </Box>
    </>
  );
};

export default Section;
