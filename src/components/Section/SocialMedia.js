import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import InstagramIcon from "@material-ui/icons/Instagram";
import FacebookIcon from "@material-ui/icons/Facebook";
import YouTubeIcon from "@material-ui/icons/YouTube";
import TwitterIcon from "@material-ui/icons/Twitter";

const useStyles = makeStyles(() => ({
  box: {
    borderRadius: 0,
    background: "rgba(33,33,33,0.5)",
    textAlign: "center",
    margin: "10px 6px",
    width: "120px",
    height: "8.5rem",
  },
  title: {
    color: "#F8F8F8",
    padding: 0,
    fontFamily: "Bebas-Bold",
    fontSize: 20,
    letterSpacing: 1,
    backgroundColor: 'rgba(18, 18, 18, 0.5)'
  },
  img: {
    width: "100%",
    height: "4rem",
  },
  textPrimary: {
    color: "#F8F8F8",
    fontFamily: "Bebas-Bold",
    fontSize: 14,
    letterSpacing: 1,
  },
  textSecondary: {
    color: "#919192",
    fontFamily: "Bebas-Bold",
    fontSize: 10,
    letterSpacing: 0.5,
  },
  content: {
    padding: 3,
    height: "auto",
  },
  icon: {
      color: '#fff'
  }
}));

const SocialMedia = () => {
  const classes = useStyles();
  return (
    <Card className={classes.box} elevation="0">
      <CardHeader
        className={classes.title}
        title={<Typography className={classes.title}>Redes</Typography>}
      ></CardHeader>
      <Box className={classes.content}>
        <Grid container>
          <Grid item xs={6}>
            <IconButton
              aria-label="instagram"
              size="medium"
              className={classes.icon}
            >
              <InstagramIcon />
            </IconButton>
            <IconButton aria-label="delete" size="medium" className={classes.icon}>
              <YouTubeIcon />
            </IconButton>
          </Grid>
          <Grid item xs={6}>
            <IconButton
              aria-label="instagram"
              size="medium"
              className={classes.icon}
            >
              <FacebookIcon />
            </IconButton>
            <IconButton aria-label="delete" size="medium" className={classes.icon}>
              <TwitterIcon />
            </IconButton>
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
};

export default SocialMedia;
