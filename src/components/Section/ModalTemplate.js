import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { Button } from "@material-ui/core";
import background from "../../assets/img/backgroundImage.png";

const useStyles = makeStyles(() => ({
  section: {
    borderRadius: 0,
    backgroundImage: `url(${background})`,
    marginBottom: "1.5rem",
    width: "600px"
  },
  text: {
    color: "#ffff",
    fontFamily: "Bebas-Bold",
    fontSize: 24,
    flexGrow: 1,
    letterSpacing: 1,
  },
  header: {
    background: "linear-gradient(#1E1E1E, #121212)",
    padding: 12,
    paddingLeft: "1.8rem",
    paddingRight: "1.5rem",
    display: "flex",
  },
  body: {
    padding: "1.5rem",
  },
  btnCancelar: {
    fontFamily: "Bebas-Bold",
    border: "1px solid #5EE8FF",
    color: "#5EE8FF",
    width: "140px",
    height: "45px",
    fontSize: "20px",
  },
  btnGuardar: {
    fontFamily: "Bebas-Bold",
    backgroundColor: "#5EE8FF",
    width: "140px",
    height: "45px",
    fontSize: "20px",
    "&:hover": {
      backgroundColor: "#5EE8FF",
    },
  },
}));

const Section = ({ title, width, children }) => {
  const classes = useStyles();

  return (
    <>
      <Box className={classes.section} width={width}>
        <Box className={classes.header}>
          <Typography className={classes.text}>{title}</Typography>
        </Box>
        <Box className={classes.body}>{children}</Box>
        <Box className={classes.header}>
          <Button className={classes.btnCancelar}>Cancelar</Button>
          <Button className={classes.btnGuardar}>Guardar</Button>
        </Box>
      </Box>
    </>
  );
};

export default Section;
