import { makeStyles } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(() => ({
  box: {
    border: "3px solid white",
    padding: "0.4rem",
    display: "flex",
  },
  textBox: {
    display: "flex",
  },
  text: {
    fontFamily: "Bebas-Bold",
    fontSize: 24,
    color: "rgba(255, 255, 255, 0.6)",
  },
  result: {
    fontFamily: "Bebas-Bold",
    fontSize: 24,
    color: "#F8F8F8",
  },
}));

const UserBox = ({ mail, mailResult, pass, passResult }) => {
  const classes = useStyles();
  return (
    <>
      <Box className={classes.box}>
        <Box className={classes.textBox}>
          <Typography className={classes.text}>{mail}</Typography>
          <Typography className={classes.result}>{mailResult}</Typography>
        </Box>
        <Box className={classes.textBox}>
          <Typography className={classes.text}>{pass}</Typography>
          <Typography className={classes.result}>{passResult}</Typography>
        </Box>
      </Box>
    </>
  );
};

export default UserBox;
