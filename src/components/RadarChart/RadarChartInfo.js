import {
  Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
} from "recharts";

const RadarChartInfo = ({ dataRadar }) => {
  return (
      <RadarChart
        cx="50%"
        cy="50%"
        outerRadius="70%"
        data={dataRadar}
        width={300}
        height={300}
      >
        <PolarGrid />
        <PolarAngleAxis
          axisLine={true}
          tick={{ fill: "white", backgroundColor: "black" }}
          domain={[1, 5]}
          dataKey="subject"
        />
        <PolarRadiusAxis
          tick={false}
          tickCount={5}
          axisLine={false}
          angle={30}
          domain={[1, 5]}
          dataKey="subject"
        />
        <Radar dataKey="A" stroke="#00F0E5" fill="#0EB22F" fillOpacity={0.6} />
      </RadarChart>
  );
};

export default RadarChartInfo;
