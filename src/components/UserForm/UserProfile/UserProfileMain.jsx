import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import DemoData from "./Components/ProfileCards/DemoData";

const useStyles = makeStyles((theme) => ({
  multilineColor: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginRight: 30,
    marginTop: 20,
    marginBottom: 30,
  },
}));
const ProfileMain = () => {
  const classes = useStyles();

  useEffect(() => {
    document.body.classList.add("background-main");
    return () => {
      document.body.classList.remove("background-main");
    };
  });

  return (
    <div className={classes.multilineColor}>
      <DemoData />
    </div>
  );
};

export default ProfileMain;
