import React from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles({
  cssOutlinedInput: {
    border: "2px solid",
    color: "white",
    borderRadius: 3,
  },
  notchedOutline: {
    borderWidth: "2px",
    borderColor: "white !important",
    margin: "5px",
    width: "20rem",
  },
  inputs: {
    borderColor: "cyan",
    color: "white",
    marginBottom: "3px",
    fontSize: "20px",
  },
});

const DemographicDataForm = ({handleChangeUser}) => {
  const classes = useStyles();

  return (
    <form
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "20 rem",
          marginLeft: "8rem",

          marginTop: 10,
          padding: 10,
        }}
      >
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Nombre"
          name='name'
          placeholder="Nombre *"
          variant="outlined"
          onChange={(e) => handleChangeUser(e)}
        />

        <TextField
          select
          fullWidth
          variant="outlined"
          label="Genero"
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
        >
          <MenuItem value="Patrocinador *">
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </TextField>

        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Telefono Personal"
          placeholder="Telefono Personal *"
          variant="outlined"
          name='private_phone'
          onChange={(e) => handleChangeUser(e)}
        />

        <TextField
          label="Fecha Nacimiento"
          variant="outlined"
          type="date"
          name='birthdate'
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          onChange={(e) => handleChangeUser(e)}
        />

        <TextField
          label="Ciudad de Residencia"
          name='city_of_residence'
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          placeholder="Ciudad de Residencia *"
          variant="outlined"
          onChange={(e) => handleChangeUser(e)}
        />

        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Pais de Nacimiento"
          name='country_of_birth'
          placeholder="Pais de Nacimiento *"
          variant="outlined"
          onChange={(e) => handleChangeUser(e)}
        />

        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="DNI"
          placeholder="DNI *"
          variant="outlined"
          onChange={(e) => handleChangeUser(e)}
          name='dni'
        />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          padding: 20,
          marginLeft: 0,
          marginRight: "8rem",
          width: "20rem",
        }}
      >
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Apellido"
          placeholder="Apellido *"
          variant="outlined"
          onChange={(e) => handleChangeUser(e)}
          name='lastname'
        />
        <TextField
          select
          fullWidth
          variant="outlined"
          label="Deporte"
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          
        >
          <MenuItem value={"futbol"}>Futbol</MenuItem>
        </TextField>
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Telefono de Contacto"
          placeholder="Telefono de Contacto *"
          variant="outlined"
          onChange={(e) => handleChangeUser(e)}
          name='telephone_contact'
        />
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Pais de Residencia"
          placeholder="Pais de Residencia *"
          variant="outlined"
          onChange={(e) => handleChangeUser(e)}
          name='country_of_residence'
        />
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Codigo Postal"
          placeholder="Codigo Postal *"
          variant="outlined"
          onChange={(e) => handleChangeUser(e)}
          name='zip_code'
        />
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Ciudad de Nacimiento"
          placeholder="Ciudad de Nacimiento*"
          variant="outlined"
          onChange={(e) => handleChangeUser(e)}
          name='city_of_birth'
        />
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Pasaporte "
          placeholder="Pasaporte *"
          variant="outlined"
          onChange={(e) => handleChangeUser(e)}
          name='passport'
        />
      </div>
    </form>
  );
};

export default DemographicDataForm;
