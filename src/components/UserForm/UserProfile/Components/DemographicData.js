import { Container, Typography } from "@material-ui/core";
import React from "react";
import DemographicDataForm from "./StepTwoComponents/DemographicDataForm";

const DemoData = ({handleChangeUser}) => {
  return (
    <Container
      style={{
        backgroundColor: "#001a1a",
        height: "40rem",
        width: "65rem",
        opacity: 0.8,
      }}
    >
      <Typography
        style={{
          color: "white",
          fontFamily: "Bebas-Bold",
          textAlign: "center",
          fontSize: "30px",
          paddingTop: "20px",
        }}
      >
        Datos demograficos
      </Typography>
      <DemographicDataForm handleChangeUser={handleChangeUser} />
    </Container>
  );
};

export default DemoData;
