import React from "react";
import SponsorData from "./SponsorData";
import SportsData from "./SportsData";

const ScreenThree = () => {
  return (
    <div
      style={{
        width: "80rem",
        height: "39rem",
        display: "flex",
        flexDirection: "row",
        justifyItems: "space-between",
        margin: 5,
      }}
    >
      <SportsData />
      <SponsorData />
    </div>
  );
};

export default ScreenThree;
