import React from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import PersonalDataForm from "../../UserProfile/Components/StepThreeComponents/PersonalDataForm";

const SportsData = () => {
  return (
    <Container
      style={{
        backgroundColor: "#001a1a",
        height: "39rem",
        width: "29rem",
        marginRight: 20,
        opacity: 0.8,
      }}
    >
      <Typography
        style={{
          color: "white",
          fontFamily: "Bebas-Bold",
          textAlign: "center",
          fontSize: "30px",
          paddingTop: "20px",
        }}
      >
        Datos Deportivos
      </Typography>
      <PersonalDataForm />
    </Container>
  );
};

export default SportsData;
