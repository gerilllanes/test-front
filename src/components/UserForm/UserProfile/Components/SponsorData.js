import { Container, Typography } from "@material-ui/core";
import React from "react";
import SponsorForm from "./StepThreeComponents/SponsorForm";

const SponsorData = () => {
  return (
    <Container
      style={{
        backgroundColor: "#001a1a",
        height: "39rem",
        width: "49rem",
        opacity: 0.8,
      }}
    >
      <Typography
        style={{
          color: "white",
          fontFamily: "Bebas-Bold",
          textAlign: "center",
          fontSize: "30px",
          paddingTop: "20px",
        }}
      >
        Datos Sponsor
      </Typography>
      <SponsorForm />
    </Container>
  );
};

export default SponsorData;
