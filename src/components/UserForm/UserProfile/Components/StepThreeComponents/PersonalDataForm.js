import React, { useState, useContext } from "react";
import TextField from "@material-ui/core/TextField";
import UserContext from "../../../../../context/UserContext/UserContext";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";

const useStyles = makeStyles((theme) => ({
  topContainer: {
    borderRadius: 4,
    width: "20rem",
    height: "1.8em",
    border: "2px solid",
    borderColor: "white",
    fontSize: 16,
    color: "white",
    padding: "5px 12px 5px 6px",
    margin: 10,
  },
  multilineColor: {
    borderWidth: "2px",
    borderColor: "white !important",
    margin: "5px",
    width: "9rem",
    opacity: 0.8,
  },
  inputs: {
    borderColor: "white",
    color: "white",
    marginBottom: "10px",
    fontSize: "20px",
    opacity: 0.8,
  },

  cssOutlinedInput: {
    border: "2px solid",
    color: "white",
    borderRadius: 3,
    opacity: 0.8,
  },
  notchedOutline: {
    borderWidth: "2px",
    borderColor: "white !important",
    margin: "5px",
    width: "20rem",
    opacity: 0.8,
  },
}));

const DataForm = () => {
  const classes = useStyles();
  const userContext = useContext(UserContext);
  const [value, setValue] = useState({
    club: 1,
  });

  const handleChange = (e) => {
    setValue(e.target.value);
  };
  return (
    <form
      style={{
        marginLeft: "2rem",
        marginTop: "4rem",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          margin: 10,
          justifyContent: "center",
        }}
      >
        <TextField
          label="Club"
          select
          variant="outlined"
          fullWidth
          onChange={handleChange}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
        >
          <MenuItem value="Club">
            <em>None</em>
          </MenuItem>
          <MenuItem value={1}>1</MenuItem>
        </TextField>

        <TextField
          label="Club"
          select
          variant="outlined"
          fullWidth
          onChange={handleChange}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
        >
          <MenuItem value="Club" required>
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </TextField>

        <TextField
          label="Posicion"
          select
          variant="outlined"
          fullWidth
          onChange={handleChange}
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
        >
          <MenuItem value="Club">
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </TextField>

        <TextField
          label="Fecha Inicio"
          type="date"
          variant="outlined"
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
        />
      </div>

      <div
        style={{
          display: "flex",
          flexDirection: "row",
        }}
      >
        <div
          style={{
            display: "flex",
            marginLeft: "10px",
            flexDirection: "column",
          }}
        >
          <TextField
            InputProps={{
              classes: {
                notchedOutline: classes.multilineColor,
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.inputs,
                focused: classes.inputs,
                notchedOutline: classes.notchedOutline,
              },
            }}
            label="Dorso"
            placeholder="Dorso"
            variant="outlined"
          />
          <TextField
            InputProps={{
              classes: {
                notchedOutline: classes.multilineColor,
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.inputs,
                focused: classes.inputs,
                notchedOutline: classes.notchedOutline,
              },
            }}
            label="Talla"
            placeholder="Talla"
            variant="outlined"
          />
          <TextField
            InputProps={{
              classes: {
                notchedOutline: classes.multilineColor,
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.inputs,
                focused: classes.inputs,
                notchedOutline: classes.notchedOutline,
              },
            }}
            label="Calzado"
            placeholder="Calzado"
            variant="outlined"
          />
        </div>
        <div
          style={{
            display: "flex",
            marginRight: "20px",
            flexDirection: "column",
          }}
        >
          <TextField
            InputProps={{
              classes: {
                notchedOutline: classes.multilineColor,
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.inputs,
                focused: classes.inputs,
                notchedOutline: classes.notchedOutline,
              },
            }}
            label="Pie Habil"
            placeholder="Pie Habil"
            variant="outlined"
          />
          <TextField
            InputProps={{
              classes: {
                notchedOutline: classes.multilineColor,
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.inputs,
                focused: classes.inputs,
                notchedOutline: classes.notchedOutline,
              },
            }}
            label="Altura"
            placeholder="Altura"
            variant="outlined"
          />
          <TextField
            InputProps={{
              classes: {
                notchedOutline: classes.multilineColor,
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.inputs,
                focused: classes.inputs,
                notchedOutline: classes.notchedOutline,
              },
            }}
            label="Peso"
            placeholder="Peso"
            variant="outlined"
          />
        </div>
      </div>
    </form>
  );
};

export default DataForm;
