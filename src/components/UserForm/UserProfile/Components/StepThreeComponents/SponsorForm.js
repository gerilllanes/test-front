import React from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";

const useStyles = makeStyles((theme) => ({
  multilineColor: {
    borderRadius: 4,
    width: "17rem",
    height: "1.8em",
    border: "2px solid",
    borderColor: "white",
    fontSize: 16,
    color: "white",
    padding: "5px 12px 5px 6px",
    margin: 15,
  },
  inputs: {
    borderColor: "white",
    color: "white",
    marginBottom: "10px",
    fontSize: "20px",
    opacity: 0.8,
  },

  cssOutlinedInput: {
    border: "2px solid",
    color: "white",
    borderRadius: 3,
    opacity: 0.8,
  },
  notchedOutline: {
    borderWidth: "2px",
    borderColor: "white !important",
    margin: "5px",
    width: "20rem",
    opacity: 0.8,
  },
}));

const SponsorForm = () => {
  const classes = useStyles();
  return (
    <form
      style={{
        display: "flex",
        flexDirection: "row",
        marginTop: "3rem",
        justifyContent: "space-between",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "20 rem",
          marginRight: "5rem",
          padding: 10,
        }}
      >
        <TextField
          fullWidth
          select
          variant="outlined"
          label="Patrocinador"
          title="Patrocinador"
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
        >
          <MenuItem value="Patrocinador *">
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>Ten</MenuItem>
          <MenuItem value={20}>Twenty</MenuItem>
          <MenuItem value={30}>Thirty</MenuItem>
        </TextField>

        <TextField
          label="Inicio Contrato"
          type="datetime-local"
          variant="outlined"
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
        />

        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Derecho Federativo"
          variant="outlined"
        />
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Derecho Imagen"
          placeholder="Derecho Imagen *"
          variant="outlined"
        />
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Relacion con el jugador"
          placeholder="Relacion con el jugador *"
          variant="outlined"
        />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          padding: 10,
          marginLeft: 10,
          marginRight: "8rem",
          width: "20rem",
        }}
      >
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Telefono Patrocinador"
          variant="outlined"
          placeholder="Telefono Patrocinador *"
        />
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          type="datetime-local"
          label="Fin Contrato"
          variant="outlined"
        />
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Derecho Economico"
          placeholder="Derecho Economico"
          variant="outlined"
        />

        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          placeholder=" Valor Jugador"
          id="standard-adornment-amount"
          variant="outlined"
        />
        <TextField
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
              notchedOutline: classes.notchedOutline,
            },
          }}
          label="Correo Electronico Patrocinador"
          placeholder="Correo Electronico Patrocinador *"
          variant="outlined"
        />
      </div>
    </form>
  );
};

export default SponsorForm;
