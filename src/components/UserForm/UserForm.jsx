import { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import playerAvatar from "../../assets/img/player-icons/maradona.png";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Container from "@material-ui/core/Container";
import ErrorIcon from '@material-ui/icons/Error';
import InputAdornment from '@material-ui/core/InputAdornment';

const useStyles = makeStyles({
  formContainer: {
    backgroundColor: "#001a1a",
    width: "33rem",
    height: "40rem",
    padding: "8px",
    opacity: 0.8,
  },
  avatar: {
    width: "250px",
    height: "200px",
    marginTop: "1rem",
    marginLeft: "8rem",
  },
  formControl: {
    color: "#fff",
    borderColor: "#fff",
    marginTop: "1rem",
  },
  multilineColor: {
    borderRadius: "3px",
    border: "3px solid",
    borderColor: "white",
    fontSize: 20,
    color: "white",
  },
  inputs: {
    borderColor: "cyan",
    color: "white",
    marginBottom: "10px",
    fontSize: "20px",
  },

  cssOutlinedInput: {
    border: "2px solid",
    color: "cyan",
    borderRadius: 3,
  },
  notchedOutline: {
    borderWidth: "3px",
    borderColor: "white !important",
    margin: "5px",
    width: "28rem",
    height: "4rem",
    color: 'white'
  },
  lcdtm: {
    color: 'white'
  }
});

const UserForm = ({ handleChangeUser }) => {
  const classes = useStyles();
  const [userRole, setUserRole] = useState("");
  const [error, setError] = useState(false)

  const roles = [
    "ROLE_TUTOR",
    "ROLE_CLUB_ADMIN",
    "ROLE_EXECUTIVE",
    "ROLE_COORDINATOR",
    "ROLE_TECHNICAL_DIRECTOR",
    "ROLE_DOCTOR",
    "ROLE_PHYSICAL_TRAINER",
    "ROLE_PLAYER",
    "ROLE_PSYCHOLOGIST",
  ];

  return (
    <Container className={classes.formContainer}>
      <h1 style={{ textAlign: "center" }}>User data</h1>
      <Avatar src={playerAvatar} className={classes.avatar} />
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          padding: "1.2rem",
        }}
      >
        <TextField
          label="Email"
          placeholder="Email"
          name='email'
          variant="outlined"
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              root: classes.inputs
            },
          }}
          InputLabelProps={{
            classes: {
              root: classes.inputs,
              focused: classes.inputs,
            },
          }}
          onChange={(e) => handleChangeUser(e)}
        />
        <FormControl variant="outlined" className={classes.formControl}>
          <TextField
            select
            id="role"
            name="role"
            label="role"
            required
            fullWidth
            variant="outlined"
            value={userRole}
            onChange={(e) => {
              setUserRole(e.target.value);
              handleChangeUser(e);
            }}
            InputProps={{
              classes: {
                notchedOutline: classes.notchedOutline,
                root: classes.inputs
              },
            }}
            InputLabelProps={{
              classes: {
                root: classes.inputs,
                focused: classes.inputs,
              },
            }}
          >
            {roles.map((role, i) => (
              <MenuItem key={i} value={role}>
                {role}
              </MenuItem>
            ))}
          </TextField>
        </FormControl>
      </div>
    </Container>
  );
};

export default UserForm;
