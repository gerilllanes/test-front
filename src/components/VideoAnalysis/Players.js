import { Container, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import diego from "../../assets/img/Enmascarar_grupo_1.png";

const useStyles = makeStyles(() => ({
  flexcontainer: {
    display: "flex",
  },
  flexitem: {
    minWidth: "70px",
    height: "70px",
    margin: "10px 0",
    marginBottom: "0",
    flex: 1,
  },
  image: {
    width: "65px",
    height: "65px",
    borderRadius: "150px",
    display: "block",
    margin: "auto",
  },
}));

const Players = () => {
  const boxes = [
    {
      id: 1,
    },
    {
      id: 2,
    },
    {
      id: 3,
    },
    {
      id: 4,
    },
    {
      id: 5,
    },
    {
      id: 6,
    },
    {
      id: 7,
    },
    {
      id: 8,
    },
  ];

  const classes = useStyles();

  return (
    <>
      <Container maxWidth="xl" className={classes.flexcontainer}>
        <Grid container spacing={1}>
          <>
            {boxes.map((inf, i) => {
              return (
                <Grid
                  item
                  xs={1}
                  sm={1}
                  md={1}
                  lg={1}
                  key={`${inf.id}-${i}`}
                  className={classes.flexitem}
                >
                  <img src={diego} alt="" className={classes.image}>
                    {inf.imagen}
                  </img>
                </Grid>
              );
            })}
          </>
        </Grid>
      </Container>
    </>
  );
};

export default Players;
