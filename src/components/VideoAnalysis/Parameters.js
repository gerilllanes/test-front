import { withStyles, makeStyles } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import DetailsTwoToneIcon from "@material-ui/icons/DetailsTwoTone";
import Slider from "@material-ui/core/Slider";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

const SliderValues = withStyles({
  root: {
    color: "#9bf5fb",
    height: "0",
    fontFamily: "Bebas-Bold",
    "&:not(:last-child)": {
      borderBottom: 0,
    },
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: "#fff",
    border: "2px solid currentColor",
    marginTop: -8,
    marginLeft: -12,
    "&:focus, &:hover, &$active": {
      boxShadow: "inherit",
    },
  },
  active: {},
  valueLabel: {
    left: "calc(-50% + 4px)",
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider);

const useStyles = makeStyles(() => ({
  root: {
    width: "90%",
    height: "10vh",
    margin: "0 15px",
  },
  expanded: {
    "&$expanded": {
      margin: 0,
    },
  },
  accordion: {
    backgroundColor: "#102e2f",
    height: "11vh",
  },
  accordionDetails: {
    backgroundColor: "#2b565f",
  },
  icon: {
    color: "#9bf5fb",
    margin: "0px",
  },
  heading: {
    fontFamily: "Bebas-Light",
    color: "white",
  },
  headingCheckbox: {
    fontFamily: "Bebas-Bold",
  },
  text: {
    fontFamily: "Bebas-Bold",
  },
  image: {
    transform: "rotate(-90deg)",
    width: "100%"
  },
}));

const Parameters = ({ title }) => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.root}>
        <Accordion classes={{ expanded: classes.expanded }}>
          <AccordionSummary
            expandIcon={<DetailsTwoToneIcon style={{ color: "#9bf5fb" }} />}
            aria-controls="panel1a-content"
            id="panel1a-header"
            className={classes.accordion}
          >
            <Typography className={classes.heading}>{title}</Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.accordionDetails}>
            <SliderValues
              valueLabelDisplay="auto"
              min={1}
              max={12}
              aria-label="pretto slider"
              defaultValue={2}
            />
          </AccordionDetails>
        </Accordion>
        <Accordion classes={{ expanded: classes.expanded }}>
          <AccordionSummary
            expandIcon={<DetailsTwoToneIcon style={{ color: "#9bf5fb" }} />}
            aria-controls="panel2a-content"
            id="panel2a-header"
            className={classes.accordion}
          >
            <Typography className={classes.heading}>AFA</Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.accordionDetails}>
            <div>
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.icon}
                label={
                  <Typography className={classes.headingCheckbox}>T</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.icon}
                label={
                  <Typography className={classes.headingCheckbox}>L</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.icon}
                label={
                  <Typography className={classes.headingCheckbox}>V</Typography>
                }
              />
              <SliderValues
                valueLabelDisplay="auto"
                min={7}
                max={16}
                aria-label="pretto slider"
                defaultValue={2}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                label={
                  <Typography className={classes.heading}>TODAS CAT</Typography>
                }
                className={classes.text}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>4DIV</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>5DIV</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>6DIV</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>7DIV</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>8DIV</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>9DIV</Typography>
                }
              />
            </div>
          </AccordionDetails>
        </Accordion>
        <Accordion classes={{ expanded: classes.expanded }}>
          <AccordionSummary
            expandIcon={<DetailsTwoToneIcon style={{ color: "#9bf5fb" }} />}
            aria-controls="panel2a-content"
            id="panel2a-header"
            className={classes.accordion}
          >
            <Typography className={classes.heading}>TODA POSIC</Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.accordionDetails}>
            <div>
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.icon}
                label={
                  <Typography className={classes.headingCheckbox}>T</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.icon}
                label={
                  <Typography className={classes.headingCheckbox}>L</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.icon}
                label={
                  <Typography className={classes.headingCheckbox}>V</Typography>
                }
              />
              <SliderValues
                valueLabelDisplay="auto"
                min={7}
                max={16}
                aria-label="pretto slider"
                defaultValue={2}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                label={
                  <Typography className={classes.heading}>TODOS</Typography>
                }
                className={classes.text}
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>GOLES</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>
                    ASISTENCIAS
                  </Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>1VS1 OF</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>1VS1 DEF</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>1VS1 AER</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>REMATES</Typography>
                }
              />
              <FormControlLabel
                control={
                  <Checkbox
                    icon={<RadioButtonUncheckedIcon className={classes.icon} />}
                    checkedIcon={
                      <RadioButtonCheckedIcon className={classes.icon} />
                    }
                    name="checkedH"
                  />
                }
                className={classes.text}
                label={
                  <Typography className={classes.heading}>
                    BALON PARADO
                  </Typography>
                }
              />
            </div>
          </AccordionDetails>
        </Accordion>
      </div>
    </>
  );
};

export default Parameters;
