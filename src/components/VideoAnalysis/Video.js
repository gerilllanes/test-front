import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  container: {
    display: "block",
    marginTop: "10px"
  },
  video: {
    width: "100%",
    height: "60vh",
  },
}));

const Video = () => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.container}>
        <iframe
          className={classes.video}
          src="https://www.youtube.com/embed/6iXWtMOwzqk"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
      </div>
    </>
  );
};

export default Video;
