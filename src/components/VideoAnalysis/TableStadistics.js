import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import PlayCircleFilledIcon from "@material-ui/icons/PlayCircleFilled";
import { Typography } from "@material-ui/core";

const TableStadistics = () => {
  const useStyles = makeStyles((theme) => ({
    root: {
      display: "flex",
    },
    container: {
      width: "100%",
    },
    content: {
      display: "flex",
      backgroundColor: "#2b565f",
      justifyContent: "left",
      marginTop: "3px",
      marginRight: "5px",
      paddingLeft: "8px",
      marginBottom: "5px",
      paddingTop: "3px",
      paddingBottom: "3px",
      overflow: "hidden",
      maxHeight: "80px",
    },
    title: {
      textAlign: "start",
      height: "40px",
      marginRight: "5px",
      marginBottom: "5px",
      paddingLeft: "10px"
    },
    frontText: {
      height: "40px",
      marginRight: "10px",
      fontSize: "14px",
    },
    text: {
      fontSize: "12px",
      fontFamily: "Bebas-Light",
      marginRight: "10px",
    },
    icon: {
      margin: "auto 5px",
      color: "#9bf5fb",
      flex: "right"
    },
    font: {
      fontFamily: "Bebas-Bold",
      paddingTop: "6px",
    },
  }));
  const classes = useStyles();

  const items = [
    {
      label: "TOTAL PARTIDOS",
      color: "#102e2f",
      key: 0,
      subitems: [
        {
          id: 0,
          label: "BARCELONA 1/3 REAL MADRID",
          sublabel: "LA LIGA 22/22/22",
        },
        {
          id: 1,
          label: "BARCELONA 1/3 REAL MADRID",
          sublabel: "LA LIGA 22/22/22",
        },
        {
          id: 2,
          label: "BARCELONA 1/3 REAL MADRID",
          sublabel: "LA LIGA 22/22/22",
        },
      ],
    },
    {
      label: "AMF CF RAMF LI MC",
      color: "#102e2f",
      key: 1,
      subitems: [
        {
          id: 0,
          label: "LI",
          sublabel: "0' PT A 25' ST - 70 MIN",
        },
        {
          id: 0,
          label: "MC",
          sublabel: "0' PT A 25' ST - 70 MIN",
        },
        {
          id: 0,
          label: "AMF CD RAMF",
          sublabel: "0' PT A 25' ST - 70 MIN",
        },
      ],
    },
    {
      label: "GOLES",
      color: "#102e2f",
      key: 2,
      subitems: [
        {
          id: 0,
          label: "GOLES",
          sublabel: "1",
        },
        {
          id: 0,
          label: "GOLES",
          sublabel: "0",
        },
        {
          id: 0,
          label: "GOLES",
          sublabel: "2",
        },
      ],
    },
    {
      label: "ASISTENCIAS",
      color: "#102e2f",
      key: 3,
      subitems: [
        {
          id: 0,
          label: "ASISTENCIAS",
          sublabel: "0",
        },
        {
          id: 0,
          label: "ASISTENCIAS",
          sublabel: "1",
        },
        {
          id: 0,
          label: "ASISTENCIAS",
          sublabel: "0",
        },
      ],
    },
    {
      label: "1VS1 OF",
      color: "#102e2f",
      key: 4,
      subitems: [
        {
          id: 0,
          label: "1VS1 OF",
          sublabel: "3",
        },
        {
          id: 0,
          label: "1VS1 OF",
          sublabel: "3",
        },
        {
          id: 0,
          label: "1VS1 OF",
          sublabel: "3",
        },
      ],
    },
    {
      label: "1VS1 DEF",
      color: "#102e2f",
      key: 5,
      subitems: [
        {
          id: 0,
          label: "1VS1 DEF",
          sublabel: "3",
        },
        {
          id: 0,
          label: "1VS1 DEF",
          sublabel: "3",
        },
        {
          id: 0,
          label: "1VS1 DEF",
          sublabel: "3",
        },
      ],
    },
    {
      label: "1VS1 AER",
      color: "#102e2f",
      key: 6,
      subitems: [
        {
          id: 0,
          label: "1VS1 AER",
          sublabel: "3",
        },
        {
          id: 0,
          label: "1VS1 AER",
          sublabel: "3",
        },
        {
          id: 0,
          label: "1VS1 AER",
          sublabel: "3",
        },
      ],
    },
    {
      label: "REMATES",
      color: "#102e2f",
      key: 7,
      subitems: [
        {
          id: 0,
          label: "REMATES",
          sublabel: "3",
        },
        {
          id: 0,
          label: "REMATES",
          sublabel: "3",
        },
        {
          id: 0,
          label: "REMATES",
          sublabel: "3",
        },
      ],
    },
  ];

  return (
    <div className={classes.root}>
      {items.map((item, key) => {
        return (
          <div className={classes.container} key={key}>
            <div
              className={classes.title}
              style={{ backgroundColor: item.color }}
            >
              <Typography
                variant="subtitle1"
                gutterBottom
                className={classes.font}
              >
                {item.label}
              </Typography>
            </div>
            {item.subitems.map((subitem, key) => {
              return (
                <div className={classes.content} key={key}>
                  <div className={classes.frontText}>
                    <div>{subitem.label}</div>
                    <div style={{ fontFamily: "Bebas-Light" }}>
                      {subitem.sublabel}
                    </div>
                  </div>
                  <div className={classes.icon}>
                    <PlayCircleFilledIcon
                      fontSize={"small"}
                    />
                  </div>
                </div>
              );
            })}
          </div>
        );
      })}
    </div>
  );
};

export default TableStadistics;

