import { useHistory } from 'react-router-dom';
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    width: "auto"
  },
  button: {
    border: "2px solid white",
    borderRadius: 0,
    padding: 0,
    marginRight: 30
  },
  icon: {
    color: "white",
    fontSize: 40,
  },
  title: {
    fontFamily: "Bebas-Bold",
    fontSize: 38,
    lineHeight: 1,
    alignItems: "center",
    margin: 4
  }
}));

const Header = ({title}) => {
  const classes = useStyles();
  const history = useHistory();
 
  const handleClick = () => {
    history.push("/dashboard");
  };

  return (
    <>
      <Box className={classes.root}>
        <Button className={classes.button} onClick={handleClick}>
          <ArrowBackIosIcon className={classes.icon} />
        </Button>
        <Typography className={classes.title}>{title}</Typography>
      </Box>
    </>
  );
};

export default Header;
