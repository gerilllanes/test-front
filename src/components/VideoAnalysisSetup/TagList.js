import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import Typography from "@material-ui/core/Typography";
import TagItem from './TagItem';

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    backgroundColor: "rgba(0, 0, 0, 0.1)",
    //backgroundColor: "#2b565f",
    padding: 15,
    color: "white",
    width: "auto"
  },
  header: {
    fontFamily: "Bebas-Bold",
    fontSize: 18
  },
  icon: {
    color: "#9bf5fb",
    margin: "0px",
    height: 20
  },
  label: {
    fontFamily: "Bebas-Light",
    fontSize: 16,
  }
}));

const TagList = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <FormControl>
        <Typography className={classes.header}>{props.title}</Typography>
        <FormGroup>
          <TagItem label="Todos" />
          <TagItem label="Jugadores" />
        </FormGroup>
      </FormControl>
    </div>
  );
};

export default TagList;
