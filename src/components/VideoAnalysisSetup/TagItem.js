import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(() => ({
  icon: {
    color: "#9bf5fb",
    margin: "0px",
    height: 20,
  },
  label: {
    fontFamily: "Bebas-Light",
    fontSize: 16,
  },
}));

const TagItem = (props) => {
  const classes = useStyles();

  return (
    <div>
      <FormControlLabel
        control={
          <Checkbox
            icon={<RadioButtonUncheckedIcon className={classes.icon} />}
            checkedIcon={<RadioButtonCheckedIcon className={classes.icon} />}
            name="checkedH"
          />
        }
        label={<Typography className={classes.label}>{props.label}</Typography>}
      />
    </div>
  );
};

export default TagItem;
