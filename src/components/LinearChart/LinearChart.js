import { Button } from "@material-ui/core";
import {
  Tooltip,
  Legend,
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
} from "recharts";
import MomentUtils from "@date-io/moment";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import moment from "moment";
import { makeStyles } from "@material-ui/core/styles";
import { getRandomColor } from "../../utils/utils";

const useStyles = makeStyles(() => ({
  root: {
    "& > *": {
      fontFamily: "Bebas-Bold",
    },
  },
  btn: {
    backgroundColor: "#1E4249",
    color: "#E1F5E9",
    borderRadius: 0,
    "&:hover": {
      backgroundColor: "#0A1A24",
      color: "#B1EFF4",
    },
    fontFamily: "Bebas-Bold !important",
    fontSize: "1rem !important",
    marginRight: "1rem",
  },
  active: {
    backgroundColor: "#0A1A24",
    color: "#B1EFF4",
    fontFamily: "Bebas-Bold",
    fontSize: "24px",
  },
  link: {
    textDecoration: "none",
  },
  margin: {
    marginBottom: "5px",
  },
  datePicker: {
    display: "flex",
    backgroundColor: "#1E4249",
    color: "white",
    width: "90%",
    height: "100%",
    "& .MuiInputBase-input": {
      fontFamily: "Bebas-Bold",
      color: "#fff",
    },
  },
}));

const LinearChart = ({
  data,
  handleClickYear,
  handleClickMonth,
  handleClickWeek,
  handleClickDay,
  handleClickLast,
  handleClickStaff,
  handleClickPlayer,
  handleClickAll,
  handleClickAutoevaluation,
  skillsNames,
  selectedDateFrom,
  inputValueFrom,
  dateFormatterFrom,
  onDateChangeFrom,
  selectedDateTo,
  inputValueTo,
  onDateChangeTo,
  dateFormatterTo,
  handleClickDates,
}) => {
  const classes = useStyles();

  console.log("skillsNames", skillsNames);

  return (
    <div>
      <nav
        style={{ display: "flex", marginLeft: "4rem", marginBottom: "1rem" }}
      >
        <Button
          className={classes.btn}
          value="All Profiles"
          aria-label="bold"
          onClick={handleClickAll}
        >
          All Profiles
        </Button>
        <Button
          className={classes.btn}
          value="Staff Evaluation"
          aria-label="bold"
          onClick={handleClickStaff}
        >
          Staff Evaluation
        </Button>
        <Button
          className={classes.btn}
          value="Teammates Evaluation"
          aria-label="bold"
          onClick={handleClickPlayer}
        >
          Teammates Evaluation
        </Button>
        <Button
          className={classes.btn}
          value="Autoevaluation"
          aria-label="bold"
          onClick={handleClickAutoevaluation}
        >
          Autoevaluation
        </Button>
      </nav>
      {skillsNames && (
        <LineChart
          width={790}
          height={250}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <Tooltip />
          <CartesianGrid stroke="#fff" />
          <XAxis dataKey="name" stroke="#fff" />
          <YAxis domain={[0, 5]} tickCount={6} stroke="#fff" />
          {skillsNames.map((s, key) => {
            return (
              <Line
                type="monotone"
                dot
                key={key}
                dataKey={s}
                stroke={getRandomColor()}
                strokeWidth={5}
                strokeDasharray="4 4"
              />
            );
          })}
        </LineChart>
      )}
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <nav style={{ display: "flex", marginLeft: "4rem" }}>
          <Button
            className={classes.btn}
            variant="contained"
            onClick={handleClickYear}
          >
            Year
          </Button>
          <Button
            className={classes.btn}
            variant="contained"
            onClick={handleClickMonth}
          >
            Month
          </Button>
          <Button
            className={classes.btn}
            variant="contained"
            onClick={handleClickWeek}
          >
            Week
          </Button>
          <Button
            className={classes.btn}
            variant="contained"
            onClick={handleClickDay}
          >
            Day
          </Button>
          <Button
            className={classes.btn}
            variant="contained"
            onClick={handleClickLast}
          >
            Last
          </Button>
        </nav>
        <nav style={{ display: "flex" }}>
          <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils}>
            <div className={classes.datePicker}>
              <KeyboardDatePicker
                autoOk={true}
                showTodayButton={true}
                value={selectedDateFrom}
                format="DD-MM-YYYY"
                inputValue={inputValueFrom}
                onChange={onDateChangeFrom}
                rifmFormatter={dateFormatterFrom}
                style={{ marginRight: "3px" }}
              />
              <KeyboardDatePicker
                autoOk={true}
                showTodayButton={true}
                value={selectedDateTo}
                format="DD-MM-YYYY"
                inputValue={inputValueTo}
                onChange={onDateChangeTo}
                rifmFormatter={dateFormatterTo}
              />
            </div>
          </MuiPickersUtilsProvider>
          <Button
            className={classes.btn}
            style={{ marginLeft: "5px", width: "5px" }}
            variant="contained"
            onClick={handleClickDates}
          >
            Load
          </Button>
        </nav>
      </div>
    </div>
  );
};

export default LinearChart;
