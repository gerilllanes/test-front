import React from "react";
import Alert from "@material-ui/lab/Alert";

const AlertInfo = ({ text }) => {
  return <Alert severity="error">{text}</Alert>;
};

export default AlertInfo;
