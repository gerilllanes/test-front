import React from "react";
import Alert from "@material-ui/lab/Alert";

const AlertInfo = ({ text }) => {
  return <Alert severity="info">{text}</Alert>;
};

export default AlertInfo;
