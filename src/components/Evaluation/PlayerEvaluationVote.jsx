import { useState } from "react";
import Grid from "@material-ui/core/Grid";
import Rating from "@material-ui/lab/Rating";
import StarBorderIcon from "@material-ui/icons/StarBorder";

import { formatMs, withStyles } from "@material-ui/core/styles";

const StyledRating = withStyles({
  iconFilled: {
    color: "#9bf5fb",
  },
})(Rating);

const PlayerEvaluationVote = ({ skillGroup, handleChangeSkillValues }) => {
  const [saveSkills, setSaveSkills] = useState("");
  console.log(saveSkills);

  return (
    <div style={{ width: "100%", overflowY: "scroll", height: "100%" }}>
      <Grid container>
        {skillGroup.length &&
          skillGroup.map((skill) => (
            <Grid item xs={3} key={skill.id}>
              <h1>{skill.name}</h1>
              <div>
                {skill.skills.map((s) => (
                  <div
                    style={{ display: "flex", alignItems: "center" }}
                    key={s.id}
                  >
                    <span style={{ marginRight: "0.5em" }}>{s.name}</span>
                    <StyledRating
                      emptyIcon={
                        <StarBorderIcon
                          fontSize="inherit"
                          style={{ color: "#9bf5fb" }}
                        />
                      }
                      onChange={(e) => {
                        setSaveSkills((saveSkills) => [
                          ...saveSkills,
                          `${s.id}=${e.target.value}`,
                        ]);
                        handleChangeSkillValues(saveSkills.toString());
                      }}
                    />
                  </div>
                ))}
              </div>
            </Grid>
          ))}
      </Grid>
    </div>
  );
};

export default PlayerEvaluationVote;
