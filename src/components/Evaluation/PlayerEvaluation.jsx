import Grid from "@material-ui/core/Grid";
import Rating from "@material-ui/lab/Rating";
import StarBorderIcon from "@material-ui/icons/StarBorder";

import { withStyles } from "@material-ui/core/styles";

const StyledRating = withStyles({
  iconFilled: {
    color: "#9bf5fb",
  },
})(Rating);

const PlayerEvaluation = ({ skillAverage, handleChangeLine }) => {
  return (
    <div style={{ width: "100%", overflowY: "scroll", height: "90%" }}>
      <Grid container>
        {skillAverage &&
          skillAverage.map((skill) => (
            <Grid item xs={3} key={skill.id}>
              <h1 onClick={handleChangeLine}>{skill.name}</h1>
              <div>
                {skill &&
                  skill.skills.map((s) => (
                    <div
                      style={{ display: "flex", alignItems: "center" }}
                      key={s.id}
                    >
                      <span style={{ marginRight: "0.5em" }}>{s.name}</span>
                      <StyledRating
                        emptyIcon={
                          <StarBorderIcon
                            fontSize="inherit"
                            style={{ color: "#9bf5fb" }}
                          />
                        }
                        readOnly
                        value={s.avg}
                      />
                    </div>
                  ))}
              </div>
            </Grid>
          ))}
      </Grid>
    </div>
  );
};

export default PlayerEvaluation;
