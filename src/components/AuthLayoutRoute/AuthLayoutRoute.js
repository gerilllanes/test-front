import React from "react";
import { Route } from "react-router";
import AuthLayout from "../../pages/layout/AuthLayout";

const AuthLayoutRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => (
        <AuthLayout>
          <Component {...props} />
        </AuthLayout>
      )}
    />
  );
};

export default AuthLayoutRoute;
