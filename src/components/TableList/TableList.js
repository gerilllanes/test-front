import MaterialTable from "material-table";
import { useState } from "react";

const TableList = ({
  columns,
  data,
  title,
  loadingTable,
  actions,
  search,
  rowClick = null,
  components = null,
}) => {
  const [selectedRow, setSelectedRow] = useState(null);

  return (
    <>
      <MaterialTable
        columns={columns}
        components={components ? components : null}
        data={data}
        title={title ? title : null}
        isLoading={loadingTable}
        onRowClick={(event, rowData) => rowClick ? rowClick(rowData.ID) : null}
        style={{
          color: "white",
          backgroundColor: "rgba(0, 0, 0, 0.0)",
          borderRadius: "0",
        }}
        options={{
          filtering: false,
          search: search ? true : false,
          searchFieldStyle: {
            color: "white",
            borderBlockColor: "rgba(255, 255, 255, 0.1)",
          },
          headerStyle: {
            color: "white",
            backgroundColor: "transparent",
            borderBlockColor: "rgba(0, 0, 0, 0.0)",
            width: "auto",
            fontFamily: "Bebas-Bold",
            fontSize: "30px",
          },
          rowStyle: (rowData) => ({
            backgroundColor: selectedRow === rowData.tableData.id,
            fontFamily: "Bebas-Bold",
          }),
          cellStyle: {
            borderBlockColor: "rgba(0, 0, 0, 0.0)",
            backgroundColor: "rgba(0, 0, 0, 0.0)",
          },
        }}
        actions={actions}
      />
    </>
  );
};

export default TableList;
