import React, { useContext } from "react";
import { useLocation } from "react-router-dom";

import Login from "../../pages/Login/Login";
import UserContext from "../../context/UserContext/UserContext";
import AuthLayoutRoute from "../AuthLayoutRoute/AuthLayoutRoute";

const RouteProtector = ({ children }) => {
  const location = useLocation();
  const userContext = useContext(UserContext);

  const existToken = () => {
    return userContext.logged && userContext.userLogged.id;
  };

  const checkPath = () => {
    return ["/login", "/register", "/recover", "/terms"].includes(
      location.pathname
    );
  };

  return (
    <>
      {existToken() ? (
        <> {children} </>
      ) : !checkPath() ? (
        <AuthLayoutRoute component={Login} />
      ) : (
        <></>
      )}
    </>
  );
};
export default RouteProtector;
