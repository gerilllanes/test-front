import {useEffect} from 'react';

//import Highcharts from "./highcharts";

const HighChart = (props) => {

	const {id, type, config, drilldown = false, className} = props;

	
	const handleChart = () => {

		const Highcharts = window.Highcharts;
		Highcharts.charts = [];

		try {
			switch(type) {
				case 'chart':
					Highcharts.chart(`highchart-id-${id}`, config);
					break;
				case 'stockChart':
					Highcharts.stockChart(`highchart-id-${id}`, config);
					break;
				default:
			}
		}catch(err) {
			console.log(err.message);
		}
	}

    useEffect( () => {

		const _highcharts = document.createElement('script');
		const _data = document.createElement('script');
		const _drilldown = document.createElement('script');
		const _exporting = document.createElement('script');
		const _export_data = document.createElement('script');
		const _accessibility = document.createElement('script');

		if(id && type && config){

			_highcharts.src = "/static/js/highcharts.js";
			_highcharts.async = true;
			document.head.appendChild(_highcharts);

			_data.src = "/static/js/data.js";
			_data.async = true;
			setTimeout( () => {
				document.head.appendChild(_data);
			}, 300);

			_drilldown.src = "/static/js/drilldown.js";
			_drilldown.async = true;
			setTimeout( () => {
				document.head.appendChild(_drilldown);
			}, 300);

			_exporting.src = "/static/js/exporting.js";
			_exporting.async = true;
			setTimeout( () => {
				document.head.appendChild(_exporting);
			}, 400);
			
			_export_data.src = "/static/js/export-data.js";
			_export_data.async = true;
			setTimeout( () => {
				document.head.appendChild(_export_data);
			}, 500);

			_accessibility.src = "/static/js/accessibility.js";
			_accessibility.async = true;
			setTimeout( () => {
				document.head.appendChild(_accessibility);
			}, 600);
		}

		setTimeout( () => {
			handleChart();
		}, 1000);

        return () => {
			try {
				document.head.removeChild(_exporting);
				document.head.removeChild(_data);
				if(_drilldown){
					document.head.removeChild(_drilldown);
					document.head.removeChild(_accessibility);
					document.head.removeChild(_export_data);
				}
			}catch(err) {
				console.log(err.message);
			}
        }
    }, []);

    return	<figure className="highcharts-figure">
				<div className={`highcharts-figure-container${className ? ' '+className: ''}`} id={`highchart-id-${id}`}></div>
			</figure>
};
export default HighChart;