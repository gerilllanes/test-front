import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

import "./App.css";
import Login from "./pages/Login/Login";
import Player from "./pages/Player/Player";
import SideBar from "./pages/layout/SideBar";
import Register from "./pages/Register/Register";
import Recover from "./pages/Recover/Recover";
import Dashboard from "./pages/Dashboard/Dashboard";
import UserState from "./context/UserContext/UserState";
import PlayerState from "./context/PlayerContext/PlayerState";
import VideoAnalysis from "./pages/VideoAnalysis/VideoAnalysis";
import RouteProtector from "./components/routeProtector/RouteProtector";
import AuthLayoutRoute from "./components/AuthLayoutRoute/AuthLayoutRoute";
import Gps from "./pages/Gps/Gps";
import Terms from "./pages/Terms/Terms";
import PlayerValoration from "./pages/PlayerInfo/PlayerValoration";
import MakeEvaluation from "./pages/MakeEvaluation/MakeEvaluation";
import EvaluationState from "./context/EvaluationContext/EvaluationState";
import SkillsState from "./context/SkillsContext/SkillsState";
import PlayerSummary from "./pages/PlayerSummary/PlayerSummary";
import AdminUsers from "./pages/Admin/AdminUsers/AdminUsers";
import Team from "./pages/Team/Team";
import User from "./pages/User/User";
import AdminClub from "./pages/Admin/AdminClub/AdminClub";
import Category from "./pages/Category/Category";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <UserState>
            <PlayerState>
              <EvaluationState>
                <SkillsState>
                <Redirect from="/" to="/login" />
                  <AuthLayoutRoute
                    exact
                    path="/register"
                    component={Register}
                  />
                  <AuthLayoutRoute exact path="/login" component={Login} />
                  <AuthLayoutRoute exact path="/terms" component={Terms} />
                  <AuthLayoutRoute exact path="/recover" component={Recover} />
                  <RouteProtector exact path="/dashboard">
                    <Route exact path="/dashboard">
                      <SideBar
                        breadcrumVisible={false}
                        breadcrumActive="Dashboard"
                        className="background-dashboard"
                      >
                        <Dashboard />
                      </SideBar>
                    </Route>
                    <Route exact path="/player">
                      <SideBar
                        breadcrumVisible={false}
                        breadcrumActive="Players list"
                      >
                        <Player />
                      </SideBar>
                    </Route>
                    <Route exact path="/gps">
                      <SideBar breadcrumActive="gps">
                        <Gps />
                      </SideBar>
                    </Route>
                    <Route exact path="/category">
                      <SideBar
                        breadcrumActive="category"
                        breadcrumVisible={false}
                      >
                        <Category />
                      </SideBar>
                    </Route>
                    <Route exact path="/make-evaluation">
                      <SideBar
                        breadcrumVisible={false}
                        breadcrumActive="make an evaluation"
                      >
                        <MakeEvaluation />
                      </SideBar>
                    </Route>
                    <Route exact path="/player/summary/:id">
                      <SideBar
                        breadcrumVisible={true}
                        breadcrumActive="summary"
                      >
                        <PlayerSummary />
                      </SideBar>
                    </Route>
                    <Route exact path="/player/valoration/:id">
                      <SideBar
                        breadcrumVisible={true}
                        breadcrumActive="Valoration"
                      >
                        <PlayerValoration />
                      </SideBar>
                    </Route>
                    <Route exact path="/player/video_analysis/:id">
                      <SideBar
                        breadcrumVisible={true}
                        breadcrumActive="Video Analysis"
                      >
                        <VideoAnalysis />
                      </SideBar>
                    </Route>
                    <Route exact path="/admin/users">
                      <AdminUsers />
                    </Route>
                    <Route exact path="/admin/club">
                      <AdminClub />
                    </Route>
                    <Route exact path="/team">
                      <SideBar breadcrumVisible={false} breadcrumActive="Team">
                        <Team />
                      </SideBar>
                    </Route>
                    <Route exact path="/user" className="background-dashboard">
                      <User />
                    </Route>
                  </RouteProtector>
                </SkillsState>
              </EvaluationState>
            </PlayerState>
          </UserState>
        </Switch>
      </Router>
    </div>
  );
}
export default App;
