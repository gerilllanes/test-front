export const buildDataSend = (data) => {
  let dataSend = new FormData();
  Object.keys(data).map((input) => {
    dataSend.append(input, data[input]);
    return null;
  });
  return dataSend;
};

export function getRandomColor() {
  //var letters = '0123456789ABCDEF';
  var letters = '3344556789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

export function fixSecoundsToHourFormat(segundos){

  let hours = segundos / 3600;
  hours = (hours+"").split(".");
  let minutes =  ("0."+hours[1]) * 60;
  if(hours[0] < 10){
    hours = `0${hours[0]}`;
  }else{
    hours = hours[0];
  }
  minutes = (minutes+"").split(".");
  if(minutes[0] < 10){
    minutes[0] = `0${minutes[0]}`;
  }
  let seg = segundos % 60;
  if(seg < 10){
    seg = `0${seg}`;
  }
  //console.log('time:', segundos+" --> "+hours+":"+minutes[0]+":"+seg);
  return hours+":"+minutes[0]+":"+parseInt(seg);
}

export function fixHourFormatToSecounds(hourFormat){

  //console.log('hourFormat', hourFormat);
  let splitHourFormat = hourFormat.split(":");
  let seg = (splitHourFormat[0] * 1) * 3600;
  seg = seg + ((splitHourFormat[1] * 1) * 60);
  seg = seg + (splitHourFormat[1] * 2);
  return seg;
}