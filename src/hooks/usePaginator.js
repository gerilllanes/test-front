import React, { useState } from 'react';

import "./usePaginator.css";

const Paginator = React.memo(({page = 1, total = 1, limit = 10, navigation = true, steps = 2, setPage = null, className = null, id = null }) => {

    const totalPage = [...new Array(Math.ceil((total / limit))).keys() ];
    page = page > totalPage.length ? totalPage.length : page;
    const rangeMinMax = ((steps * 2));
    let minPage = page - steps;
    while (minPage <= 0) { minPage++;}
    minPage = (totalPage.length - rangeMinMax) > minPage ? minPage : (totalPage.length - rangeMinMax);
    const maxPage = (minPage + rangeMinMax);
    
    return  <nav id={ id ? id : 'hook-paginator'} className={`hook-paginator-container${className ? ' ' + className: ''}`}>
                <ul className="hook-pagination">

                    { navigation &&
                        <li 
                            className={`page-item prev${page === 1 ? ' disabled' : ''}`}
                            onClick={ () => setPage(page - 1) }
                        >
                                {'<<'}
                        </li> }

                    { minPage > 1 && <>
                        <li 
                        className={`page-item`}
                        onClick={ () => setPage(1) }
                        >
                            1
                        </li>

                        <li className={`page-item disabled ellipsis`}>
                            ...
                        </li> </>}

                    {totalPage.map( index => {
                        return   ((index + 1) <= maxPage & (index + 1) >= minPage)  ?
                            <li
                                key={`page-item-${index}`}
                                className={`page-item ${page === (index + 1) ? 'active' : ''} ${index + 1 > 99 ? ' three-dig': index + 1 > 9 ? ' two-dig' : ''}`}
                                onClick={ () => setPage(index + 1) }
                            >
                                {index + 1}
                            </li>
                        :
                            <li className="page-item hide" key={index}>{index + 1}</li>
                    })}

                    { maxPage < totalPage.length && <>

                        <li className={`page-item disabled ellipsis`}>
                            ...
                        </li>

                        <li 
                        className={`page-item${totalPage.length > 99 ? ' three-dig': totalPage.length > 9 ? ' two-dig' : ''}`}
                        onClick={ () => setPage(totalPage.length) }
                        >
                            {totalPage.length}
                        </li> </>}

                    { navigation &&
                        <li
                            //className={`page-item next${maxPage === totalPage.length ? ' disabled' : ''}`}
                            className={`page-item next${page === totalPage.length ? ' disabled' : ''}`}
                            onClick={ () => { if(page < totalPage.length){ setPage(page + 1)} } }
                        >
                            {'>>'}
                        </li>}
                </ul>
            </nav>
})
const usePaginator = (initialPage = 1) => {
    const [page, setPage] = useState(parseInt(initialPage ? initialPage : 1) );
    return [Paginator, page, setPage];
}
export default usePaginator;
/* HOW TO USE
    const [Paginator, page, setPage] = usePaginator(query.get('page'));

    <Paginator 
        page={page}
        total={300}
        limit={2}
        navigation={true}
        //steps={3}
        setPage={setPage}
        className={null}
        id={null}
    />

*/