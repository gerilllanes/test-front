const useDebounce = () => {
    
    let myDebounce = null;
    function debounce(fn, delay, params) {
        if(fn && {}.toString.call(fn) === '[object Function]'){
            return setTimeout( function(){ fn(params); }, delay );
        }else{
            return null;
        }
    }
    const setDebounce = (fn, delay, params) => {
        clearTimeout(myDebounce);
        myDebounce = debounce(fn, delay, params);
    }
    return setDebounce;
};
export default useDebounce;
/* HOW TO USE:
    const setDebounce = useDebounce();
    setDebounce( someFunction, 2000, {parameters to function} );
*/