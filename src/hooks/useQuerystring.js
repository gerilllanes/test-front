import {useCallback, useEffect, useMemo, useState} from 'react';
import { useHistory, useLocation } from "react-router-dom";

const useQuerystring = (params) => {

    const history = useHistory();
    const location = useLocation();
    const [querystring, setQuerystring] = useState(location.search);

    const query = useMemo( () => {
        return new URLSearchParams(location.search);
    }, [location]);

    const removeParams = (parameters) => {
        if( typeof parameters === 'object' ) {
            parameters.forEach( element => {
                query.delete(element);    
            });
        }else{
            query.delete(parameters);
        }
        setQuerystring(`?${query.toString()}`);
    }

    const addParams = useCallback( (parameters) => {
        Object.keys(parameters).forEach( key => {
            query.set(key, parameters[key]);
        });
        setQuerystring(`?${query.toString()}`);
    }, [query]);

    useEffect( () => {
        if(querystring){ history.push(querystring) };
    }, [querystring, history]);

    const handleInitialRender = useCallback( () => {
        if(params){ addParams(params);}
    }, [params, addParams]);

    useEffect( () => {
        handleInitialRender();
    }, [handleInitialRender]);
    return [query, querystring, addParams, removeParams];
};
export default useQuerystring;
/* HOW TO USE: const [query, querystring, addParams, removeParams] = useQuerystring({page: 1, limit: 10}); */