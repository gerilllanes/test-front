import { useCallback, useEffect, useState } from "react";

import useCheckUser from "./useCheckUser";

const BASE_API = process.env.REACT_APP_API_URL;

function getHeader() {
  return {
      "Authorization" : `Bearer ${localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")).apiToken : ''}` 
  };
}

const buildDateSend = (data) => {
  let dataSend = new FormData();
  for (let key in data) { dataSend.append(key, data[key]); }
  return dataSend;
}

function _get(url, params, token = true) {
  const query = new URLSearchParams(params);
  const newUrl = params ? new URL(`${url}${query?`?${query.toString()}`:''}`) : url;
  const config = token ? { method: 'GET', headers: getHeader() } : { method: 'GET' };
  return fetch( newUrl, config ).then( function(response) {
      return response.json();
  });
}

function _post(url, params, token = true) {
  let formData = buildDateSend(params);
  const config = token ? { mode: "cors", method: 'POST', body: formData, headers: getHeader() } : { mode: "cors", method: 'POST', body: formData };
  return fetch( url, config ).then( function(response) {
      return response.json();
  });
}

function _put(url, params, token = true) {
  let formData = buildDateSend(params);
  formData.append('_method', 'PUT');
  const config = token ? { mode: "cors", method: 'POST', body: formData, headers: getHeader() } : { mode: "cors", method: 'POST', body: formData };
  return fetch( url, config ).then( function(response) {
      return response.json();
  });
}

function _delete(url, token = true) {
  const config = token ? { mode: "cors", method: 'DELETE', headers: getHeader() } : { mode: "cors", method: 'DELETE' };
  return fetch( url, config ).then( function(response) {
      return response.json();
  });
}

const useFetch = (url, params = null, method = 'GET', onError = null, token = true) => {

  const [response, setResponse] = useState([]);
  const [check] = useCheckUser();

  const fetchData = useCallback(
    async (u = url, p = params, m = method, oE = onError, t = token) => {
      try {
        let res = null;
        if (u) {
          switch(m) {
            case 'POST':
              _post(BASE_API+u, p, t);
              break;
            case 'PUT':
              _put(BASE_API+u, p, t);
              break;
            case 'DELETE':
              _delete(BASE_API+u, t);
              break;
            default:
              res = await _get(BASE_API+u, p, t);
              return setResponse(res.data);
          }
        }
      } catch (error) {
        check(error);
      }
    },
    [url, params, method, onError, token, check]
  );

  useEffect(() => {
    fetchData();
  }, [fetchData]);
  return [response, fetchData];
};
export default useFetch;