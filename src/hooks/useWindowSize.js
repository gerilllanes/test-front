import {useLayoutEffect, useState} from 'react';

const useWindowSize = () => {

    const [size, setSize] = useState([0, 0]);
    useLayoutEffect( () => {
        
        function updateSize() {

            let responsiveSize = null;

            switch(true) {
                case (window.innerWidth <= 480):
                    responsiveSize = 'xs';
                    break;
                case (window.innerWidth <= 768):
                    responsiveSize = 'sm';
                    break;
                case (window.innerWidth <= 992):
                    responsiveSize = 'md';
                    break;
                case (window.innerWidth <= 1200):
                    responsiveSize = 'lg';
                    break;
                default:
                    responsiveSize = 'xl';
            }

            setSize([window.innerWidth, window.innerHeight, responsiveSize]);
        }
        window.addEventListener('resize', updateSize);
        updateSize();
        return () => window.removeEventListener('resize', updateSize);
    }, []);
    return size;
};

export default useWindowSize;