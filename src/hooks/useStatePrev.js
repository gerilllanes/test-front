import {useState, useRef, useEffect} from 'react';

const useStatePrev = (value) => {

    const [state, setState] = useState(value);
    const prevCountRef = useRef();
    useEffect( () => {
        prevCountRef.current = state;
    });
    const prev = prevCountRef.current;
    return [state, prev, setState];
};
export default useStatePrev;