import {useContext} from 'react';

import swal from "sweetalert";
import translate from "../lang/lang";
import { useHistory } from "react-router-dom";
import UserContext from "../context/UserContext/UserContext";

const useCheckUser = () => {

    const history = useHistory();
    const userContext = useContext(UserContext);
    const { setLogged, logged } = userContext;

    const check = (e) => {
        if(e && e.response){
            if(e.response.status === 401){
                if(e.response.data.message === 'Invalid token!'){
                    if(logged){ setLogged(false); }
                }else{
                    history.push("/dashboard");
                }
            }else if(e.response.status === 403 && [
                    'Login failed!',
                    'Invalid argument!',
                    'Login failed or expired!',
                    'Invalid user or logged in elsewhere!'
                ].includes(e.response.data.message) ){
                if(logged){ setLogged(false); }
            }
            swal({
                title: translate("Error"),
                text: translate(e.response.data.message),
                icon: "error",
            });
        }
    }
    return [check];
};
export default useCheckUser;