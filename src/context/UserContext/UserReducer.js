import {
  LOGIN,
  USER_MODULES,
  LOGGED,
  RECOVER_PASSWORD,
  REGISTER,
  USER_ADD,
  GET_ACTIVE_USERS,
  BLOCK_USER,
  ACTIVATE_USER_PEND,
  GET_USERS_PEND,
} from "./constants";

const userReducer = (state, action) => {
  const { payload, type } = action;

  switch (type) {
    case LOGIN:
      return {
        ...state,
        userLogged: payload,
      };
    case USER_MODULES:
      return {
        ...state,
        userModules: payload,
      };
    case LOGGED:
      return {
        ...state,
        logged: payload,
      };
    case RECOVER_PASSWORD:
      return {
        ...state,
        recoverPassword: payload,
      };
    case REGISTER:
      return {
        ...state,
        register: payload,
      };

    case USER_ADD:
      return {
        ...state,
        addUser: payload,
      };
    case GET_ACTIVE_USERS:
      return {
        ...state,
        activeUsers: payload
      }
    case BLOCK_USER: {
      return {
        ...state,
        userBlocked: payload
      }
    }
    case ACTIVATE_USER_PEND: {
      return {
        ...state,
        userToActivate: payload
      }
    }
    case GET_USERS_PEND: {
      return {
        ...state,
        usersPend: payload
      }
    }
    default:
      return state;
  }
};

export default userReducer;
