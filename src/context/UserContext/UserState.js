import { useEffect, useReducer } from "react";
import { useHistory, useLocation } from "react-router-dom";

import swal from "sweetalert";
import UserContext from "./UserContext";
import UserReducer from "./UserReducer";
//CONSTANTS
import {
  LOGIN,
  USER_MODULES,
  LOGGED,
  RECOVER_PASSWORD,
  REGISTER,
  USER_ADD,
  GET_ACTIVE_USERS,
  BLOCK_USER,
  ACTIVATE_USER_PEND,
  GET_USERS_PEND
} from "./constants";
import translate from "../../lang/lang";
//UTILS
import axios from "axios";
import { buildDataSend } from "../../utils/utils";

const UserState = ({ children }) => {
  const initialState = {
    userModules: [],
    userLogged: [],
    logged: true,
    recoverPassword: {},
    register: {},
    addUser: [],
    activeUsers: [],
    userBlocked: null,
    userToActivate: [],
    usersPend: []
  };

  const history = useHistory();

  const location = useLocation();
  const BASE_API_URL = process.env.REACT_APP_API_URL;
  const [state, dispatch] = useReducer(UserReducer, initialState);

  const setLogged = (value) => {
    dispatch({
      type: LOGGED,
      payload: value,
    });
  };

  const setLogin = (user) => {
    dispatch({
      type: LOGIN,
      payload: user,
    });
  };

  const config = {
    headers: {
      Authorization: `Bearer ${JSON.parse(localStorage.getItem("apiToken"))}`,
    },
  };

  const setAddUser = (user) => {
    dispatch({
      type: USER_ADD,
      payload: user,
    });
  };

  const setModules = (modules) => {
    let newModules = {};
    modules.forEach((module) => {
      newModules = {
        ...newModules,
        [module.moduleName]: module.value.split(","),
      };
    });
    dispatch({
      type: USER_MODULES,
      payload: newModules,
    });
  };

  const userLogin = async (userData) => {
    try {
      if (initialState.logged === true) {
        const body = buildDataSend(userData);
        const res = await axios.post(`${BASE_API_URL}/api/login`, body);
        localStorage.setItem("user", JSON.stringify(res.data.data));
        localStorage.setItem(
          "apiToken",
          JSON.stringify(res.data.data.apiToken)
        );
        setLogged(true);
        setLogin(res.data.data);
        setModules(res.data.data.modules);
        if (location.pathname === "/login" || location.pathname === "/") {
          history.push("/dashboard");
        }
      }
    } catch (e) {
      swal({
        title: translate("Error"),
        text: translate(e.response.data.message),
        icon: "error",
      });
      localStorage.clear();
    }
  };

  const putRecoverPassword = async (userEmail) => {
    try {
      const res = await axios.put(
        `${BASE_API_URL}/api/user/password/forget`,
        userEmail,
        { headers: { "Content-Type": "application/x-www-form-urlencoded" } }
      );
      dispatch({
        type: RECOVER_PASSWORD,
        payload: res,
      });
    } catch (e) {
      swal({
        title: translate("Error"),
        text: translate(e.response.data.message),
        icon: "error",
      });
    }
  };

  const postRegister = async (data) => {
    try {
      const body = buildDataSend(data);
      const res = await axios.post(`${BASE_API_URL}/api/user/pend/add`, body, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      dispatch({
        type: REGISTER,
        payload: res,
      });
    } catch (e) {
      swal({
        title: translate("Error"),
        text: translate(e.response.data.message),
        icon: "error",
      });
    }
  };

  const postUserAdd = async (data) => {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${JSON.parse(
            localStorage.getItem("apiToken")
          )}`,
        },
      };
      const body = buildDataSend(data);
      const res = await axios.post(
        `${BASE_API_URL}/api/admin/user/pend/add`,
        body,
        config
      );
      dispatch({
        type: USER_ADD,
        payload: res.data.data,
      });
      // swal({
      //   title: translate("Success"),
      //   text: translate(res.data.message),
      //   icon: "success",
      // });
    } catch (e) {
      swal({
        title: translate("Error"),
        text: translate(e.response.data.message),
        icon: "error",
      });
    }
  };

  const getActiveUsers = async () => {
    try {
      const res = await axios.get(`${BASE_API_URL}/api/user?limit=1000`, config);
      dispatch({
        type: GET_ACTIVE_USERS,
        payload: res.data.data,
      });
    } catch (e) {
      swal({
        title: translate("Error"),
        text: translate(e.response.data.message),
        icon: "error",
      });
    }
  };

  const blockUser = async (id) => {
    try {
      const res = await axios.put(
        `${BASE_API_URL}/api/admin/block`,
        id,
        config
      );
      dispatch({
        type: BLOCK_USER,
        payload: id,
      });
      if (res.status === 201) {
        swal({
          title: translate("Success"),
          text: 'User blocked successfully',
          icon: "success",
        });
      }
    } catch (e) {
      swal({
        title: translate("Error"),
        text: translate(e.response.data.message),
        icon: "error",
      });
    }
  };

  const activateUserPend = async (user) => {
    try {
      const res = await axios.put(`${BASE_API_URL}/api/activate/user`, user, config)
      dispatch({
        type: ACTIVATE_USER_PEND,
        payload: user
      })
      if (res.status === 201) {
        swal({
          title: translate("Success"),
          text: 'User activated successfully',
          icon: "success",
        });
      }
    } catch (e) {
      swal({
        title: translate("Error"),
        text: translate(e.response.data.message),
        icon: "error",
      });
    }
  }

  const getUsersPend = async () => {
    try {
      const res = await axios.get(`${BASE_API_URL}/api/user/pend/list?limit=1000`, config)
      dispatch({
        type: GET_USERS_PEND,
        payload: res.data.data
      })
    } catch (e) {
      swal({
        title: translate("Error"),
        text: translate(e.response.data.message),
        icon: "error",
      });
    }
  };

  useEffect(() => {
    if (localStorage.getItem("user")) {
      const user = JSON.parse(localStorage.getItem("user"));
      setLogin(user);
      setModules(user.modules);
    }
  }, []);

  return (
    <UserContext.Provider
      value={{
        userLogged: state.userLogged,
        userLogin,
        userModules: state.userModules,
        setLogged,
        logged: state.logged,
        recoverPassword: state.recoverPassword,
        putRecoverPassword,
        register: state.register,
        postRegister,
        postUserAdd,
        addUser: state.addUser,
        getActiveUsers,
        activeUsers: state.activeUsers,
        blockUser,
        userBlocked: state.userBlocked,
        activateUserPend,
        userToActivate: state.userToActivate,
        getUsersPend,
        usersPend: state.usersPend
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserState;
