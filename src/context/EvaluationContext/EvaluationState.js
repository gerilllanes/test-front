import { useReducer } from "react";
import axios from "axios";
import {
  GET_EVALUATION_TYPE,
  POST_EVALUATION,
  POST_EVALUATION_TYPE,
  GET_EVALUATIONS_LIST
} from "./constants";
import EvaluationContext from "./EvaluationContext";
import EvaluationReducer from "./EvaluationReducer";
import useCheckUser from "../../hooks/useCheckUser";
import swal from "sweetalert";
import { buildDataSend } from "../../utils/utils";
import translate from "../../lang/lang";

const EvaluationState = ({ children }) => {
  const initialState = {
    evaluation: [],
    evaluationTypes: [],
    evaluationType: [],
    evaluationList: []
  };

  const [check] = useCheckUser();
  const BASE_API_URL = process.env.REACT_APP_API_URL;
  const [state, dispatch] = useReducer(EvaluationReducer, initialState);

  const config = {
    headers: {
      Authorization: `Bearer ${JSON.parse(localStorage.getItem("apiToken"))}`,
    },
  };

  const postEvaluation = async (data) => {
    try {
      const body = buildDataSend(data);
      const res = await axios.post(
        `${BASE_API_URL}/api/evaluation`,
        body,
        config
      );
      dispatch({
        type: POST_EVALUATION,
        payload: res,
      });
    } catch (e) {
      swal({
        title: translate("Error"),
        text: translate(e.response.data.message),
        icon: "error",
      });
    }
  };

  const getEvaluationType = async () => {
    try {
      const res = await axios.get(
        `${BASE_API_URL}/api/evaluation-type`,
        config
      );
      const response = res.data.data;
      let evaluationTypes = []
      console.log('dassa', typeof evaluationTypes);
      for (const i in response) {
          evaluationTypes.push(response[i])
      }
      console.log('dasddassaas', evaluationTypes);
      dispatch({
        type: GET_EVALUATION_TYPE,
        payload: evaluationTypes,
      });
    } catch (e) {
      check(e);
    }
  };

  const getEvaluationList = async () => {
    try {
      const res = await axios.get(
        `${BASE_API_URL}/api/evaluation`,
        config
      )
      const evaluationListResponse = res.data.data
      dispatch({
        type: GET_EVALUATIONS_LIST,
        payload: evaluationListResponse
      })
      console.log('ev', evaluationListResponse);
    } catch (e) {
      check(e)
    }
  }

  const postEvaluationType = async (data) => {
    try {
      const body = buildDataSend(data);
      const res = await axios.post(
        `${BASE_API_URL}/api/evaluation-type`,
        body,
        config
      );
      dispatch({
        type: POST_EVALUATION_TYPE,
        payload: res,
      });
    } catch (e) {
      swal({
        title: translate("Error"),
        text: translate(e.response.data.message),
        icon: "error",
      });
    }
  };

  return (
    <EvaluationContext.Provider
      value={{
        evaluation: state.evaluation,
        postEvaluation,
        evaluationTypes: state.evaluationTypes,
        getEvaluationType,
        evaluationType: state.evaluationType,
        postEvaluationType,
        evaluationList: state.evaluationList,
        getEvaluationList
      }}
    >
      {children}
    </EvaluationContext.Provider>
  );
};
export default EvaluationState;
