import {
  GET_EVALUATION_TYPE,
  POST_EVALUATION,
  POST_EVALUATION_TYPE,
  GET_EVALUATIONS_LIST
} from "./constants";

const evaluationReducer = (state, action) => {
  const { payload, type } = action;

  switch (type) {
    case POST_EVALUATION:
      return {
        ...state,
        evaluation: payload,
      };
    case GET_EVALUATION_TYPE:
      return {
        ...state,
        evaluationTypes: payload,
      };
    case POST_EVALUATION_TYPE:
      return {
        ...state,
        evaluationType: payload,
      };
    case GET_EVALUATIONS_LIST: 
      return {
        ...state,
        evaluationList: payload
      }
    default:
      return state;
  }
};

export default evaluationReducer;
