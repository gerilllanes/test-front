import { useReducer } from "react";
import axios from "axios";

//import swal from "sweetalert";
//import translate from "../../lang/lang";
import {  GET_PLAYERS, GET_PLAYER_INFO } from "./constants";
import PlayerContext from "./PlayerContext";
import PlayerReducer from "./PlayerReducer";
import { getRandomColor } from "../../utils/utils";
import useCheckUser from "../../hooks/useCheckUser";

const PlayerState = ({ children }) => {

  const initialState = {
    players: [],
    playerInfo: []
  };

  const [check] = useCheckUser();
  const BASE_API_URL = process.env.REACT_APP_API_URL;
  const [state, dispatch] = useReducer(PlayerReducer, initialState);

  const config = {
    headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem("apiToken"))}`,
      }
  }

  const getPlayers = async (queryString = '') => {
    try {
      const res = await axios.get(`${BASE_API_URL}/api/player${queryString}`, config);
      let players = res.data.data;
      players = players.map( player => { return {...player, color: getRandomColor() } });
      console.log('players', players)
      dispatch({
        type: GET_PLAYERS,
        payload: players,
      });
    } catch (e) {
      check(e);
    }
  };
const getPlayerInfo = async (id) => {
  try {
    const res = await axios.get(`${BASE_API_URL}/api/player/${id}`, config)
    const player = ([res.data.data])
    console.log(player[0])
    dispatch({
      type: GET_PLAYER_INFO,
      payload: player[0]
    })
  } catch(e) {
    console.log(e)
  }
}

  return (
    <PlayerContext.Provider
      value={{
        players: state.players,
        getPlayers,
        playerInfo: state.playerInfo,
        getPlayerInfo
      }}
    >
      {children}
    </PlayerContext.Provider>
  );
};
export default PlayerState;