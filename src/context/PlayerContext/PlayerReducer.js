import {  GET_PLAYERS, GET_PLAYER_INFO } from "./constants";

//const playerReducer = (state, action) => { to fix warning
function playerReducer(state, action){

  const { payload, type } = action;

  switch (type) {
    case GET_PLAYERS:
      return {
        ...state,
        players: payload,
      };
    case GET_PLAYER_INFO:
      return {
        ...state,
        playerInfo: payload
      }
    default: return state
  }
};
export default playerReducer;