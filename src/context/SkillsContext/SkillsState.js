import { useReducer } from "react";
import SkillsContext from "./SkillsContext";
import SkillsReducer from "./SkillsReducer";
//CONSTANTS
import {
  GET_SKILL_GROUP,
  CREATE_NEW_SKILL_GROUP,
  GET_SKILL_AVERAGE,
  GET_SKILLS_HISTORICAL,
  GET_SKILLS_LAST,
} from "./constants";
//UTILS
import axios from "axios";
import useCheckUser from "../../hooks/useCheckUser";
import { buildDataSend } from "../../utils/utils";

const SkillsState = ({ children }) => {
  const initialState = {
    skillGroup: [],
    newSkilGroup: [],
    skillAverage: [],
    skillHistorical: [],
    skillLast: []
  };
  const BASE_API_URL = process.env.REACT_APP_API_URL;
  const [state, dispatch] = useReducer(SkillsReducer, initialState);
  const [check] = useCheckUser();
  const config = {
    headers: {
      Authorization: `Bearer ${JSON.parse(localStorage.getItem("apiToken"))}`,
    },
  };

  const getSkillGroup = async () => {
    try {
      const res = await axios.get(`${BASE_API_URL}/api/skill-group`, config);
      const skillGroup = res.data.data;
      dispatch({
        type: GET_SKILL_GROUP,
        payload: skillGroup,
      });
    } catch (e) {
      check(e);
    }
  };

  const getSkillAverage = async (queryString) => {
    try {
      const res = await axios.get(
        `${BASE_API_URL}/api/evaluation/statistics/average${queryString}`,
        config
      );
      let skillAverage = res.data.data.skiil_group;
      dispatch({
        type: GET_SKILL_AVERAGE,
        payload: skillAverage,
      });
    } catch (e) {
      check(e);
    }
  };

  const getSkillHistorical = async (queryString) => {
    try {
      const res = await axios.get(
        `${BASE_API_URL}/api/evaluation/statistics/historical${queryString}`,
        config
      );
      let skillHistorical = res.data.data;
      dispatch({
        type: GET_SKILLS_HISTORICAL,
        payload: skillHistorical,
      });
    } catch (e) {
      check(e);
    }
  };

  const getSkillLast = async (id) => {
    try {
      const res = await axios.get(
        `${BASE_API_URL}/api/evaluation/${id}/last-date`,
        config
      );
      let skillLast = res.data.data;
      dispatch({
        type: GET_SKILLS_LAST,
        payload: skillLast,
      });
    } catch (e) {
      check(e);
    }
  };

  const createSkillGroup = async (data) => {
    try {
      const body = buildDataSend(data);
      const res = await axios.post(
        `${BASE_API_URL}/api/skill-group`,
        body,
        config
      );
      dispatch({
        type: CREATE_NEW_SKILL_GROUP,
        payload: res,
      });
    } catch (e) {
      check(e);
    }
  };
  return (
    <SkillsContext.Provider
      value={{
        skillGroup: state.skillGroup,
        getSkillGroup,
        newSkilGroup: state.newSkilGroup,
        createSkillGroup,
        skillAverage: state.skillAverage,
        getSkillAverage,
        skillHistorical: state.skillHistorical,
        getSkillHistorical,
        skillLast: state.skillLast,
        getSkillLast
      }}
    >
      {children}
    </SkillsContext.Provider>
  );
};

export default SkillsState;
