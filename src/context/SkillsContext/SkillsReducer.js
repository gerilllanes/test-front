import {
  GET_SKILL_GROUP,
  CREATE_NEW_SKILL_GROUP,
  GET_SKILL_AVERAGE,
  GET_SKILLS_HISTORICAL,
  GET_SKILLS_LAST,
} from "./constants";

const skillsReducer = (state, action) => {
  const { payload, type } = action;

  switch (type) {
    case GET_SKILL_GROUP:
      return {
        ...state,
        skillGroup: payload,
      };
    case CREATE_NEW_SKILL_GROUP:
      return {
        ...state,
        newSkilGroup: payload,
      };
    case GET_SKILL_AVERAGE:
      return {
        ...state,
        skillAverage: payload,
      };
      case GET_SKILLS_HISTORICAL:
        return {
          ...state,
          skillHistorical: payload,
        };
        case GET_SKILLS_LAST:
        return {
          ...state,
          skillLast: payload,
        };
    default:
      return state;
  }
};

export default skillsReducer;
