import axios from 'axios'
import useCheckUser from '../../hooks/useCheckUser'
import ClubContext from './ClubContext'
import clubReducer from './ClubReducer'

const ClubState = ({children}) => {
    const initialState = {
        club: []
    }

    const [check] = useCheckUser()
    const BASE_API_URL = process.env.REACT_APP_API_URL;
    const [state, dispatch] = useReducer(clubReducer, initialState);

    const config = {
        headers: {
          Authorization: `Bearer ${JSON.parse(localStorage.getItem("apiToken"))}`,
        },
      };

      const getClubById = (id) => {
        try {
            // get CLUB

        } catch (e) {
            check(e)
        }
      }

      return (
          <ClubContext value={{
              club: state.club,
              getClubById
          }}>
              {children}
          </ClubContext>
      )
    
}