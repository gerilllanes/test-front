import { GET_CLUB } from "./constants";

const clubReducer = (state, action) => {
  const { payload, type } = action;

  switch (type) {
    case GET_CLUB:
      return {
        ...state,
        club: payload,
      };
    default:
      return state;
  }
};

export default clubReducer;
